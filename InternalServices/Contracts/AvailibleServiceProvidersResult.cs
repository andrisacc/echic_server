﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InternalServices.Contracts
{
    internal class AvailibleServiceProvidersResult
    {
        public List<Guid> ContractServiceProviders { get; set; }

        public List<Guid> ExpiriencedServiceProviders { get; set; }
    }
}
