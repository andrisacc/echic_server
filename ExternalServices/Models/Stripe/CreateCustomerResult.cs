﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExternalServices.Models.Stripe
{
    public class CreateCustomerResult
    {
        public string CustomerId { get; set; }
        public string CardId { get; set; }
    }
}
