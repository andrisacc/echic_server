﻿namespace ExternalServices.Models.GoogleMap
{
    public class Geometry
    {
        public Location Location { get; set; }
        public string LocationType { get; set; }
    }
}
