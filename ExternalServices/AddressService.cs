﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using ExternalServices.Helpers;
using ExternalServices.Models.GetAddressIO;
using ExternalServices.Models.GoogleMap;
using ExternalServices.Contracts;
using ExternalServices.Models;
using Newtonsoft.Json;

namespace ExternalServices
{
    public class AddressService : IAddressService
    {
        private string _urlFormat = "http://maps.googleapis.com/maps/api/geocode/json?address={0}&sensor=true";
        private string _apiKey;

        public AddressService(String apiKey)
        {
            _apiKey = apiKey;
        }

        private GeocodeResult Geocode(string address)
        {
            GeocodeResult result;
            using (WebClient wc = new WebClient())
            {
                var json = wc.DownloadString(string.Format(_urlFormat, address));
                result = JsonConvert.DeserializeObject<GeocodeResult>(json, new JsonSerializerSettings
                {
                    ContractResolver = new UnderscorePropertyNamesContractResolver()
                });
            }
            return result;
        }

        public bool CheckAddress(string address)
        {
            var geocodeResult = Geocode(address);
            return geocodeResult.Status == "OK" && geocodeResult.Results.Any(item => item.Geometry.LocationType == "ROOFTOP" && (item.Types.Contains("street_address") || item.Types.Contains("premise")));
        }

        public GeocodeAddressResult GeocodeAddress(string address)
        {
            var result = new GeocodeAddressResult();
            var geocodeResult = Geocode(address);
            var geocodeAddressResult = geocodeResult.Results.FirstOrDefault(item => (item.Geometry.LocationType == "ROOFTOP" || item.Geometry.LocationType == "RANGE_INTERPOLATED")
                && (item.Types.Contains("street_address") || item.Types.Contains("premise") || item.Types.Contains("subpremise")));
            if (geocodeResult.Status == "OK" && geocodeAddressResult != null)
            {
                result.Success = true;
                result.Latitude = geocodeAddressResult.Geometry.Location.Lat;
                result.Longitude = geocodeAddressResult.Geometry.Location.Lng;
            }
            return result;
        }

        public AddressList GetAddressList(string postcode)
        {
            var result = new AddressList();
            var getAddressResult = GetAddresses(postcode);

            result.Addresses = getAddressResult.Addresses.Select(item => FormatAddress(item)).ToArray();
            result.Latitude = getAddressResult.Latitude;
            result.Longitude = getAddressResult.Longitude;

            return result;
        }

        public FullAddressList GetFullAddressList(string postcode)
        {
            var result = new FullAddressList();
            var getAddressResult = GetAddresses(postcode);

            if (getAddressResult == null)
            {
                return new FullAddressList() { IsExist = false };
            }

            result.Addresses = getAddressResult.Addresses.Select(item =>
                new FullAddress
                {
                    Address = FormatAddress(item),
                    DetailedAddress = FormatAddress(item, true),

                }).ToArray();

            result.Latitude = getAddressResult.Latitude;
            result.Longitude = getAddressResult.Longitude;
            result.IsExist = true;

            return result;
        }

        private string FormatAddress(string item, bool isFull = false)
        {
            IEnumerable<string> parts = item.Split(',');
            if (!isFull) parts = parts.Take(4);
            return string.Join(",", parts.Where(part => !string.IsNullOrWhiteSpace(part)));
        }

        private GetAddressListResult GetAddresses(string postcode)
        {
            using (WebClient wc = new WebClient())
            {
                var credentials = Convert.ToBase64String(
                    Encoding.ASCII.GetBytes($"api-key:{_apiKey}"));
                wc.Headers[HttpRequestHeader.Authorization] = $"Basic {credentials}";
                try
                {
                    var json = wc.DownloadString($"https://api.getaddress.io/v2/uk/{postcode}");
                    return JsonConvert.DeserializeObject<GetAddressListResult>(json);
                }
                catch(Exception exc)
                {
                    return null;
                }
            }
        }
    }
}
