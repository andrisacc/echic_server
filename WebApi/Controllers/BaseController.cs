﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Database;
using db = Database.Entities;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Microsoft.Extensions.Logging;
using Mapster;
using WebApi.Models;

namespace WebApi.Controllers
{
    public class BaseController : Controller
    {
        protected DataContext _dataContext;
        protected readonly ILogger<BaseController> _logger;

        public BaseController(DataContext dataContext, ILogger<BaseController> logger)
        {
            _logger = logger;
            _dataContext = dataContext;

            TypeAdapterConfig<db.ServicesGroup, ServicesGroupModel>
                .NewConfig()
                .Map(dest => dest.IsSelected, src => src.IsSelectedByDefault);

            TypeAdapterConfig<db.Service, ServiceModel>
                .NewConfig()
                .Map(dest => dest.Cost, src => (Int32)(src.Cost*100));
        }

        private db.ServiceProvider currentUser;
        protected db.ServiceProvider CurrentUser
        {
            get
            {
                if (currentUser != null)
                {
                    return currentUser;
                }
                else
                {
                    if (User.Identity.IsAuthenticated)
                    {
                        Guid userId;
                        if (Guid.TryParse(User.Identity.Name, out userId))
                        {
                            currentUser = _dataContext.ServiceProviders.FirstOrDefault(u => u.Id == userId);
                            return currentUser;
                        }
                        return null;
                    }
                    return null;
                }
            }
        }

        protected JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings() { Formatting = Formatting.Indented, ContractResolver = new CamelCasePropertyNamesContractResolver() };
    }
}