﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Models
{
    public class PurchaseModel
    {
        public Guid Id { get; set; }

        public Int32 Quantity { get; set; }
    }
}
