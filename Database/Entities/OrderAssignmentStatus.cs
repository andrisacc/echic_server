﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Entities
{
    public enum OrderAssignmentStatus
    {
        Unassigned,
        Assigned,
        Broadcasted,
        FailedAssignment
    }
}
