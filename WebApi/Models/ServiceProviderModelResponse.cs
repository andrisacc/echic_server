﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Models
{
    public class ServiceProviderModelResponse
    {
        public Boolean IsSuccess { get; set; }

        public Boolean IsPhoneNumberAlreadyRegistred { get; set; }

        public Boolean IsEmailAlreadyRegistred { get; set; }
    }
}
