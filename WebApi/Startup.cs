﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Database;
using Database.Utilities;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.Extensions.FileProviders;
using ExternalServices;
using ExternalServices.Helpers;
using ExternalServices.Contracts;
using InternalServices;
using InternalServices.Contracts;

namespace WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IFileProvider>(new PhysicalFileProvider(DirectoryHelper.FilesDirectory));
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme).AddCookie();
            services.AddMvc();
            services.AddDbContext<DataContext>(options => options.UseMySql(Configuration.GetConnectionString("DefaultConnection")));
            services.AddEntityFrameworkSqlServer();
            services.AddAuthorization(options => { options.AddPolicy("UserOnly", policy => policy.RequireClaim("UserPolicy")); });
            services.AddCors(o => o.AddPolicy("MyPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader()
                       .AllowCredentials();
            }));
            services.AddTransient<IAddressService, AddressService>(provider => {
                return new AddressService(Configuration["ExternalServices:AddressApiKey"]);
            });

            var maxCheapOrderAmountStr = Configuration["InternalServices:OrderAssignment:maxCheapOrderAmount"];
            Int32 maxCheapOrderAmount;
            if (!Int32.TryParse(maxCheapOrderAmountStr, out maxCheapOrderAmount))
                maxCheapOrderAmount = 0;

            var orderSpanStr = Configuration["InternalServices:OrderAssignment:orderSpan"];
            Int32 orderSpan;
            if (!Int32.TryParse(orderSpanStr, out orderSpan))
                orderSpan = 0;

            var sp = services.BuildServiceProvider();

            services.AddTransient<IOrderAssignmentService, OrderAssignmentService>(provider => {
                return new OrderAssignmentService(provider.GetService<DataContext>(), maxCheapOrderAmount, orderSpan, sp.GetService<IAddressService>());
            });
#if DEBUG
            services.AddTransient<INexmoService, NexmoTestService>(provider => {
                return new NexmoTestService();
            });
#else
            services.AddTransient<INexmoService, NexmoService>(provider => {
                return new NexmoService(Configuration["ExternalServices:Nexmo:ApiKey"], Configuration["ExternalServices:Nexmo:ApiSecret"], Configuration["ExternalServices:Nexmo:Brand"]);
            });
#endif
            services.AddTransient<IStripeService, StripeService>(provider => {
                return new StripeService(Configuration["ExternalServices:Stripe:PublishableKey"]
                    , Configuration["ExternalServices:Stripe:SecretKey"]
                    , Configuration["ExternalServices:Orders:SecretKey"]
                    , Configuration["ExternalServices:Stripe:EndpointSecret"]);
            });

            services.AddTransient<IEmailService, EmailService>(provider => {
                return new EmailService(Configuration["ExternalServices:EmailService:BeauticianAdminEmail"]);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var dbContext = serviceScope.ServiceProvider.GetService<DataContext>();
                dbContext.Database.Migrate();

                DataSeeder.Seed(dbContext);
            }

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

            }

            app.UseCors("MyPolicy");

            app.UseAuthentication();

            app.UseDefaultFiles();

            app.UseStaticFiles();

            app.UseMvc();

            
        }
    }
}
