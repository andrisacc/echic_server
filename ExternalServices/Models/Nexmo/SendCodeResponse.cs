﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExternalServices.Models.Nexmo
{
    public class SendCodeResponse : BaseResponse
    {
        public string RequestId { get; set; }
    }
}
