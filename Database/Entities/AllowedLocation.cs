﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Entities
{
    public class AllowedLocation : BaseEntity
    {
        public Double RadiusLatitude { get; set; }

        public Double RadiusLongitude { get; set; }

        public Double Radius { get; set; }

        public String Type { get; set; }

        public String PhoneCode { get; set; }

        public Int32 TimeZone { get; set; }

        public String Currency { get; set; }

        public virtual List<Order> Orders { get; set; }
    }
}
