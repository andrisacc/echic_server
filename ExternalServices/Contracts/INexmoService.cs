﻿namespace ExternalServices.Contracts
{
    public interface INexmoService
    {
        string SendCode(string code, string number);
        bool VerifyCode(string requestId, string code);
    }
}
