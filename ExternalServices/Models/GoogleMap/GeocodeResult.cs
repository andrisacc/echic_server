﻿namespace ExternalServices.Models.GoogleMap
{
    public class GeocodeResult
    {
        public Result[] Results { get; set; }
        public string Status { get; set; }
    }
}
