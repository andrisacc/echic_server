﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Database.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AllowedLocations",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    Currency = table.Column<string>(nullable: true),
                    Modified = table.Column<DateTime>(nullable: false),
                    PhoneCode = table.Column<string>(nullable: true),
                    Radius = table.Column<double>(nullable: false),
                    RadiusLatitude = table.Column<double>(nullable: false),
                    RadiusLongitude = table.Column<double>(nullable: false),
                    TimeZone = table.Column<int>(nullable: false),
                    Type = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AllowedLocations", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Countries",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Countries", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Orders",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Address = table.Column<string>(nullable: true),
                    City = table.Column<string>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false),
                    Email = table.Column<string>(nullable: true),
                    ExternalId = table.Column<int>(nullable: false),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    Modified = table.Column<DateTime>(nullable: false),
                    PhoneCode = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PostalCode = table.Column<string>(nullable: true),
                    RequestedDateTimeLocal = table.Column<DateTime>(nullable: false),
                    RequestedDateTimeUtc = table.Column<DateTime>(nullable: false),
                    State = table.Column<int>(nullable: false),
                    StripeToken = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Orders", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ServiceProviders",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Address = table.Column<string>(nullable: true),
                    City = table.Column<string>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false),
                    Email = table.Column<string>(nullable: true),
                    FirstName = table.Column<string>(nullable: true),
                    IsAgree = table.Column<bool>(nullable: false),
                    LastName = table.Column<string>(nullable: true),
                    Modified = table.Column<DateTime>(nullable: false),
                    Nationality = table.Column<string>(nullable: true),
                    PhoneCode = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PostalCode = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServiceProviders", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ServicesGroups",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    IsSelectedByDefault = table.Column<bool>(nullable: false),
                    Modified = table.Column<DateTime>(nullable: false),
                    Order = table.Column<int>(nullable: false),
                    PictureUrl = table.Column<string>(nullable: true),
                    SubTitle = table.Column<string>(nullable: true),
                    Title = table.Column<string>(nullable: true),
                    UiId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServicesGroups", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ServiceProviderDocs",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    DocType = table.Column<string>(nullable: true),
                    FileExtension = table.Column<string>(nullable: true),
                    FileName = table.Column<string>(nullable: true),
                    Modified = table.Column<DateTime>(nullable: false),
                    ServiceProviderId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServiceProviderDocs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ServiceProviderDocs_ServiceProviders_ServiceProviderId",
                        column: x => x.ServiceProviderId,
                        principalTable: "ServiceProviders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Services",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Cost = table.Column<decimal>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    Duration = table.Column<int>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    Modified = table.Column<DateTime>(nullable: false),
                    Order = table.Column<int>(nullable: false),
                    ServicesGroupId = table.Column<Guid>(nullable: false),
                    SubTitle = table.Column<string>(nullable: true),
                    Title = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Services", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Services_ServicesGroups_ServicesGroupId",
                        column: x => x.ServicesGroupId,
                        principalTable: "ServicesGroups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "OrdersServices",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    Modified = table.Column<DateTime>(nullable: false),
                    OrderId = table.Column<Guid>(nullable: false),
                    Quantity = table.Column<int>(nullable: false),
                    ServiceId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrdersServices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrdersServices_Orders_OrderId",
                        column: x => x.OrderId,
                        principalTable: "Orders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrdersServices_Services_ServiceId",
                        column: x => x.ServiceId,
                        principalTable: "Services",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_OrdersServices_OrderId",
                table: "OrdersServices",
                column: "OrderId");

            migrationBuilder.CreateIndex(
                name: "IX_OrdersServices_ServiceId",
                table: "OrdersServices",
                column: "ServiceId");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceProviderDocs_ServiceProviderId",
                table: "ServiceProviderDocs",
                column: "ServiceProviderId");

            migrationBuilder.CreateIndex(
                name: "IX_Services_ServicesGroupId",
                table: "Services",
                column: "ServicesGroupId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AllowedLocations");

            migrationBuilder.DropTable(
                name: "Countries");

            migrationBuilder.DropTable(
                name: "OrdersServices");

            migrationBuilder.DropTable(
                name: "ServiceProviderDocs");

            migrationBuilder.DropTable(
                name: "Orders");

            migrationBuilder.DropTable(
                name: "Services");

            migrationBuilder.DropTable(
                name: "ServiceProviders");

            migrationBuilder.DropTable(
                name: "ServicesGroups");
        }
    }
}
