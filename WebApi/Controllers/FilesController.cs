﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ExternalServices.Helpers;
using Database;
using Database.Entities;
using Microsoft.Extensions.Logging;

namespace WebApi.Controllers
{
    [Route("api/files")]
    public class FilesController : BaseController
    {
        public FilesController(DataContext dataContext, ILogger<BaseController> logger) : base(dataContext, logger)
        {
        }

        //api/files/filename
        [HttpGet()]
        public async Task<IActionResult> Download(String serviceProviderId, String filename)
        {
            Guid tmpServiceProviderId;
            if (!Guid.TryParse(serviceProviderId, out tmpServiceProviderId))
                return StatusCode(500);

            Guid tmpFilename;
            if (!Guid.TryParse(filename, out tmpFilename))
                return StatusCode(500);

            var file = _dataContext.ServiceProviderDocs.Where(spd => spd.Id == tmpFilename && spd.ServiceProviderId == tmpServiceProviderId).FirstOrDefault();
            if (file == null)
                return StatusCode(500);

            var path = Path.Combine(DirectoryHelper.FilesDirectory, filename);

            var memory = new MemoryStream();
            using (var stream = new FileStream(path, FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }
            memory.Position = 0;
            return File(memory, "text/plain", file.FileName);
        }

        //api/files/upload
        [HttpPost()]
        public async Task<IActionResult> Upload(String serviceProviderId, IFormFile file, String docType)
        {
            Guid tmpServiceProviderId;
            if (!Guid.TryParse(serviceProviderId, out tmpServiceProviderId))
                return StatusCode(500);

            if (file == null || file.Length == 0)
                return StatusCode(500);

            if (!_dataContext.ServiceProviders.Any(sp => sp.Id == tmpServiceProviderId))
                return StatusCode(500);

            var filename = Guid.NewGuid();
            var path = Path.Combine(DirectoryHelper.FilesDirectory, filename.ToString());

            using (var stream = new FileStream(path, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }

            _dataContext.ServiceProviderDocs.Add(new ServiceProviderDoc()
            {
                Id = filename,
                FileName = file.FileName,
                FileExtension = Path.GetExtension(file.FileName),
                ServiceProviderId = tmpServiceProviderId,
                DocType = docType
            });
            _dataContext.SaveChanges();

            return new JsonResult(new { filename }, new JsonSerializerSettings() { Formatting = Formatting.Indented });
        }

        [HttpDelete()]
        public async Task<IActionResult> Delete(String filename, String serviceProviderId)
        {
            if (String.IsNullOrEmpty(filename))
                return StatusCode(503);

            Guid tmpServiceProviderId;
            if (!Guid.TryParse(serviceProviderId, out tmpServiceProviderId))
                return StatusCode(500);

            Guid tmpFilename;
            if (!Guid.TryParse(filename, out tmpFilename))
                return StatusCode(500);

            var doc = _dataContext.ServiceProviderDocs.Where(spd => spd.Id == tmpFilename && spd.ServiceProviderId == tmpServiceProviderId).FirstOrDefault();
            if (doc == null)
                return StatusCode(500);

            var path = Path.Combine(DirectoryHelper.FilesDirectory, filename);

            System.IO.File.Delete(path);

            _dataContext.ServiceProviderDocs.Remove(doc);
            _dataContext.SaveChanges();

            return new JsonResult(true, new JsonSerializerSettings() { Formatting = Formatting.Indented });
        }
    }
}