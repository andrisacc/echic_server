﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Models
{
    public class ServiceModel
    {
        public Guid Id { get; set; }

        public Guid ServicesGroupId { get; set; }

        public String Title { get; set; }

        public String SubTitle { get; set; }

        public Int32 Order { get; set; }

        public Int32 Cost { get; set; }

        public Int32 Duration { get; set; }

        public Int32 Quantity { get; set; }
    }
}
