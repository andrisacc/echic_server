﻿//using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Database.Entities;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Database
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.EnableSensitiveDataLogging();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ServiceProvider>().ToTable("ServiceProviders");
            modelBuilder.Entity<ServiceProvider>().Property(sp => sp.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<ServiceProvider>().HasMany(sp => sp.Docs).WithOne(p => p.ServiceProvider);
            modelBuilder.Entity<ServiceProvider>().Property(sp => sp.ServiceProviderType).HasDefaultValue(ServiceProviderTypes.Unassigned);
            modelBuilder.Entity<ServiceProvider>().Property(sp => sp.ManicureExpirience).HasDefaultValue(1);
            modelBuilder.Entity<ServiceProvider>().Property(sp => sp.EyebrowsExpirience).HasDefaultValue(1);
            modelBuilder.Entity<ServiceProvider>().HasMany(sp => sp.Orders).WithOne(o => o.ServiceProvider);

            modelBuilder.Entity<ServiceProviderDoc>().ToTable("ServiceProviderDocs");
            modelBuilder.Entity<ServiceProviderDoc>().Property(c => c.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<ServiceProviderDoc>().HasOne(c => c.ServiceProvider).WithMany(p => p.Docs);

            modelBuilder.Entity<AllowedLocation>().ToTable("AllowedLocations");
            modelBuilder.Entity<AllowedLocation>().Property(c => c.Id).ValueGeneratedOnAdd();

            modelBuilder.Entity<Country>().ToTable("Countries");
            modelBuilder.Entity<Country>().Property(c => c.Id).ValueGeneratedOnAdd();

            modelBuilder.Entity<ServicesGroup>().ToTable("ServicesGroups");
            modelBuilder.Entity<ServicesGroup>().Property(sp => sp.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<ServicesGroup>().HasMany(u => u.Services).WithOne(p => p.ServicesGroup);

            modelBuilder.Entity<Service>().ToTable("Services");
            modelBuilder.Entity<Service>().Property(c => c.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<Service>().HasOne(c => c.ServicesGroup).WithMany(p => p.Services);

            modelBuilder.Entity<CustomerCard>().ToTable("CustomerCards");
            modelBuilder.Entity<CustomerCard>().Property(cc => cc.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<CustomerCard>().HasOne(cc => cc.Customer).WithMany(p => p.Cards);

            modelBuilder.Entity<Customer>().ToTable("Customers");
            modelBuilder.Entity<Customer>().Property(c => c.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<Customer>().HasMany(c => c.Cards).WithOne(cc => cc.Customer);
            modelBuilder.Entity<Customer>().HasMany(c => c.Orders).WithOne(o => o.Customer);

            modelBuilder.Entity<Order>().ToTable("Orders");
            modelBuilder.Entity<Order>().HasKey(o => o.Id);
            modelBuilder.Entity<Order>().Property(o => o.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<Order>().HasMany(o => o.Services).WithOne(s => s.Order);
            modelBuilder.Entity<Order>().HasOne(o => o.Customer).WithMany(c => c.Orders);
            modelBuilder.Entity<Order>().HasOne(o => o.Location).WithMany(c => c.Orders);
            modelBuilder.Entity<Order>().HasOne(o => o.ServiceProvider).WithMany(sp => sp.Orders);
            modelBuilder.Entity<Order>().Property(o => o.AssignmentStatus).HasDefaultValue(OrderAssignmentStatus.Unassigned);

            modelBuilder.Entity<OrderService>().ToTable("OrdersServices");
            modelBuilder.Entity<OrderService>().Property(c => c.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<OrderService>().HasOne(c => c.Order).WithMany(p => p.Services);
            modelBuilder.Entity<OrderService>().HasOne(c => c.Service);
        }

        public override int SaveChanges()
        {
            AddAuitInfo();
            return base.SaveChanges();
        }

        public async Task<int> SaveChangesAsync()
        {
            AddAuitInfo();
            return await base.SaveChangesAsync();
        }

        private void AddAuitInfo()
        {
            var entries = ChangeTracker.Entries().Where(x => x.Entity is BaseEntity && (x.State == EntityState.Added || x.State == EntityState.Modified));
            foreach (var entry in entries)
            {
                if (entry.State == EntityState.Added)
                {
                    ((BaseEntity)entry.Entity).Created = DateTime.UtcNow;
                }
            ((BaseEntity)entry.Entity).Modified = DateTime.UtcNow;
            }
        }

        public DbSet<ServiceProvider> ServiceProviders { get; set; }
        public DbSet<ServiceProviderDoc> ServiceProviderDocs { get; set; }
        public DbSet<AllowedLocation> AllowedLocations { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<ServicesGroup> ServicesGroups { get; set; }
        public DbSet<Service> Services { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderService> OrdersServices { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<CustomerCard> CustomerCards { get; set; }
    }
}
