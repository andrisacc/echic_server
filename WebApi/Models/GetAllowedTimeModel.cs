﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Models
{
    public class GetAllowedTimeModel
    {
        public Int32 Year { get; set; }

        public Int32 Month { get; set; }

        public Int32 TotalDuration { get; set; }

        public Int32 TimeZone { get; set; }
    }
}
