﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Database;
using Database.Entities;
using Mapster;
using WebApi.Models;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authentication.Cookies;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;

namespace WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Auth")]
    public class AuthController : Controller
    {
        private DataContext _dataContext;

        public AuthController(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        // PUT: api/auth/login
        [HttpPost("Login")]
        public IActionResult Login([FromBody]UserModel model)
        {
            //if(model != null && !String.IsNullOrEmpty(model.Username))
            //{
            //    var user = _dataContext.Users.Where(u => u.Username == model.Username).FirstOrDefault();
            //    if(user != null && user.Password == model.Password)
            //    {
            //        var claims = new List<Claim>
            //        {
            //            new Claim(ClaimTypes.Name, user.Id.ToString()),
            //            new Claim("UserPolicy", ""),
            //            new Claim("LastChanged", DateTime.Now.ToString())
            //        };

            //        var claimsIdentity = new ClaimsIdentity(
            //            claims,
            //            CookieAuthenticationDefaults.AuthenticationScheme);

            //        HttpContext.SignInAsync(
            //            CookieAuthenticationDefaults.AuthenticationScheme,
            //            new ClaimsPrincipal(claimsIdentity),
            //            new AuthenticationProperties
            //            {
            //                IsPersistent = true
            //            }).GetAwaiter().GetResult();

            //        return Ok();
            //    }
            //}

            return new UnauthorizedResult();
        }

        // PUT: api/auth/logout
        [HttpPost("Logout")]
        public void Logout()
        {
            HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme).GetAwaiter().GetResult();
        }
    }
}