﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Database.Entities
{
    public class ServiceProviderDoc : BaseEntity
    {
        [Required]
        public Guid ServiceProviderId { get; set; }

        public String FileName { get; set; }

        public String FileExtension { get; set; }

        public String DocType { get; set; }

        [ForeignKey("ServiceProviderId")]
        public virtual ServiceProvider ServiceProvider { get; set; }
    }
}
