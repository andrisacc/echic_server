﻿using System;
using System.Collections.Generic;
using System.Text;
using Database;
using Database.Entities;

namespace InternalServices.Models
{
    public class OrderAssignmentResult
    {
        public Boolean IsAssigned { get; set; }

        public Boolean IsBroadcasted { get; set; }
        
        public ServiceProvider ServiceProvider { get; set; }
    }
}
