﻿using System;
using System.Collections.Generic;
using System.Text;
using Database.Entities;
using System.Linq;

namespace Database.Utilities
{
    public class DataSeeder
    {
        public static void Seed(DataContext dataContext)
        {
            if (!dataContext.AllowedLocations.Any())
            {
                var london = new AllowedLocation()
                {
                    RadiusLatitude = 51.5074,
                    RadiusLongitude = -0.127756,
                    Radius = 34000,
                    Type = "Radius",
                    PhoneCode = "+44",
                    TimeZone = 1,
                    Currency = "gbp"
                };
                dataContext.AllowedLocations.Add(london);

                var wick = new AllowedLocation()
                {
                    RadiusLatitude = 58.4365348815918,
                    RadiusLongitude = -3.08660173416138,
                    Radius = 2500,
                    Type = "Radius",
                    PhoneCode = "+7",
                    TimeZone = 1,
                    Currency = "gbp"
                };
                dataContext.AllowedLocations.Add(wick);

                var thurso = new AllowedLocation()
                {
                    RadiusLatitude = 58.594686,
                    RadiusLongitude = -3.522066,
                    Radius = 2500,
                    Type = "Radius",
                    PhoneCode = "+44",
                    TimeZone = 1,
                    Currency = "gbp"
                };
                dataContext.AllowedLocations.Add(thurso);

                var brora = new AllowedLocation()
                {
                    RadiusLatitude = 58.0126674,
                    RadiusLongitude = -3.8534821,
                    Radius = 2500,
                    Type = "Radius",
                    PhoneCode = "+375",
                    TimeZone = 1,
                    Currency = "gbp"
                };
                dataContext.AllowedLocations.Add(brora);

                dataContext.SaveChanges();
            }

            if (!dataContext.ServicesGroups.Any())
            {
                var nails = new ServicesGroup() { UiId = 1, Order = 1, PictureUrl = "/static/images/checkout/Manicure_icon_booking.svg", Title = "Manicure & Pedicure", SubTitle = "This is a perfect treat to make your hands and feet feel like new", IsActive = true, IsSelectedByDefault = true };
                var waxing = new ServicesGroup() { UiId = 2, Order = 2, PictureUrl = "/static/images/checkout/waxing_booking_icon.svg", Title = "Waxing", SubTitle = "All waxing is available with the use of Tea Tree Creme Wax or Hot Wax. Both types of wax are made with tea tree oil and contain natural antiseptic properties.", IsActive = true, IsSelectedByDefault = false };
                var eyebrows = new ServicesGroup() { UiId = 3, Order = 3, PictureUrl = "/static/images/checkout/eyes_brows_booking_icon.svg", Title = "Eyebrows & Eyelashes", SubTitle = "", IsActive = true, IsSelectedByDefault = false };
                var mens = new ServicesGroup() { UiId = 4, Order = 4, PictureUrl = "/static/images/checkout/mens_bookiing_icon.svg", Title = "Men's Services", SubTitle = "", IsActive = true, IsSelectedByDefault = false };

                dataContext.Add(nails);
                dataContext.Add(waxing);
                dataContext.Add(eyebrows);
                dataContext.Add(mens);
                dataContext.SaveChanges();

                if (!dataContext.Services.Any())
                {
                    dataContext.Services.Add(new Service() { Order = 1, Title = "Basic Manicure", IsManiPedi = true, IsWaxing = false, SubTitle = "Your regular manicure that includes: a  soak, file, cuticle work, hand massage and polish", Cost = 40, Duration = 45, ServicesGroupId = nails.Id, IsActive = true });
                    dataContext.Services.Add(new Service() { Order = 2, Title = "Luxury Manicure", IsManiPedi = true, IsWaxing = false, SubTitle = "Just like your Basic Manicure this manicure gives every inch of the hands the pampering it needs. But, it also includes: exfoliation, hand mask and a hand &arm massage", Cost = 50, Duration = 60, ServicesGroupId = nails.Id, IsActive = true });
                    dataContext.Services.Add(new Service() { Order = 3, Title = "Luxury Gel Manicure", IsManiPedi = true, IsWaxing = false, SubTitle = "Offering all of the Luxury Manicure benefits plus the gel colour polish", Cost = 63, Duration = 90, ServicesGroupId = nails.Id, IsActive = true });
                    dataContext.Services.Add(new Service() { Order = 4, Title = "Gel Manicure ", IsManiPedi = true, IsWaxing = false, SubTitle = "Offering all your Basic Manicure benefits plus the gel colour polish", Cost = 45, Duration = 60, ServicesGroupId = nails.Id, IsActive = true });
                    dataContext.Services.Add(new Service() { Order = 5, Title = "Basic Pedicure", IsManiPedi = true, IsWaxing = false, SubTitle = "Includes foot spa, hard skin removal, nails cut and filed, cuticle work, and a foot and leg massage", Cost = 45, Duration = 50, ServicesGroupId = nails.Id, IsActive = true });
                    dataContext.Services.Add(new Service() { Order = 6, Title = "Luxury Pedicure", IsManiPedi = true, IsWaxing = false, SubTitle = "Offering all your Basic Pedicure benefits plus Foot Mask and Heated Boots!", Cost = 55, Duration = 60, ServicesGroupId = nails.Id, IsActive = true });
                    dataContext.Services.Add(new Service() { Order = 7, Title = "Luxury Gel Pedicure", IsManiPedi = true, IsWaxing = false, SubTitle = "Full gel pedicure, Foot Mask and Heated Boots", Cost = 70, Duration = 90, ServicesGroupId = nails.Id, IsActive = true });
                    dataContext.Services.Add(new Service() { Order = 8, Title = "Gel Pedicure", IsManiPedi = true, IsWaxing = false, SubTitle = "Offering all your Full Pedicure benefits plus Long Lasting gel colours - Chip Free", Cost = 63, Duration = 60, ServicesGroupId = nails.Id, IsActive = true });
                    dataContext.Services.Add(new Service() { Order = 9, Title = "Express Mani-Pad", IsManiPedi = true, IsWaxing = false, SubTitle = "Offering quick mani-pedi on-the-go, including: file, shape and polish", Cost = 55, Duration = 45, ServicesGroupId = nails.Id, IsActive = true });
                    dataContext.Services.Add(new Service() { Order = 10, Title = "Normal Mani-Padi", IsManiPedi = true, IsWaxing = false, SubTitle = "Offering full Manicure plus full Pedicure benefits", Cost = 75, Duration = 120, ServicesGroupId = nails.Id, IsActive = true });
                    dataContext.Services.Add(new Service() { Order = 11, Title = "Luxury Gel Mani-Padi", IsManiPedi = true, IsWaxing = false, SubTitle = "Offering all your full Gel Luxury Manicure and Gel Luxury Pedicure benefits", Cost = 115, Duration = 180, ServicesGroupId = nails.Id, IsActive = true });
                    dataContext.Services.Add(new Service() { Order = 12, Title = "Gel Mani-Padi", IsManiPedi = true, IsWaxing = false, SubTitle = "Offering all your Full Pedicure and Manicure benefits plus Long Lasting gel colours - Chip Free", Cost = 90, Duration = 120, ServicesGroupId = nails.Id, IsActive = true });
                    dataContext.Services.Add(new Service() { Order = 13, Title = "Express Manicure", IsManiPedi = true, IsWaxing = false, SubTitle = "Includes: File, Shape, Polish", Cost = 30, Duration = 20, ServicesGroupId = nails.Id, IsActive = true });
                    dataContext.Services.Add(new Service() { Order = 14, Title = "Express Pedicure", IsManiPedi = true, IsWaxing = false, SubTitle = "Includes: File, Shape, Polish", Cost = 35, Duration = 25, ServicesGroupId = nails.Id, IsActive = true });

                    dataContext.Services.Add(new Service() { Order = 1, Title = "Full Legs", IsManiPedi = false, IsWaxing = true, SubTitle = "From ankles up to bikini line", Cost = 50, Duration = 40, ServicesGroupId = waxing.Id, IsActive = true });
                    dataContext.Services.Add(new Service() { Order = 2, Title = "Half Legs", IsManiPedi = false, IsWaxing = true, SubTitle = "From ankles up to above the knee", Cost = 50, Duration = 30, ServicesGroupId = waxing.Id, IsActive = true });
                    dataContext.Services.Add(new Service() { Order = 3, Title = "Brazillian", IsManiPedi = false, IsWaxing = true, SubTitle = "A step up from the regular waxing procedures - hair is removed from the front, back and everywhere in between", Cost = 50, Duration = 25, ServicesGroupId = waxing.Id, IsActive = true });
                    dataContext.Services.Add(new Service() { Order = 4, Title = "Hollywood", IsManiPedi = false, IsWaxing = true, SubTitle = "The ultimate solution! Removes all of the hair from the front, middle, and all the way to the backside.", Cost = 50, Duration = 30, ServicesGroupId = waxing.Id, IsActive = true });
                    dataContext.Services.Add(new Service() { Order = 5, Title = "Bikini", IsManiPedi = false, IsWaxing = true, SubTitle = "Removes all hair around your underwear line, nice and elegant!", Cost = 30, Duration = 15, ServicesGroupId = waxing.Id, IsActive = true });
                    dataContext.Services.Add(new Service() { Order = 6, Title = "Half Leg & Bikini", IsManiPedi = false, IsWaxing = true, SubTitle = "Offering Half legs waxing and a bikini line as one!", Cost = 60, Duration = 45, ServicesGroupId = waxing.Id, IsActive = true });
                    dataContext.Services.Add(new Service() { Order = 7, Title = "Full Leg & Bikini", IsManiPedi = false, IsWaxing = true, SubTitle = "Offering all of the Full Legs and Bikini Line waxing benefits", Cost = 65, Duration = 50, ServicesGroupId = waxing.Id, IsActive = true });
                    dataContext.Services.Add(new Service() { Order = 8, Title = "Venus (full Body Wax)", IsManiPedi = false, IsWaxing = true, SubTitle = "The complete package! Includes: full leg, full arms ,Hollywood, back and chest", Cost = 150, Duration = 120, ServicesGroupId = waxing.Id, IsActive = true });
                    dataContext.Services.Add(new Service() { Order = 9, Title = "Lip & Chin & Eyebrow Wax", IsManiPedi = false, IsWaxing = true, SubTitle = "Offering all your Full Lip & Chin and Eyebrow wax benefits", Cost = 40, Duration = 30, ServicesGroupId = waxing.Id, IsActive = true });
                    dataContext.Services.Add(new Service() { Order = 10, Title = "Bikini & Full Legs & Eyebrow wax", IsManiPedi = false, IsWaxing = true, SubTitle = "A Great treatments combination of Bikini line & Full legs and Eyebrow wax", Cost = 80, Duration = 60, ServicesGroupId = waxing.Id, IsActive = true });
                    dataContext.Services.Add(new Service() { Order = 11, Title = "Eyebrow tint & eyebrow wax & underarm wax", IsManiPedi = false, IsWaxing = true, SubTitle = "Includies: Eyebrow Shape, Colour and Underarm Waxing", Cost = 40, Duration = 45, ServicesGroupId = waxing.Id, IsActive = true });
                    dataContext.Services.Add(new Service() { Order = 12, Title = "Bikini & underarm wax", IsManiPedi = false, IsWaxing = true, SubTitle = "Offering full Bikini Line and Underarm Wax benefits", Cost = 40, Duration = 30, ServicesGroupId = waxing.Id, IsActive = true });
                    dataContext.Services.Add(new Service() { Order = 13, Title = "Full Face Wax", IsManiPedi = false, IsWaxing = true, SubTitle = "*Includes eyebrows", Cost = 65, Duration = 30, ServicesGroupId = waxing.Id, IsActive = true });
                    dataContext.Services.Add(new Service() { Order = 14, Title = "Half Arm", IsManiPedi = false, IsWaxing = true, SubTitle = "You can choose to have your lower arms waxed, which removes everything from the elbow down to the wrist. Perfect for removing dead skin and improving the appearance of the skin", Cost = 30, Duration = 30, ServicesGroupId = waxing.Id, IsActive = true });
                    dataContext.Services.Add(new Service() { Order = 15, Title = "Full Arm", IsManiPedi = false, IsWaxing = true, SubTitle = "Waxing done from wrists and up to your shoulders", Cost = 30, Duration = 40, ServicesGroupId = waxing.Id, IsActive = true });
                    dataContext.Services.Add(new Service() { Order = 16, Title = "Chin/Lip/Sideburn", IsManiPedi = false, IsWaxing = true, SubTitle = "Smooth results last up to 4 weeks", Cost = 30, Duration = 20, ServicesGroupId = waxing.Id, IsActive = true });

                    dataContext.Services.Add(new Service() { Order = 1, Title = "Shape + Tint for Brows + Tint for Eyelashes", IsManiPedi = false, IsWaxing = false, SubTitle = "Shape/Tint for Eyebrows/Eyelashes - when the beautician arrives, you can let them know which of the three you would prefer", Cost = 35, Duration = 45, ServicesGroupId = eyebrows.Id, IsActive = true });
                    dataContext.Services.Add(new Service() { Order = 2, Title = "Full Face Thread", IsManiPedi = false, IsWaxing = false, SubTitle = "(included eyebrows)", Cost = 65, Duration = 30, ServicesGroupId = eyebrows.Id, IsActive = true });
                    dataContext.Services.Add(new Service() { Order = 3, Title = "Lip & Chin & Eyebrow Wax", IsManiPedi = false, IsWaxing = false, SubTitle = "Offering Full Lip ,Chin and Eyebrow waxing Benefits", Cost = 40, Duration = 30, ServicesGroupId = eyebrows.Id, IsActive = true });
                    dataContext.Services.Add(new Service() { Order = 4, Title = "Eyebrow wax/threading", IsManiPedi = false, IsWaxing = false, SubTitle = "Your brows are measured and customised to suit your individual face shape and brow potential. Let our beautician know which of the two you would prefer, when they arrive.", Cost = 30, Duration = 15, ServicesGroupId = eyebrows.Id, IsActive = true });

                    dataContext.Services.Add(new Service() { Order = 1, Title = "Basic Manicure", IsManiPedi = true, IsWaxing = false, SubTitle = "Includes a soak, file, cuticle work and hand massage", Cost = 40, Duration = 45, ServicesGroupId = mens.Id, IsActive = true });
                    dataContext.Services.Add(new Service() { Order = 2, Title = "Luxury Manicure", IsManiPedi = true, IsWaxing = false, SubTitle = "All of the normal manicure benefits, and also ex-foliation of hands and paraffin treatment. Helps to ease stiff joints, rheumatism, arthritis and dry hands.", Cost = 50, Duration = 60, ServicesGroupId = mens.Id, IsActive = true });
                    dataContext.Services.Add(new Service() { Order = 3, Title = "Pedicure", IsManiPedi = true, IsWaxing = false, SubTitle = "Male pedicures are simply designed to give a naturally neat and healthy look to your feet. Treatments usually involve a cleansing soak, a gentle massage, a good rough exfoliating scrub to get rid of hard skin, nice-smelling moisturiser to soften and hydrate the skin and nails, and finally a neat clip.", Cost = 45, Duration = 45, ServicesGroupId = mens.Id, IsActive = true });
                    dataContext.Services.Add(new Service() { Order = 4, Title = "Luxury Pedicure", IsManiPedi = true, IsWaxing = false, SubTitle = "Offering all of your Basic Pedicure benefits plus Foot Mask and Heated boots", Cost = 55, Duration = 60, ServicesGroupId = mens.Id, IsActive = true });
                    dataContext.Services.Add(new Service() { Order = 5, Title = "Basic Mani-Pedi", IsManiPedi = true, IsWaxing = false, SubTitle = "Having a manicure and pedicure every once a month make sure your hands and feet stay in top shape and ensure you make a good impression wherever you go.)", Cost = 75, Duration = 90, ServicesGroupId = mens.Id, IsActive = true });
                    dataContext.Services.Add(new Service() { Order = 6, Title = "Luxury Mani-Pedi", IsManiPedi = true, IsWaxing = false, SubTitle = "Offering all of you full Luxury Manicure and Luxury Pedicure benefits", Cost = 95, Duration = 120, ServicesGroupId = mens.Id, IsActive = true });
                    dataContext.Services.Add(new Service() { Order = 7, Title = "Chest wax", IsManiPedi = false, IsWaxing = true, SubTitle = "Includes the removal of hair across the chest using hot wax or strip wax", Cost = 40, Duration = 30, ServicesGroupId = mens.Id, IsActive = true });
                    dataContext.Services.Add(new Service() { Order = 8, Title = "Eyebrow Threading", IsManiPedi = false, IsWaxing = false, SubTitle = "The main difference between women's and men's threading is the extent of the hair removal. For men, only minimal cleaning is required. The natural shape of the eyebrows is maintained, but stray hairs are removed. Stray hair, especially with strands thinner than eyebrow hair, can cause a shadowy shade around the eye.", Cost = 30, Duration = 20, ServicesGroupId = mens.Id, IsActive = true });
                    dataContext.Services.Add(new Service() { Order = 9, Title = "Back Wax", IsManiPedi = false, IsWaxing = true, SubTitle = "Hot wax  or strip wax applied to lower back and across the shoulders and removed once hardened, removing hair in the process.", Cost = 40, Duration = 30, ServicesGroupId = mens.Id, IsActive = true });
                    dataContext.Services.Add(new Service() { Order = 10, Title = "Half-Arm", IsManiPedi = false, IsWaxing = true, SubTitle = "Bare those arms! Our waxing service is designed to smoothen and remove unwanted hair. You can choose to have your lower arms waxed which removes everything from the elbow down to the wrist. Perfect for removing dead skin and improving the appearance of the skin.", Cost = 30, Duration = 30, ServicesGroupId = mens.Id, IsActive = true });
                    dataContext.Services.Add(new Service() { Order = 11, Title = "Back Wax + Arm", IsManiPedi = false, IsWaxing = true, SubTitle = "Offering full Back Wax benefits and full Arm Wax benefits", Cost = 60, Duration = 45, ServicesGroupId = mens.Id, IsActive = true });
                    dataContext.Services.Add(new Service() { Order = 12, Title = "Chest + Half-Arm", IsManiPedi = false, IsWaxing = true, SubTitle = "Offering full Chest and Half Arm Waxing benefits", Cost = 45, Duration = 45, ServicesGroupId = mens.Id, IsActive = true });
                    dataContext.SaveChanges();
                }
            }

            if (!dataContext.Countries.Any())
            {
                dataContext.Countries.Add(new Country() { Name = "	Afghanistan	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Albania	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Algeria	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Andorra	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Angola	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Antigua & Deps	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Argentina	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Armenia	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Australia	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Austria	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Azerbaijan	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Bahamas	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Bahrain	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Bangladesh	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Barbados	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Belarus	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Belgium	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Belize	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Benin	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Bhutan	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Bolivia	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Bosnia Herzegovina	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Botswana	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Brazil	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Brunei	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Bulgaria	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Burkina	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Burundi	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Cambodia	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Cameroon	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Canada	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Cape Verde	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Central African Rep	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Chad	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Chile	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	China	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Colombia	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Comoros	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Congo	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Congo {Democratic Rep}	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Costa Rica	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Croatia	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Cuba	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Cyprus	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Czech Republic	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Denmark	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Djibouti	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Dominica	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Dominican Republic	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	East Timor	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Ecuador	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Egypt	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	El Salvador	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Equatorial Guinea	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Eritrea	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Estonia	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Ethiopia	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Fiji	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Finland	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	France	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Gabon	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Gambia	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Georgia	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Germany	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Ghana	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Greece	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Grenada	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Guatemala	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Guinea	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Guinea-Bissau	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Guyana	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Haiti	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Honduras	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Hungary	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Iceland	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	India	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Indonesia	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Iran	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Iraq	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Ireland {Republic}	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Israel	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Italy	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Ivory Coast	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Jamaica	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Japan	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Jordan	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Kazakhstan	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Kenya	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Kiribati	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Korea North	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Korea South	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Kosovo	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Kuwait	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Kyrgyzstan	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Laos	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Latvia	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Lebanon	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Lesotho	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Liberia	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Libya	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Liechtenstein	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Lithuania	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Luxembourg	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Macedonia	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Madagascar	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Malawi	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Malaysia	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Maldives	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Mali	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Malta	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Marshall Islands	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Mauritania	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Mauritius	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Mexico	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Micronesia	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Moldova	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Monaco	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Mongolia	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Montenegro	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Morocco	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Mozambique	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Myanmar, {Burma}	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Namibia	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Nauru	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Nepal	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Netherlands	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	New Zealand	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Nicaragua	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Niger	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Nigeria	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Norway	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Oman	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Pakistan	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Palau	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Panama	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Papua New Guinea	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Paraguay	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Peru	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Philippines	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Poland	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Portugal	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Qatar	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Romania	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Russian Federation	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Rwanda	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	St Kitts & Nevis	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	St Lucia	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Saint Vincent & the Grenadines	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Samoa	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	San Marino	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Sao Tome & Principe	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Saudi Arabia	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Senegal	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Serbia	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Seychelles	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Sierra Leone	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Singapore	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Slovakia	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Slovenia	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Solomon Islands	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Somalia	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	South Africa	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	South Sudan	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Spain	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Sri Lanka	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Sudan	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Suriname	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Swaziland	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Sweden	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Switzerland	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Syria	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Taiwan	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Tajikistan	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Tanzania	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Thailand	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Togo	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Tonga	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Trinidad & Tobago	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Tunisia	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Turkey	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Turkmenistan	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Tuvalu	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Uganda	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Ukraine	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	United Arab Emirates	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	United Kingdom	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	United States	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Uruguay	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Uzbekistan	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Vanuatu	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Vatican City	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Venezuela	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Vietnam	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Yemen	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Zambia	".TrimStart().TrimEnd().Replace("\t", "") });
                dataContext.Countries.Add(new Country() { Name = "	Zimbabwe	".TrimStart().TrimEnd().Replace("\t", "") });

                dataContext.SaveChanges();
            }
        }
    }
}
