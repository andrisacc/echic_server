﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Database.Entities
{
    public class Customer : BaseEntity
    {
        public String CustomerStripeId { get; set; }

        public String PhoneCode { get; set; }

        public String PhoneNumber { get; set; }

        public String FirstName { get; set; }

        public String LastName { get; set; }

        public String Email { get; set; }

        public virtual List<CustomerCard> Cards { get; set; }

        public virtual List<Order> Orders { get; set; }
    }
}
