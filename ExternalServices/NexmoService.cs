﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using ExternalServices.Models.Nexmo;
using ExternalServices.Contracts;
using Newtonsoft.Json;
using ExternalServices.Helpers;

namespace ExternalServices
{
    public class NexmoService : INexmoService
    {
        private String _apiKey;
        private String _apiSecret;
        private String _brand;

        private const string ApiBaseUrl = "https://api.nexmo.com/verify";

        public NexmoService(String apiKey, String apiSecret, String brand)
        {
            _apiKey = apiKey;
            _apiSecret = apiSecret;
            _brand = brand;
        }

        //var json = "{\"request_id\":\"783416b9b07449079d4c0c422a590eab\",\"status\":\"0\"}";
        public string SendCode(string code, string number)
        {
            if(number.StartsWith("00"))
            {
                number = number.Substring(2, number.Length - 2);
            }
            else if (number.StartsWith("0"))
            {
                number = number.Substring(1, number.Length - 1);
            }

            var fullNumber = code + number;

            using (var client = new WebClient())
            {
                var json = client.DownloadString($"{ApiBaseUrl}/json?api_key={_apiKey}&api_secret={_apiSecret}&number={fullNumber}&brand={_brand}");

                var response = JsonConvert.DeserializeObject<SendCodeResponse>(json, new JsonSerializerSettings
                {
                    ContractResolver = new UnderscorePropertyNamesContractResolver()
                });

                return response.RequestId;
            }
        }

        //var json = "{\"status\":\"0\",\"error_text\":\"Invalid code\"}";
        public bool VerifyCode(string requestId, string code)
        {
            var result = false;
            using (var client = new WebClient())
            {
                var json = client.DownloadString($"{ApiBaseUrl}/check/json?api_key={_apiKey}&api_secret={_apiSecret}&request_id={requestId}&code={code}");

                var response = JsonConvert.DeserializeObject<VerifyCodeResponse>(json, new JsonSerializerSettings
                {
                    ContractResolver = new UnderscorePropertyNamesContractResolver()
                });

                if (!string.IsNullOrWhiteSpace(response.ErrorText))
                {
                    //Log.Info($"Verifying code error: {response.ErrorText}");
                }

                if (response.Status == 0)
                    result = true;
            }
            return result;
        }
    }
}
