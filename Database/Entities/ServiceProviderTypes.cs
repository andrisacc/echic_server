﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Entities
{
    public enum ServiceProviderTypes
    {
        Unassigned,
        Contract,
        ManiPedi,
        WaxingEyebrows
    }
}
