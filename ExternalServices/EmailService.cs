﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using ExternalServices.Contracts;
using System.Net;
using System.Net.Mail;

namespace ExternalServices
{
    public class EmailService : IEmailService
    {
        private String _beauticianAdminEmail;

        private NLog.Logger _logger = NLog.Web.NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();

        public EmailService(String beauticianAdminEmail)
        {
            _beauticianAdminEmail = beauticianAdminEmail;
        }

        public void SendEmailToAdmins(string subject, string body, Stream attachment, string attachmentName)
        {
            const String FROM = "no-reply@echic.world";
            // Supply your SMTP credentials below. Note that your SMTP credentials are different from your AWS credentials.
            const String SMTP_USERNAME = "AKIAJN4OJ6K3UMXRAKPQ";  // Replace with your SMTP username. 
            const String SMTP_PASSWORD = "AsDLcwC/iwhduhDzw7Azbo5+giO0jDhHzqQYHgZLKg2e";  // Replace with your SMTP password.

            // Amazon SES SMTP host name. This example uses the US West (Oregon) region.
            const String HOST = "email-smtp.us-west-2.amazonaws.com";

            // The port you will connect to on the Amazon SES SMTP endpoint. We are choosing port 587 because we will use
            // STARTTLS to encrypt the connection.
            const int PORT = 587;

            using (var message = new MailMessage())
            {
                message.From = new MailAddress(FROM);
                message.To.Add(new MailAddress(_beauticianAdminEmail));
                message.IsBodyHtml = true;
                message.Body = body;
                message.Subject = subject;
                var smtpClient = new SmtpClient(HOST, PORT)
                {
                    Credentials = new NetworkCredential(SMTP_USERNAME, SMTP_PASSWORD),
                    EnableSsl = true,
                };

                if (attachment != null)
                {
                    message.Attachments.Add(new Attachment(attachment, attachmentName));
                }

                smtpClient.Send(message);
            }
        }
    }
}
