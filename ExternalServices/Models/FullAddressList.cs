﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExternalServices.Models
{
    public class FullAddressList
    {
        public Boolean IsExist { get; set; }
        public Boolean IsAllowed { get; set; }
        public Int32 TimeZone { get; set; }
        public String PhoneCode { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public Guid LocationId { get; set; }
        public FullAddress[] Addresses { get; set; }
    }
}