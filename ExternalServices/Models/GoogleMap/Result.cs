﻿namespace ExternalServices.Models.GoogleMap
{
    public class Result
    {
        public AddressComponent[] AddressComponents { get; set; }
        public Geometry Geometry { get; set; }
        public string[] Types { get; set; }
    }
}
