﻿using System;
using System.Collections.Generic;
using System.Text;
using ExternalServices.Contracts;
using Stripe;
using ExternalServices.Models.Stripe;
using System.Linq;

namespace ExternalServices
{
    public class StripeService : IStripeService
    {
        private NLog.Logger _logger = NLog.Web.NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();

        String _publishableKey;
        String _secretKey;
        String _internalSecretKey;
        String _endpointSecret;

        public StripeService(String publishableKey, String secretKey, String internalSecretKey, String endpointSecret)
        {
            _publishableKey = publishableKey;
            _secretKey = secretKey;
            _internalSecretKey = internalSecretKey;
            _endpointSecret = endpointSecret;
        }

        public String GetPublishableKey()
        {
            return _publishableKey;
        }

        public String GetInternalKey()
        {
            return _internalSecretKey;
        }

        public Card[] GetCustomerCards(string customerId)
        {
            var cardsService = new StripeCardService(_secretKey);
            var cards = cardsService.List(customerId);
            return cards.Select(MapCard).ToArray();
        }

        public Card GetCard(string customerId, string cardId)
        {
            var cardsService = new StripeCardService(_secretKey);
            var card = cardsService.Get(customerId, cardId);
            return MapCard(card);
        }

        private Card MapCard(StripeCard card)
        {
            return new Card
            {
                Id = card.Id,
                Details = $"{card.Brand} **** {card.Last4}",
                CardNumber = card.Last4,
                ExpirationMonth = card.ExpirationMonth,
                ExpirationYear = card.ExpirationYear,
                CardType = card.Brand,
            };
        }

        public CreateCustomerResult CreateCustomer(string token, string email)
        {
            var result = new CreateCustomerResult();
            var customers = new StripeCustomerService(_secretKey);

            var customer = customers.Create(new StripeCustomerCreateOptions
            {
                SourceToken = token,
                Email = email
            });

            result.CustomerId = customer.Id;
            result.CardId = customer.DefaultSourceId;
            return result;
        }

        public void UpdateCustomer(string stripeCustomerId, string email)
        {
            var result = new CreateCustomerResult();
            var customers = new StripeCustomerService(_secretKey);

            var customer = customers.Update(stripeCustomerId, new StripeCustomerUpdateOptions() {
                Email = email           
            });
        }

        public Card AddCustomerCard(string customerId, string token)
        {
            var cards = new StripeCardService(_secretKey);

            var options = new StripeCardCreateOptions
            {
                SourceToken = token
            };
            var card = cards.Create(customerId, options);
            return MapCard(card);
        }

        public ChargeResult Charge(String customerId, String token, String orderNumber, Int32 Amount, Guid orderId, String currency)
        {
            try
            {
                StripeConfiguration.SetApiKey(_secretKey);

                var options = new StripeChargeCreateOptions
                {
                    CustomerId = customerId,
                    Amount = Amount*100,
                    Currency = currency,
                    Description = "Order payment: " + orderNumber,
                    SourceTokenOrExistingSourceId = token,
                    Metadata = new Dictionary<String, String>() { { "orderId", orderId.ToString() } },
                };
                var service = new StripeChargeService();
                StripeCharge charge = service.Create(options);

                var result = new ChargeResult() { IsSuccess = true };

                return result;
            }
            catch(Exception exc)
            {
                var result = new ChargeResult() { IsSuccess = false };

                return result;
            }
        }

        public WebHookRequest ParseWebhook(String json, String signature)
        {
            try
            {
                var stripeEvent = StripeEventUtility.ParseEvent(json);
                var stripeResult = StripeEventUtility.ConstructEvent(json, signature, _endpointSecret);

                if (stripeResult.Type == "charge.succeeded")
                {
                    var result = new WebHookRequest()
                    {
                        Request = json,
                        OrderId = Guid.Parse((String)stripeResult.Data.Object.metadata.orderId),
                        Amount = ((Decimal)stripeResult.Data.Object.amount / 100),
                        Status = stripeResult.Data.Object.status
                    };

                    result.IsPaymentSuccesfull = result.Status == "succeeded" && stripeResult.Type == "charge.succeeded";

                    return result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception exc)
            {
                _logger.Error(exc.Message);
                return null;
            }
        }
    }
}
