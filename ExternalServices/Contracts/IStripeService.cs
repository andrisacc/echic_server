﻿using System;
using System.Collections.Generic;
using System.Text;
using ExternalServices.Models.Stripe;

namespace ExternalServices.Contracts
{
    public interface IStripeService
    {
        String GetPublishableKey();
        String GetInternalKey();
        Card[] GetCustomerCards(string customerId);
        Card GetCard(string customerId, string cardId);
        CreateCustomerResult CreateCustomer(string token, string email);
        void UpdateCustomer(string stripeCustomerId, string email);
        Card AddCustomerCard(string customerId, string token);
        ChargeResult Charge(String customerId, String token, String orderNumber, Int32 Amount, Guid orderId, String currency);
        WebHookRequest ParseWebhook(String json, String signature);
    }
}
