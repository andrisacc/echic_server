﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExternalServices.Models.Stripe
{
    public class WebHookRequest
    {
        public Guid OrderId { get; set; }

        public Boolean IsPaymentSuccesfull { get; set; }

        public Decimal Amount { get; set; }

        public String Status { get; set; }

        public String Request { get; set; }
    }
}
