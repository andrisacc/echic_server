using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.FileExtensions;
using Microsoft.Extensions.Configuration.Json;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Database;
using db = Database.Entities;
using Database.Utilities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using InternalServices;
using InternalServices.Contracts;
using InternalServices.Models;
using ExternalServices;


namespace InternalServicesTest
{
    [TestClass]
    public class OrderAssignmentServiceTests
    {
        private IConfigurationRoot _configuration;

        private DbContextOptions<DataContext> _options;

        private Guid AddServiceProvider(DataContext dataContext,
            String PostalCode,
            String PhoneCode,
            String PhoneNumber,
            String FirstName,
            String LastName,
            String Email,
            String Address,
            String City,
            String Nationality,
            db.ServiceProviderTypes ServiceProviderType,
            Int32 ManicureExpirience,
            Int32 EyebrowsExpirience,
            Int32 Rating,
            List<Guid> OrderIds)
        {
            var sp = new db.ServiceProvider()
            {
                PostalCode = PostalCode,
                PhoneCode = PhoneCode,
                PhoneNumber = PhoneNumber,
                FirstName = FirstName,
                LastName = LastName,
                Email = Email,
                Address = Address,
                City = City,
                Nationality = Nationality,
                ServiceProviderType = ServiceProviderType,
                ManicureExpirience = ManicureExpirience,
                EyebrowsExpirience = EyebrowsExpirience,
                Rating = Rating,
                Orders = new List<db.Order>()
            };

            var orders = dataContext.Orders.Where(o => OrderIds.Contains(o.Id)).ToArray();
            sp.Orders.AddRange(orders);

            dataContext.Add(sp);

            dataContext.SaveChanges();

            return sp.Id;
        }

        private Guid AddCustomer(DataContext dataContext, String CustomerStripeId,
            String PhoneCode,
            String PhoneNumber,
            String FirstName,
            String LastName,
            String Email)
        {
            var customer = new db.Customer()
            {
                PhoneCode = PhoneCode,
                PhoneNumber = PhoneNumber,
                FirstName = FirstName,
                LastName = LastName,
                Email = Email
            };

            dataContext.Customers.Add(customer);
            dataContext.SaveChanges();

            return customer.Id;
        }

        private Guid AddOrder(DataContext dataContext,
            Int32 ExternalId,
            String PostalCode,
            String Address,
            String City,
            db.OrderStates State,
            DateTime RequestedDateTimeUtc,
            Guid CustomerId,
            List<db.Service> services,
            Guid ServiceProviderId)
        {
            var location = dataContext.AllowedLocations.Where(l => l.PhoneCode == "+44").First();

            var order = new db.Order()
            {
                ExternalId = ExternalId,
                PostalCode = PostalCode,
                Address = Address,
                City = City,
                State = State,
                RequestedDateTimeUtc = RequestedDateTimeUtc,
                RequestedDateTimeLocal = RequestedDateTimeUtc.ToLocalTime(),
                CustomerId = CustomerId,
                LocationId = location.Id,
                Services = new List<db.OrderService>()
            };

            if (ServiceProviderId != Guid.Empty)
                order.ServiceProviderId = ServiceProviderId;

            dataContext.Orders.Add(order);
            dataContext.SaveChanges();

            var orderServices = services.Select(s => new db.OrderService() { ServiceId = s.Id, OrderId = order.Id, Quantity = 1 });
            dataContext.OrdersServices.AddRange(orderServices);

            dataContext.SaveChanges();

            return order.Id;
        }

        public OrderAssignmentServiceTests()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            _configuration = builder.Build();
            _options = new DbContextOptionsBuilder<DataContext>()
                .UseMySql(_configuration.GetConnectionString("DefaultConnection"))
                .Options;
        }

        [TestInitialize]
        public void Initialize()
        {
            using (var context = new DataContext(_options))
            {
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();

                DataSeeder.Seed(context);
            }
        }

        #region cheap
        //order type: any
        //order amount: cheap
        //availible timeslots: yes
        //first sp: contracted, already has two orders
        //second sp: contracted, already has two orders
        //third sp: special, has no orders
        [TestMethod]
        public void TestMethod1111()
        {
            using (var context = new DataContext(_options))
            {
                //Arrange
                var service1 = context.Services.Where(s => s.Title == "Basic Manicure").First();
                var service2 = context.Services.Where(s => s.Title == "Luxury Manicure").First();

                var customerId1 = AddCustomer(context, "", "+44", "", "cust1", "cust1", "cust1@cust.com");
                var customerId2 = AddCustomer(context, "", "+44", "", "cust2", "cust2", "cust2@cust.com");

                var serviceProviderId1 = AddServiceProvider(context, "SE10DH", "+44", "11111111111111", "111", "111", "111@111.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 1, 1, 1, new List<Guid>());
                var serviceProviderId2 = AddServiceProvider(context, "SE10DH", "+44", "22222222222222", "222", "222", "222@222.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 1, 1, 1, new List<Guid>());
                var serviceProviderId3 = AddServiceProvider(context, "SE10DH", "+44", "33333333333333", "333", "333", "333@333.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.ManiPedi, 1, 1, 1, new List<Guid>());

                var dtOrder1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId1 = AddOrder(context, 1, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder1.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId1);
                var dtOrder2 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 17, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId2 = AddOrder(context, 2, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder2.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId1);

                var dtOrder3 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId3 = AddOrder(context, 3, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder3.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId2);
                var dtOrder4 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 17, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId4 = AddOrder(context, 4, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder4.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId2);

                var addressService = new AddressService("jNoFfAuChUSW46rMEDspKQ9178");

                var orderService = new OrderAssignmentService(context, 50, 120, addressService);
                //Act
                var newOrderDt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 14, 0, 0, DateTimeKind.Local).AddDays(1);
                var isAvailibleTime = orderService.IsAssignmentPossible(new List<db.OrderService>(new db.OrderService[] { new db.OrderService() { Quantity = 1, Service = service1 } }), newOrderDt.ToUniversalTime());

                var newOrder = AddOrder(context, 5, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, newOrderDt.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), Guid.Empty);

                var spResult = orderService.AssignOrder(newOrder);

                //Assert
                Assert.AreEqual(true, isAvailibleTime);
                Assert.AreEqual(true, spResult.IsBroadcasted);
                Assert.AreEqual(false, spResult.IsAssigned);
                Assert.AreEqual(null, spResult.ServiceProvider);
            }
        }

        //order type: any
        //order amount: cheap
        //availible timeslots: yes
        //first sp: contracted, already has two orders
        //second sp: contracted, has one order
        //third sp: special, has no orders
        [TestMethod]
        public void TestMethod1112()
        {
            using (var context = new DataContext(_options))
            {
                //Arrange
                var service1 = context.Services.Where(s => s.Title == "Basic Manicure").First();
                var service2 = context.Services.Where(s => s.Title == "Luxury Manicure").First();

                var customerId1 = AddCustomer(context, "", "+44", "", "cust1", "cust1", "cust1@cust.com");
                var customerId2 = AddCustomer(context, "", "+44", "", "cust2", "cust2", "cust2@cust.com");

                var serviceProviderId1 = AddServiceProvider(context, "SE10DH", "+44", "11111111111111", "111", "111", "111@111.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 1, 1, 1, new List<Guid>());
                var serviceProviderId2 = AddServiceProvider(context, "SE10DH", "+44", "22222222222222", "222", "222", "222@222.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 1, 1, 1, new List<Guid>());
                var serviceProviderId3 = AddServiceProvider(context, "SE10DH", "+44", "33333333333333", "333", "333", "333@333.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.ManiPedi, 1, 1, 1, new List<Guid>());

                var dtOrder1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId1 = AddOrder(context, 1, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder1.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId1);
                var dtOrder2 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 17, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId2 = AddOrder(context, 2, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder2.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId1);

                var dtOrder3 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId3 = AddOrder(context, 3, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder3.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId2);

                var addressService = new AddressService("jNoFfAuChUSW46rMEDspKQ9178");

                var orderService = new OrderAssignmentService(context, 50, 120, addressService);
                //Act
                var newOrderDt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 14, 0, 0, DateTimeKind.Local).AddDays(1);
                var isAvailibleTime = orderService.IsAssignmentPossible(new List<db.OrderService>(new db.OrderService[] { new db.OrderService() { Quantity = 1, Service = service1 } }), newOrderDt.ToUniversalTime());

                var newOrder = AddOrder(context, 5, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, newOrderDt.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), Guid.Empty);

                var spResult = orderService.AssignOrder(newOrder);

                //Assert
                Assert.AreEqual(true, isAvailibleTime);
                Assert.AreEqual(false, spResult.IsBroadcasted);
                Assert.AreEqual(true, spResult.IsAssigned);
                Assert.AreEqual(serviceProviderId2, spResult.ServiceProvider.Id);
            }
        }

        //order type: any
        //order amount: cheap
        //availible timeslots: yes
        //first sp: contracted, has one order, experience higher
        //second sp: contracted, has one order, experience lower
        //third sp: special, has no orders
        [TestMethod]
        public void TestMethod1121()
        {
            using (var context = new DataContext(_options))
            {
                //Arrange
                var service1 = context.Services.Where(s => s.Title == "Basic Manicure").First();
                var service2 = context.Services.Where(s => s.Title == "Luxury Manicure").First();

                var customerId1 = AddCustomer(context, "", "+44", "", "cust1", "cust1", "cust1@cust.com");
                var customerId2 = AddCustomer(context, "", "+44", "", "cust2", "cust2", "cust2@cust.com");

                var serviceProviderId1 = AddServiceProvider(context, "SE10DH", "+44", "11111111111111", "111", "111", "111@111.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 2, 1, 1, new List<Guid>());
                var serviceProviderId2 = AddServiceProvider(context, "SE10DH", "+44", "22222222222222", "222", "222", "222@222.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 1, 1, 1, new List<Guid>());
                var serviceProviderId3 = AddServiceProvider(context, "SE10DH", "+44", "33333333333333", "333", "333", "333@333.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.ManiPedi, 1, 1, 1, new List<Guid>());

                var dtOrder1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId1 = AddOrder(context, 1, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder1.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId1);

                var dtOrder3 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId3 = AddOrder(context, 3, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder3.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId2);

                var addressService = new AddressService("jNoFfAuChUSW46rMEDspKQ9178");

                var orderService = new OrderAssignmentService(context, 50, 120, addressService);
                //Act
                var newOrderDt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 14, 0, 0, DateTimeKind.Local).AddDays(1);
                var isAvailibleTime = orderService.IsAssignmentPossible(new List<db.OrderService>(new db.OrderService[] { new db.OrderService() { Quantity = 1, Service = service1 } }), newOrderDt.ToUniversalTime());

                var newOrder = AddOrder(context, 5, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, newOrderDt.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), Guid.Empty);

                var spResult = orderService.AssignOrder(newOrder);

                //Assert
                Assert.AreEqual(true, isAvailibleTime);
                Assert.AreEqual(false, spResult.IsBroadcasted);
                Assert.AreEqual(true, spResult.IsAssigned);
                Assert.AreEqual(serviceProviderId1, spResult.ServiceProvider.Id);
            }
        }

        //order type: any
        //order amount: cheap
        //availible timeslots: yes
        //first sp: contracted, has one order, experience equal, rating lower
        //second sp: contracted, has one order, experience equal, rating higher
        //third sp: special, has no orders
        [TestMethod]
        public void TestMethod1131()
        {
            using (var context = new DataContext(_options))
            {
                //Arrange
                var service1 = context.Services.Where(s => s.Title == "Basic Manicure").First();
                var service2 = context.Services.Where(s => s.Title == "Luxury Manicure").First();

                var customerId1 = AddCustomer(context, "", "+44", "", "cust1", "cust1", "cust1@cust.com");
                var customerId2 = AddCustomer(context, "", "+44", "", "cust2", "cust2", "cust2@cust.com");

                var serviceProviderId1 = AddServiceProvider(context, "SE10DH", "+44", "11111111111111", "111", "111", "111@111.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 2, 1, 3, new List<Guid>());
                var serviceProviderId2 = AddServiceProvider(context, "SE10DH", "+44", "22222222222222", "222", "222", "222@222.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 2, 1, 4, new List<Guid>());
                var serviceProviderId3 = AddServiceProvider(context, "SE10DH", "+44", "33333333333333", "333", "333", "333@333.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.ManiPedi, 1, 1, 1, new List<Guid>());

                var dtOrder1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId1 = AddOrder(context, 1, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder1.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId1);

                var dtOrder3 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId3 = AddOrder(context, 3, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder3.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId2);

                var addressService = new AddressService("jNoFfAuChUSW46rMEDspKQ9178");

                var orderService = new OrderAssignmentService(context, 50, 120, addressService);
                //Act
                var newOrderDt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 14, 0, 0, DateTimeKind.Local).AddDays(1);
                var isAvailibleTime = orderService.IsAssignmentPossible(new List<db.OrderService>(new db.OrderService[] { new db.OrderService() { Quantity = 1, Service = service1 } }), newOrderDt.ToUniversalTime());

                var newOrder = AddOrder(context, 5, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, newOrderDt.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), Guid.Empty);

                var spResult = orderService.AssignOrder(newOrder);

                //Assert
                Assert.AreEqual(true, isAvailibleTime);
                Assert.AreEqual(false, spResult.IsBroadcasted);
                Assert.AreEqual(true, spResult.IsAssigned);
                Assert.AreEqual(serviceProviderId2, spResult.ServiceProvider.Id);
            }
        }

        //order type: any
        //order amount: cheap
        //availible timeslots: yes
        //first sp: contracted, has one order, experience equal, rating lower
        //second sp: contracted, has one order, experience equal, rating higher
        //third sp: special, has no orders
        [TestMethod]
        public void TestMethod1141()
        {
            using (var context = new DataContext(_options))
            {
                //Arrange
                var service1 = context.Services.Where(s => s.Title == "Basic Manicure").First();
                var service2 = context.Services.Where(s => s.Title == "Luxury Manicure").First();

                var customerId1 = AddCustomer(context, "", "+44", "", "cust1", "cust1", "cust1@cust.com");
                var customerId2 = AddCustomer(context, "", "+44", "", "cust2", "cust2", "cust2@cust.com");

                var serviceProviderId1 = AddServiceProvider(context, "SE10DH", "+44", "11111111111111", "111", "111", "111@111.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 2, 1, 3, new List<Guid>());
                var serviceProviderId2 = AddServiceProvider(context, "W25BE", "+44", "22222222222222", "222", "222", "222@222.com", "36 Chepstow Rd, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 2, 1, 3, new List<Guid>());
                var serviceProviderId3 = AddServiceProvider(context, "SE10DH", "+44", "33333333333333", "333", "333", "333@333.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.ManiPedi, 1, 1, 1, new List<Guid>());

                var dtOrder1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId1 = AddOrder(context, 1, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder1.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId1);

                var dtOrder3 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId3 = AddOrder(context, 3, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder3.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId2);

                var addressService = new AddressService("jNoFfAuChUSW46rMEDspKQ9178");

                var orderService = new OrderAssignmentService(context, 50, 120, addressService);
                //Act
                var newOrderDt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 14, 0, 0, DateTimeKind.Local).AddDays(1);
                var isAvailibleTime = orderService.IsAssignmentPossible(new List<db.OrderService>(new db.OrderService[] { new db.OrderService() { Quantity = 1, Service = service1 } }), newOrderDt.ToUniversalTime());

                var newOrder = AddOrder(context, 5, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, newOrderDt.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), Guid.Empty);

                var spResult = orderService.AssignOrder(newOrder);

                //Assert
                Assert.AreEqual(true, isAvailibleTime);
                Assert.AreEqual(false, spResult.IsBroadcasted);
                Assert.AreEqual(true, spResult.IsAssigned);
                Assert.AreEqual(serviceProviderId1, spResult.ServiceProvider.Id);
            }
        }

        //order type: any
        //order amount: cheap
        //availible timeslots: no
        [TestMethod]
        public void TestMethod12()
        {
            using (var context = new DataContext(_options))
            {
                //Arrange
                var service1 = context.Services.Where(s => s.Title == "Basic Manicure").First();
                var service2 = context.Services.Where(s => s.Title == "Luxury Manicure").First();

                var customerId1 = AddCustomer(context, "", "+44", "", "cust1", "cust1", "cust1@cust.com");
                var customerId2 = AddCustomer(context, "", "+44", "", "cust2", "cust2", "cust2@cust.com");

                var serviceProviderId1 = AddServiceProvider(context, "SE10DH", "+44", "11111111111111", "111", "111", "111@111.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 1, 1, 1, new List<Guid>());
                var serviceProviderId2 = AddServiceProvider(context, "SE10DH", "+44", "22222222222222", "222", "222", "222@222.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 1, 1, 1, new List<Guid>());

                var dtOrder1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId1 = AddOrder(context, 1, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder1.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId1);

                var dtOrder2 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId2 = AddOrder(context, 2, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder2.ToUniversalTime(), customerId2, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId2);

                var addressService = new AddressService("jNoFfAuChUSW46rMEDspKQ9178");

                var orderService = new OrderAssignmentService(context, 50, 120, addressService);
                //Act
                var newOrderDt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var isAvailibleTime = orderService.IsAssignmentPossible(new List<db.OrderService>(new db.OrderService[] { new db.OrderService() { Quantity = 1, Service = service1 } }), newOrderDt.ToUniversalTime());

                //Assert
                Assert.AreEqual(false, isAvailibleTime);
            }
        }
        #endregion

        #region expensive, mixed order type
        //order type: mixed
        //order amount: expensive
        //availible timeslots: yes
        //first sp: contracted, already has two orders
        //second sp: contracted, already has two orders
        //third sp: special, has no orders
        [TestMethod]
        public void TestMethod21111()
        {
            using (var context = new DataContext(_options))
            {
                //Arrange
                var service1 = context.Services.Where(s => s.Title == "Basic Manicure").First();
                var service2 = context.Services.Where(s => s.Title == "Full Legs").First();

                var customerId1 = AddCustomer(context, "", "+44", "", "cust1", "cust1", "cust1@cust.com");
                var customerId2 = AddCustomer(context, "", "+44", "", "cust2", "cust2", "cust2@cust.com");

                var serviceProviderId1 = AddServiceProvider(context, "SE10DH", "+44", "11111111111111", "111", "111", "111@111.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 1, 1, 1, new List<Guid>());
                var serviceProviderId2 = AddServiceProvider(context, "SE10DH", "+44", "22222222222222", "222", "222", "222@222.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 1, 1, 1, new List<Guid>());
                var serviceProviderId3 = AddServiceProvider(context, "SE10DH", "+44", "33333333333333", "333", "333", "333@333.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.ManiPedi, 1, 1, 1, new List<Guid>());

                var dtOrder1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId1 = AddOrder(context, 1, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder1.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId1);
                var dtOrder2 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId2 = AddOrder(context, 2, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder2.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId1);

                var dtOrder3 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId3 = AddOrder(context, 3, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder3.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId2);
                var dtOrder4 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId4 = AddOrder(context, 4, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder4.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId2);

                var addressService = new AddressService("jNoFfAuChUSW46rMEDspKQ9178");

                var orderService = new OrderAssignmentService(context, 50, 120, addressService);
                //Act
                var newOrderDt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 14, 0, 0, DateTimeKind.Local).AddDays(1);
                var isAvailibleTime = orderService.IsAssignmentPossible(new List<db.OrderService>(new db.OrderService[] { new db.OrderService() { Quantity = 1, Service = service1 }, new db.OrderService() { Quantity = 1, Service = service2 } }), newOrderDt.ToUniversalTime());

                var newOrder = AddOrder(context, 5, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, newOrderDt.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1, service2 }), Guid.Empty);

                var spResult = orderService.AssignOrder(newOrder);

                //Assert
                Assert.AreEqual(true, isAvailibleTime);
                Assert.AreEqual(true, spResult.IsBroadcasted);
                Assert.AreEqual(false, spResult.IsAssigned);
                Assert.AreEqual(null, spResult.ServiceProvider);
            }
        }

        //order type: mixed
        //order amount: expensive
        //availible timeslots: yes
        //first sp: contracted, already has two orders
        //second sp: contracted, has one order
        //third sp: special, has no orders
        [TestMethod]
        public void TestMethod21112()
        {
            using (var context = new DataContext(_options))
            {
                //Arrange
                var service1 = context.Services.Where(s => s.Title == "Basic Manicure").First();
                var service2 = context.Services.Where(s => s.Title == "Full Legs").First();

                var customerId1 = AddCustomer(context, "", "+44", "", "cust1", "cust1", "cust1@cust.com");
                var customerId2 = AddCustomer(context, "", "+44", "", "cust2", "cust2", "cust2@cust.com");

                var serviceProviderId1 = AddServiceProvider(context, "SE10DH", "+44", "11111111111111", "111", "111", "111@111.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 1, 1, 1, new List<Guid>());
                var serviceProviderId2 = AddServiceProvider(context, "SE10DH", "+44", "22222222222222", "222", "222", "222@222.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 1, 1, 1, new List<Guid>());
                var serviceProviderId3 = AddServiceProvider(context, "SE10DH", "+44", "33333333333333", "333", "333", "333@333.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.ManiPedi, 1, 1, 1, new List<Guid>());

                var dtOrder1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId1 = AddOrder(context, 1, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder1.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId1);
                var dtOrder2 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 17, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId2 = AddOrder(context, 2, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder2.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId1);

                var dtOrder3 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId3 = AddOrder(context, 3, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder3.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId2);

                var addressService = new AddressService("jNoFfAuChUSW46rMEDspKQ9178");

                var orderService = new OrderAssignmentService(context, 50, 120, addressService);
                //Act
                var newOrderDt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 14, 0, 0, DateTimeKind.Local).AddDays(1);
                var isAvailibleTime = orderService.IsAssignmentPossible(new List<db.OrderService>(new db.OrderService[] { new db.OrderService() { Quantity = 1, Service = service1 }, new db.OrderService() { Quantity = 1, Service = service2 } }), newOrderDt.ToUniversalTime());

                var newOrder = AddOrder(context, 5, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, newOrderDt.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1, service2 }), Guid.Empty);

                var spResult = orderService.AssignOrder(newOrder);

                //Assert
                Assert.AreEqual(true, isAvailibleTime);
                Assert.AreEqual(false, spResult.IsBroadcasted);
                Assert.AreEqual(true, spResult.IsAssigned);
                Assert.AreEqual(serviceProviderId2, spResult.ServiceProvider.Id);
            }
        }

        //order type: mixed
        //order amount: expensive
        //availible timeslots: yes
        //first sp: contracted, has one order, experience higher
        //second sp: contracted, has one order, experience lower
        //third sp: special, has no orders
        [TestMethod]
        public void TestMethod21121()
        {
            using (var context = new DataContext(_options))
            {
                //Arrange
                var service1 = context.Services.Where(s => s.Title == "Basic Manicure").First();
                var service2 = context.Services.Where(s => s.Title == "Full Legs").First();

                var customerId1 = AddCustomer(context, "", "+44", "", "cust1", "cust1", "cust1@cust.com");
                var customerId2 = AddCustomer(context, "", "+44", "", "cust2", "cust2", "cust2@cust.com");

                var serviceProviderId1 = AddServiceProvider(context, "SE10DH", "+44", "11111111111111", "111", "111", "111@111.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 2, 1, 1, new List<Guid>());
                var serviceProviderId2 = AddServiceProvider(context, "SE10DH", "+44", "22222222222222", "222", "222", "222@222.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 1, 1, 1, new List<Guid>());
                var serviceProviderId3 = AddServiceProvider(context, "SE10DH", "+44", "33333333333333", "333", "333", "333@333.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.ManiPedi, 1, 1, 1, new List<Guid>());

                var dtOrder1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId1 = AddOrder(context, 1, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder1.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId1);

                var dtOrder3 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId3 = AddOrder(context, 3, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder3.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId2);

                var addressService = new AddressService("jNoFfAuChUSW46rMEDspKQ9178");

                var orderService = new OrderAssignmentService(context, 50, 120, addressService);
                //Act
                var newOrderDt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 14, 0, 0, DateTimeKind.Local).AddDays(1);
                var isAvailibleTime = orderService.IsAssignmentPossible(new List<db.OrderService>(new db.OrderService[] { new db.OrderService() { Quantity = 1, Service = service1 }, new db.OrderService() { Quantity = 1, Service = service2 } }), newOrderDt.ToUniversalTime());

                var newOrder = AddOrder(context, 5, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, newOrderDt.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1, service2 }), Guid.Empty);

                var spResult = orderService.AssignOrder(newOrder);

                //Assert
                Assert.AreEqual(true, isAvailibleTime);
                Assert.AreEqual(false, spResult.IsBroadcasted);
                Assert.AreEqual(true, spResult.IsAssigned);
                Assert.AreEqual(serviceProviderId1, spResult.ServiceProvider.Id);
            }
        }

        //order type: mixed
        //order amount: expensive
        //availible timeslots: yes
        //first sp: contracted, has one order, experience equal, rating lower
        //second sp: contracted, has one order, experience equal, rating higher
        //third sp: special, has no orders
        [TestMethod]
        public void TestMethod21131()
        {
            using (var context = new DataContext(_options))
            {
                //Arrange
                var service1 = context.Services.Where(s => s.Title == "Basic Manicure").First();
                var service2 = context.Services.Where(s => s.Title == "Full Legs").First();

                var customerId1 = AddCustomer(context, "", "+44", "", "cust1", "cust1", "cust1@cust.com");
                var customerId2 = AddCustomer(context, "", "+44", "", "cust2", "cust2", "cust2@cust.com");

                var serviceProviderId1 = AddServiceProvider(context, "SE10DH", "+44", "11111111111111", "111", "111", "111@111.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 2, 1, 3, new List<Guid>());
                var serviceProviderId2 = AddServiceProvider(context, "SE10DH", "+44", "22222222222222", "222", "222", "222@222.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 2, 1, 4, new List<Guid>());
                var serviceProviderId3 = AddServiceProvider(context, "SE10DH", "+44", "33333333333333", "333", "333", "333@333.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.ManiPedi, 1, 1, 1, new List<Guid>());

                var dtOrder1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId1 = AddOrder(context, 1, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder1.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId1);

                var dtOrder3 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId3 = AddOrder(context, 3, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder3.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId2);

                var addressService = new AddressService("jNoFfAuChUSW46rMEDspKQ9178");

                var orderService = new OrderAssignmentService(context, 50, 120, addressService);
                //Act
                var newOrderDt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 14, 0, 0, DateTimeKind.Local).AddDays(1);
                var isAvailibleTime = orderService.IsAssignmentPossible(new List<db.OrderService>(new db.OrderService[] { new db.OrderService() { Quantity = 1, Service = service1 }, new db.OrderService() { Quantity = 1, Service = service2 } }), newOrderDt.ToUniversalTime());

                var newOrder = AddOrder(context, 5, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, newOrderDt.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1, service2 }), Guid.Empty);

                var spResult = orderService.AssignOrder(newOrder);

                //Assert
                Assert.AreEqual(true, isAvailibleTime);
                Assert.AreEqual(false, spResult.IsBroadcasted);
                Assert.AreEqual(true, spResult.IsAssigned);
                Assert.AreEqual(serviceProviderId2, spResult.ServiceProvider.Id);
            }
        }

        //order type: mixed
        //order amount: expensive
        //availible timeslots: yes
        //first sp: contracted, has one order, experience equal, rating lower
        //second sp: contracted, has one order, experience equal, rating higher
        //third sp: special, has no orders
        [TestMethod]
        public void TestMethod21141()
        {
            using (var context = new DataContext(_options))
            {
                //Arrange
                var service1 = context.Services.Where(s => s.Title == "Basic Manicure").First();
                var service2 = context.Services.Where(s => s.Title == "Full Legs").First();

                var customerId1 = AddCustomer(context, "", "+44", "", "cust1", "cust1", "cust1@cust.com");
                var customerId2 = AddCustomer(context, "", "+44", "", "cust2", "cust2", "cust2@cust.com");

                var serviceProviderId1 = AddServiceProvider(context, "SE10DH", "+44", "11111111111111", "111", "111", "111@111.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 2, 1, 3, new List<Guid>());
                var serviceProviderId2 = AddServiceProvider(context, "W25BE", "+44", "22222222222222", "222", "222", "222@222.com", "36 Chepstow Rd, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 2, 1, 3, new List<Guid>());
                var serviceProviderId3 = AddServiceProvider(context, "SE10DH", "+44", "33333333333333", "333", "333", "333@333.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.ManiPedi, 1, 1, 1, new List<Guid>());

                var dtOrder1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId1 = AddOrder(context, 1, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder1.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId1);

                var dtOrder3 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId3 = AddOrder(context, 3, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder3.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId2);

                var addressService = new AddressService("jNoFfAuChUSW46rMEDspKQ9178");

                var orderService = new OrderAssignmentService(context, 50, 120, addressService);
                //Act
                var newOrderDt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 14, 0, 0, DateTimeKind.Local).AddDays(1);
                var isAvailibleTime = orderService.IsAssignmentPossible(new List<db.OrderService>(new db.OrderService[] { new db.OrderService() { Quantity = 1, Service = service1 }, new db.OrderService() { Quantity = 1, Service = service2 } }), newOrderDt.ToUniversalTime());

                var newOrder = AddOrder(context, 5, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, newOrderDt.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1, service2 }), Guid.Empty);

                var spResult = orderService.AssignOrder(newOrder);

                //Assert
                Assert.AreEqual(true, isAvailibleTime);
                Assert.AreEqual(false, spResult.IsBroadcasted);
                Assert.AreEqual(true, spResult.IsAssigned);
                Assert.AreEqual(serviceProviderId1, spResult.ServiceProvider.Id);
            }
        }

        //order type: mixed
        //order amount: expensive
        //availible timeslots: no
        [TestMethod]
        public void TestMethod212()
        {
            using (var context = new DataContext(_options))
            {
                //Arrange
                var service1 = context.Services.Where(s => s.Title == "Basic Manicure").First();
                var service2 = context.Services.Where(s => s.Title == "Full Legs").First();

                var customerId1 = AddCustomer(context, "", "+44", "", "cust1", "cust1", "cust1@cust.com");
                var customerId2 = AddCustomer(context, "", "+44", "", "cust2", "cust2", "cust2@cust.com");

                var serviceProviderId1 = AddServiceProvider(context, "SE10DH", "+44", "11111111111111", "111", "111", "111@111.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 1, 1, 1, new List<Guid>());
                var serviceProviderId2 = AddServiceProvider(context, "SE10DH", "+44", "22222222222222", "222", "222", "222@222.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 1, 1, 1, new List<Guid>());

                var dtOrder1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId1 = AddOrder(context, 1, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder1.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId1);

                var dtOrder2 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId2 = AddOrder(context, 2, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder2.ToUniversalTime(), customerId2, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId2);

                var addressService = new AddressService("jNoFfAuChUSW46rMEDspKQ9178");

                var orderService = new OrderAssignmentService(context, 50, 120, addressService);
                //Act
                var newOrderDt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var isAvailibleTime = orderService.IsAssignmentPossible(new List<db.OrderService>(new db.OrderService[] { new db.OrderService() { Quantity = 1, Service = service1 }, new db.OrderService() { Quantity = 1, Service = service2 } }), newOrderDt.ToUniversalTime());

                //Assert
                Assert.AreEqual(false, isAvailibleTime);
            }
        }
        #endregion

        #region expensive, not mixed order type, only contracted sp timeslots exists
        //order type: not mixed
        //order amount: expensive
        //availible timeslots: no
        //first sp: contracted, already has two orders
        //second sp: contracted, already has two orders
        [TestMethod]
        public void TestMethod22111()
        {
            using (var context = new DataContext(_options))
            {
                //Arrange
                var service1 = context.Services.Where(s => s.Title == "Luxury Gel Pedicure").First();
                var service2 = context.Services.Where(s => s.Title == "Normal Mani-Padi").First();

                var customerId1 = AddCustomer(context, "", "+44", "", "cust1", "cust1", "cust1@cust.com");
                var customerId2 = AddCustomer(context, "", "+44", "", "cust2", "cust2", "cust2@cust.com");

                var serviceProviderId1 = AddServiceProvider(context, "SE10DH", "+44", "11111111111111", "111", "111", "111@111.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 1, 1, 1, new List<Guid>());
                var serviceProviderId2 = AddServiceProvider(context, "SE10DH", "+44", "22222222222222", "222", "222", "222@222.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 1, 1, 1, new List<Guid>());

                var dtOrder1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId1 = AddOrder(context, 1, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder1.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId1);
                var dtOrder2 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId2 = AddOrder(context, 2, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder2.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId1);

                var dtOrder3 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId3 = AddOrder(context, 3, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder3.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId2);
                var dtOrder4 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId4 = AddOrder(context, 4, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder4.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId2);

                var addressService = new AddressService("jNoFfAuChUSW46rMEDspKQ9178");

                var orderService = new OrderAssignmentService(context, 50, 120, addressService);
                //Act
                var newOrderDt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 14, 0, 0, DateTimeKind.Local).AddDays(1);
                var isAvailibleTime = orderService.IsAssignmentPossible(new List<db.OrderService>(new db.OrderService[] { new db.OrderService() { Quantity = 1, Service = service1 }}), newOrderDt.ToUniversalTime());

                var newOrder = AddOrder(context, 5, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, newOrderDt.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), Guid.Empty);

                var spResult = orderService.AssignOrder(newOrder);

                //Assert
                Assert.AreEqual(true, isAvailibleTime);
                Assert.AreEqual(true, spResult.IsBroadcasted);
                Assert.AreEqual(false, spResult.IsAssigned);
                Assert.AreEqual(null, spResult.ServiceProvider);
            }
        }

        //order type: not mixed
        //order amount: expensive
        //availible timeslots: yes
        //first sp: contracted, already has two orders
        //second sp: contracted, has one order
        [TestMethod]
        public void TestMethod22112()
        {
            using (var context = new DataContext(_options))
            {
                //Arrange
                var service1 = context.Services.Where(s => s.Title == "Luxury Gel Pedicure").First();
                var service2 = context.Services.Where(s => s.Title == "Normal Mani-Padi").First();

                var customerId1 = AddCustomer(context, "", "+44", "", "cust1", "cust1", "cust1@cust.com");
                var customerId2 = AddCustomer(context, "", "+44", "", "cust2", "cust2", "cust2@cust.com");

                var serviceProviderId1 = AddServiceProvider(context, "SE10DH", "+44", "11111111111111", "111", "111", "111@111.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 1, 1, 1, new List<Guid>());
                var serviceProviderId2 = AddServiceProvider(context, "SE10DH", "+44", "22222222222222", "222", "222", "222@222.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 1, 1, 1, new List<Guid>());

                var dtOrder1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId1 = AddOrder(context, 1, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder1.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId1);
                var dtOrder2 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 17, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId2 = AddOrder(context, 2, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder2.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId1);

                var dtOrder3 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId3 = AddOrder(context, 3, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder3.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId2);

                var addressService = new AddressService("jNoFfAuChUSW46rMEDspKQ9178");

                var orderService = new OrderAssignmentService(context, 50, 120, addressService);
                //Act
                var newOrderDt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 14, 0, 0, DateTimeKind.Local).AddDays(1);
                var isAvailibleTime = orderService.IsAssignmentPossible(new List<db.OrderService>(new db.OrderService[] { new db.OrderService() { Quantity = 1, Service = service1 }}), newOrderDt.ToUniversalTime());

                var newOrder = AddOrder(context, 5, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, newOrderDt.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), Guid.Empty);

                var spResult = orderService.AssignOrder(newOrder);

                //Assert
                Assert.AreEqual(true, isAvailibleTime);
                Assert.AreEqual(false, spResult.IsBroadcasted);
                Assert.AreEqual(true, spResult.IsAssigned);
                Assert.AreEqual(serviceProviderId2, spResult.ServiceProvider.Id);
            }
        }

        //order type: mixed
        //order amount: expensive
        //availible timeslots: yes
        //first sp: contracted, has one order, experience higher
        //second sp: contracted, has one order, experience lower
        [TestMethod]
        public void TestMethod22121()
        {
            using (var context = new DataContext(_options))
            {
                //Arrange
                var service1 = context.Services.Where(s => s.Title == "Luxury Gel Pedicure").First();
                var service2 = context.Services.Where(s => s.Title == "Normal Mani-Padi").First();

                var customerId1 = AddCustomer(context, "", "+44", "", "cust1", "cust1", "cust1@cust.com");
                var customerId2 = AddCustomer(context, "", "+44", "", "cust2", "cust2", "cust2@cust.com");

                var serviceProviderId1 = AddServiceProvider(context, "SE10DH", "+44", "11111111111111", "111", "111", "111@111.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 2, 1, 1, new List<Guid>());
                var serviceProviderId2 = AddServiceProvider(context, "SE10DH", "+44", "22222222222222", "222", "222", "222@222.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 1, 1, 1, new List<Guid>());

                var dtOrder1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId1 = AddOrder(context, 1, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder1.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId1);

                var dtOrder3 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId3 = AddOrder(context, 3, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder3.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId2);

                var addressService = new AddressService("jNoFfAuChUSW46rMEDspKQ9178");

                var orderService = new OrderAssignmentService(context, 50, 120, addressService);
                //Act
                var newOrderDt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 14, 0, 0, DateTimeKind.Local).AddDays(1);
                var isAvailibleTime = orderService.IsAssignmentPossible(new List<db.OrderService>(new db.OrderService[] { new db.OrderService() { Quantity = 1, Service = service1 }}), newOrderDt.ToUniversalTime());

                var newOrder = AddOrder(context, 5, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, newOrderDt.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), Guid.Empty);

                var spResult = orderService.AssignOrder(newOrder);

                //Assert
                Assert.AreEqual(true, isAvailibleTime);
                Assert.AreEqual(false, spResult.IsBroadcasted);
                Assert.AreEqual(true, spResult.IsAssigned);
                Assert.AreEqual(serviceProviderId1, spResult.ServiceProvider.Id);
            }
        }

        //order type: mixed
        //order amount: expensive
        //availible timeslots: yes
        //first sp: contracted, has one order, experience equal, rating lower
        //second sp: contracted, has one order, experience equal, rating higher
        [TestMethod]
        public void TestMethod22131()
        {
            using (var context = new DataContext(_options))
            {
                //Arrange
                var service1 = context.Services.Where(s => s.Title == "Luxury Gel Pedicure").First();
                var service2 = context.Services.Where(s => s.Title == "Normal Mani-Padi").First();

                var customerId1 = AddCustomer(context, "", "+44", "", "cust1", "cust1", "cust1@cust.com");
                var customerId2 = AddCustomer(context, "", "+44", "", "cust2", "cust2", "cust2@cust.com");

                var serviceProviderId1 = AddServiceProvider(context, "SE10DH", "+44", "11111111111111", "111", "111", "111@111.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 2, 1, 3, new List<Guid>());
                var serviceProviderId2 = AddServiceProvider(context, "SE10DH", "+44", "22222222222222", "222", "222", "222@222.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 2, 1, 4, new List<Guid>());

                var dtOrder1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId1 = AddOrder(context, 1, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder1.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId1);

                var dtOrder3 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId3 = AddOrder(context, 3, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder3.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId2);

                var addressService = new AddressService("jNoFfAuChUSW46rMEDspKQ9178");

                var orderService = new OrderAssignmentService(context, 50, 120, addressService);
                //Act
                var newOrderDt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 14, 0, 0, DateTimeKind.Local).AddDays(1);
                var isAvailibleTime = orderService.IsAssignmentPossible(new List<db.OrderService>(new db.OrderService[] { new db.OrderService() { Quantity = 1, Service = service1 } }), newOrderDt.ToUniversalTime());

                var newOrder = AddOrder(context, 5, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, newOrderDt.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), Guid.Empty);

                var spResult = orderService.AssignOrder(newOrder);

                //Assert
                Assert.AreEqual(true, isAvailibleTime);
                Assert.AreEqual(false, spResult.IsBroadcasted);
                Assert.AreEqual(true, spResult.IsAssigned);
                Assert.AreEqual(serviceProviderId2, spResult.ServiceProvider.Id);
            }
        }

        //order type: mixed
        //order amount: expensive
        //availible timeslots: yes
        //first sp: contracted, has one order, experience equal, rating equal, living closer to order
        //second sp: contracted, has one order, experience equal, rating equal, living far from order
        [TestMethod]
        public void TestMethod22141()
        {
            using (var context = new DataContext(_options))
            {
                //Arrange
                var service1 = context.Services.Where(s => s.Title == "Luxury Gel Pedicure").First();
                var service2 = context.Services.Where(s => s.Title == "Normal Mani-Padi").First();

                var customerId1 = AddCustomer(context, "", "+44", "", "cust1", "cust1", "cust1@cust.com");
                var customerId2 = AddCustomer(context, "", "+44", "", "cust2", "cust2", "cust2@cust.com");

                var serviceProviderId1 = AddServiceProvider(context, "SE10DH", "+44", "11111111111111", "111", "111", "111@111.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 2, 1, 3, new List<Guid>());
                var serviceProviderId2 = AddServiceProvider(context, "W25BE", "+44", "22222222222222", "222", "222", "222@222.com", "36 Chepstow Rd, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 2, 1, 3, new List<Guid>());

                var dtOrder1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId1 = AddOrder(context, 1, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder1.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId1);

                var dtOrder3 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId3 = AddOrder(context, 3, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder3.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId2);

                var addressService = new AddressService("jNoFfAuChUSW46rMEDspKQ9178");

                var orderService = new OrderAssignmentService(context, 50, 120, addressService);
                //Act
                var newOrderDt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 14, 0, 0, DateTimeKind.Local).AddDays(1);
                var isAvailibleTime = orderService.IsAssignmentPossible(new List<db.OrderService>(new db.OrderService[] { new db.OrderService() { Quantity = 1, Service = service1 } }), newOrderDt.ToUniversalTime());

                var newOrder = AddOrder(context, 5, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, newOrderDt.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), Guid.Empty);

                var spResult = orderService.AssignOrder(newOrder);

                //Assert
                Assert.AreEqual(true, isAvailibleTime);
                Assert.AreEqual(false, spResult.IsBroadcasted);
                Assert.AreEqual(true, spResult.IsAssigned);
                Assert.AreEqual(serviceProviderId1, spResult.ServiceProvider.Id);
            }
        }

        #endregion

        #region expensive, not mixed order type, only noncontracted sp timeslots exists
        //order type: not mixed
        //order amount: expensive
        //availible timeslots: no
        //first sp: noncontracted, already has two orders
        //second sp: noncontracted, already has two orders
        [TestMethod]
        public void TestMethod22211()
        {
            using (var context = new DataContext(_options))
            {
                //Arrange
                var service1 = context.Services.Where(s => s.Title == "Luxury Gel Pedicure").First();
                var service2 = context.Services.Where(s => s.Title == "Normal Mani-Padi").First();

                var customerId1 = AddCustomer(context, "", "+44", "", "cust1", "cust1", "cust1@cust.com");
                var customerId2 = AddCustomer(context, "", "+44", "", "cust2", "cust2", "cust2@cust.com");

                var serviceProviderId1 = AddServiceProvider(context, "SE10DH", "+44", "11111111111111", "111", "111", "111@111.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.ManiPedi, 1, 1, 1, new List<Guid>());
                var serviceProviderId2 = AddServiceProvider(context, "SE10DH", "+44", "22222222222222", "222", "222", "222@222.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.ManiPedi, 1, 1, 1, new List<Guid>());

                var dtOrder1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId1 = AddOrder(context, 1, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder1.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId1);
                var dtOrder2 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId2 = AddOrder(context, 2, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder2.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId1);

                var dtOrder3 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId3 = AddOrder(context, 3, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder3.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId2);
                var dtOrder4 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId4 = AddOrder(context, 4, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder4.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId2);

                var addressService = new AddressService("jNoFfAuChUSW46rMEDspKQ9178");

                var orderService = new OrderAssignmentService(context, 50, 120, addressService);
                //Act
                var newOrderDt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 14, 0, 0, DateTimeKind.Local).AddDays(1);
                var isAvailibleTime = orderService.IsAssignmentPossible(new List<db.OrderService>(new db.OrderService[] { new db.OrderService() { Quantity = 1, Service = service1 } }), newOrderDt.ToUniversalTime());

                var newOrder = AddOrder(context, 5, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, newOrderDt.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), Guid.Empty);

                var spResult = orderService.AssignOrder(newOrder);

                //Assert
                Assert.AreEqual(true, isAvailibleTime);
                Assert.AreEqual(true, spResult.IsBroadcasted);
                Assert.AreEqual(false, spResult.IsAssigned);
                Assert.AreEqual(null, spResult.ServiceProvider);
            }
        }

        //order type: not mixed
        //order amount: expensive
        //availible timeslots: yes
        //first sp: noncontracted, already has two orders
        //second sp: noncontracted, has one order
        [TestMethod]
        public void TestMethod22212()
        {
            using (var context = new DataContext(_options))
            {
                //Arrange
                var service1 = context.Services.Where(s => s.Title == "Luxury Gel Pedicure").First();
                var service2 = context.Services.Where(s => s.Title == "Normal Mani-Padi").First();

                var customerId1 = AddCustomer(context, "", "+44", "", "cust1", "cust1", "cust1@cust.com");
                var customerId2 = AddCustomer(context, "", "+44", "", "cust2", "cust2", "cust2@cust.com");

                var serviceProviderId1 = AddServiceProvider(context, "SE10DH", "+44", "11111111111111", "111", "111", "111@111.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.ManiPedi, 1, 1, 1, new List<Guid>());
                var serviceProviderId2 = AddServiceProvider(context, "SE10DH", "+44", "22222222222222", "222", "222", "222@222.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.ManiPedi, 1, 1, 1, new List<Guid>());

                var dtOrder1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId1 = AddOrder(context, 1, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder1.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId1);
                var dtOrder2 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 17, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId2 = AddOrder(context, 2, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder2.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId1);

                var dtOrder3 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId3 = AddOrder(context, 3, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder3.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId2);

                var addressService = new AddressService("jNoFfAuChUSW46rMEDspKQ9178");

                var orderService = new OrderAssignmentService(context, 50, 120, addressService);
                //Act
                var newOrderDt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 14, 0, 0, DateTimeKind.Local).AddDays(1);
                var isAvailibleTime = orderService.IsAssignmentPossible(new List<db.OrderService>(new db.OrderService[] { new db.OrderService() { Quantity = 1, Service = service1 } }), newOrderDt.ToUniversalTime());

                var newOrder = AddOrder(context, 5, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, newOrderDt.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), Guid.Empty);

                var spResult = orderService.AssignOrder(newOrder);

                //Assert
                Assert.AreEqual(true, isAvailibleTime);
                Assert.AreEqual(false, spResult.IsBroadcasted);
                Assert.AreEqual(true, spResult.IsAssigned);
                Assert.AreEqual(serviceProviderId2, spResult.ServiceProvider.Id);
            }
        }

        //order type: mixed
        //order amount: expensive
        //availible timeslots: yes
        //first sp: noncontracted, has one order, experience higher
        //second sp: noncontracted, has one order, experience lower
        [TestMethod]
        public void TestMethod22221()
        {
            using (var context = new DataContext(_options))
            {
                //Arrange
                var service1 = context.Services.Where(s => s.Title == "Luxury Gel Pedicure").First();
                var service2 = context.Services.Where(s => s.Title == "Normal Mani-Padi").First();

                var customerId1 = AddCustomer(context, "", "+44", "", "cust1", "cust1", "cust1@cust.com");
                var customerId2 = AddCustomer(context, "", "+44", "", "cust2", "cust2", "cust2@cust.com");

                var serviceProviderId1 = AddServiceProvider(context, "SE10DH", "+44", "11111111111111", "111", "111", "111@111.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.ManiPedi, 2, 1, 1, new List<Guid>());
                var serviceProviderId2 = AddServiceProvider(context, "SE10DH", "+44", "22222222222222", "222", "222", "222@222.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.ManiPedi, 1, 1, 1, new List<Guid>());

                var dtOrder1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId1 = AddOrder(context, 1, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder1.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId1);

                var dtOrder3 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId3 = AddOrder(context, 3, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder3.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId2);

                var addressService = new AddressService("jNoFfAuChUSW46rMEDspKQ9178");

                var orderService = new OrderAssignmentService(context, 50, 120, addressService);
                //Act
                var newOrderDt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 14, 0, 0, DateTimeKind.Local).AddDays(1);
                var isAvailibleTime = orderService.IsAssignmentPossible(new List<db.OrderService>(new db.OrderService[] { new db.OrderService() { Quantity = 1, Service = service1 } }), newOrderDt.ToUniversalTime());

                var newOrder = AddOrder(context, 5, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, newOrderDt.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), Guid.Empty);

                var spResult = orderService.AssignOrder(newOrder);

                //Assert
                Assert.AreEqual(true, isAvailibleTime);
                Assert.AreEqual(false, spResult.IsBroadcasted);
                Assert.AreEqual(true, spResult.IsAssigned);
                Assert.AreEqual(serviceProviderId1, spResult.ServiceProvider.Id);
            }
        }

        //order type: mixed
        //order amount: expensive
        //availible timeslots: yes
        //first sp: noncontracted, has one order, experience equal, rating lower
        //second sp: noncontracted, has one order, experience equal, rating higher
        [TestMethod]
        public void TestMethod22231()
        {
            using (var context = new DataContext(_options))
            {
                //Arrange
                var service1 = context.Services.Where(s => s.Title == "Luxury Gel Pedicure").First();
                var service2 = context.Services.Where(s => s.Title == "Normal Mani-Padi").First();

                var customerId1 = AddCustomer(context, "", "+44", "", "cust1", "cust1", "cust1@cust.com");
                var customerId2 = AddCustomer(context, "", "+44", "", "cust2", "cust2", "cust2@cust.com");

                var serviceProviderId1 = AddServiceProvider(context, "SE10DH", "+44", "11111111111111", "111", "111", "111@111.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.ManiPedi, 2, 1, 3, new List<Guid>());
                var serviceProviderId2 = AddServiceProvider(context, "SE10DH", "+44", "22222222222222", "222", "222", "222@222.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.ManiPedi, 2, 1, 4, new List<Guid>());

                var dtOrder1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId1 = AddOrder(context, 1, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder1.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId1);

                var dtOrder3 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId3 = AddOrder(context, 3, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder3.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId2);

                var addressService = new AddressService("jNoFfAuChUSW46rMEDspKQ9178");

                var orderService = new OrderAssignmentService(context, 50, 120, addressService);
                //Act
                var newOrderDt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 14, 0, 0, DateTimeKind.Local).AddDays(1);
                var isAvailibleTime = orderService.IsAssignmentPossible(new List<db.OrderService>(new db.OrderService[] { new db.OrderService() { Quantity = 1, Service = service1 } }), newOrderDt.ToUniversalTime());

                var newOrder = AddOrder(context, 5, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, newOrderDt.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), Guid.Empty);

                var spResult = orderService.AssignOrder(newOrder);

                //Assert
                Assert.AreEqual(true, isAvailibleTime);
                Assert.AreEqual(false, spResult.IsBroadcasted);
                Assert.AreEqual(true, spResult.IsAssigned);
                Assert.AreEqual(serviceProviderId2, spResult.ServiceProvider.Id);
            }
        }

        //order type: mixed
        //order amount: expensive
        //availible timeslots: yes
        //first sp: noncontracted, has one order, experience equal, rating equal, living closer to order
        //second sp: noncontracted, has one order, experience equal, rating equal, living far from order
        [TestMethod]
        public void TestMethod22241()
        {
            using (var context = new DataContext(_options))
            {
                //Arrange
                var service1 = context.Services.Where(s => s.Title == "Luxury Gel Pedicure").First();
                var service2 = context.Services.Where(s => s.Title == "Normal Mani-Padi").First();

                var customerId1 = AddCustomer(context, "", "+44", "", "cust1", "cust1", "cust1@cust.com");
                var customerId2 = AddCustomer(context, "", "+44", "", "cust2", "cust2", "cust2@cust.com");

                var serviceProviderId1 = AddServiceProvider(context, "SE10DH", "+44", "11111111111111", "111", "111", "111@111.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.ManiPedi, 2, 1, 3, new List<Guid>());
                var serviceProviderId2 = AddServiceProvider(context, "W25BE", "+44", "22222222222222", "222", "222", "222@222.com", "36 Chepstow Rd, London", "London", "United Kingdom", db.ServiceProviderTypes.ManiPedi, 2, 1, 3, new List<Guid>());

                var dtOrder1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId1 = AddOrder(context, 1, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder1.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId1);

                var dtOrder3 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId3 = AddOrder(context, 3, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder3.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId2);

                var addressService = new AddressService("jNoFfAuChUSW46rMEDspKQ9178");

                var orderService = new OrderAssignmentService(context, 50, 120, addressService);
                //Act
                var newOrderDt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 14, 0, 0, DateTimeKind.Local).AddDays(1);
                var isAvailibleTime = orderService.IsAssignmentPossible(new List<db.OrderService>(new db.OrderService[] { new db.OrderService() { Quantity = 1, Service = service1 } }), newOrderDt.ToUniversalTime());

                var newOrder = AddOrder(context, 5, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, newOrderDt.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), Guid.Empty);

                var spResult = orderService.AssignOrder(newOrder);

                //Assert
                Assert.AreEqual(true, isAvailibleTime);
                Assert.AreEqual(false, spResult.IsBroadcasted);
                Assert.AreEqual(true, spResult.IsAssigned);
                Assert.AreEqual(serviceProviderId1, spResult.ServiceProvider.Id);
            }
        }

        #endregion

        #region expensive, not mixed order type, contracted and noncontracted sp timeslots exists
        //order type: not mixed
        //order amount: expensive
        //availible timeslots: yes
        //first sp: contracted, already has two orders
        //second sp: contracted, already has two orders
        //third sp: noncontracted, already has two orders
        //fourth sp: noncontracted, already has two orders
        [TestMethod]
        public void TestMethod22311()
        {
            using (var context = new DataContext(_options))
            {
                //Arrange
                var service1 = context.Services.Where(s => s.Title == "Luxury Gel Pedicure").First();
                var service2 = context.Services.Where(s => s.Title == "Normal Mani-Padi").First();

                var customerId1 = AddCustomer(context, "", "+44", "", "cust1", "cust1", "cust1@cust.com");
                var customerId2 = AddCustomer(context, "", "+44", "", "cust2", "cust2", "cust2@cust.com");

                var serviceProviderId1 = AddServiceProvider(context, "SE10DH", "+44", "11111111111111", "111", "111", "111@111.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 1, 1, 1, new List<Guid>());
                var serviceProviderId2 = AddServiceProvider(context, "SE10DH", "+44", "22222222222222", "222", "222", "222@222.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 1, 1, 1, new List<Guid>());
                var serviceProviderId3 = AddServiceProvider(context, "SE10DH", "+44", "22222222222222", "222", "222", "222@222.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.ManiPedi, 1, 1, 1, new List<Guid>());
                var serviceProviderId4 = AddServiceProvider(context, "SE10DH", "+44", "22222222222222", "222", "222", "222@222.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.ManiPedi, 1, 1, 1, new List<Guid>());

                var dtOrder1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId1 = AddOrder(context, 1, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder1.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId1);
                var dtOrder2 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId2 = AddOrder(context, 2, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder2.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId1);

                var dtOrder3 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId3 = AddOrder(context, 3, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder3.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId2);
                var dtOrder4 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId4 = AddOrder(context, 4, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder4.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId2);

                var dtOrder5 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId5 = AddOrder(context, 5, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder5.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId3);
                var dtOrder6 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId6 = AddOrder(context, 6, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder6.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId3);

                var dtOrder7 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId7 = AddOrder(context, 7, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder7.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId4);
                var dtOrder8 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId8 = AddOrder(context, 8, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder8.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId4);

                var addressService = new AddressService("jNoFfAuChUSW46rMEDspKQ9178");

                var orderService = new OrderAssignmentService(context, 50, 120, addressService);
                //Act
                var newOrderDt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 14, 0, 0, DateTimeKind.Local).AddDays(1);
                var isAvailibleTime = orderService.IsAssignmentPossible(new List<db.OrderService>(new db.OrderService[] { new db.OrderService() { Quantity = 1, Service = service1 } }), newOrderDt.ToUniversalTime());

                var newOrder = AddOrder(context, 5, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, newOrderDt.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), Guid.Empty);

                var spResult = orderService.AssignOrder(newOrder);

                //Assert
                Assert.AreEqual(true, isAvailibleTime);
                Assert.AreEqual(true, spResult.IsBroadcasted);
                Assert.AreEqual(false, spResult.IsAssigned);
                Assert.AreEqual(null, spResult.ServiceProvider);
            }
        }

        //order type: not mixed
        //order amount: expensive
        //availible timeslots: yes
        //first sp: contracted, has one orders
        //second sp: contracted, already has two orders
        //third sp: noncontracted, already has two orders
        //fourth sp: noncontracted, already has two orders
        [TestMethod]
        public void TestMethod22312()
        {
            using (var context = new DataContext(_options))
            {
                //Arrange
                var service1 = context.Services.Where(s => s.Title == "Luxury Gel Pedicure").First();
                var service2 = context.Services.Where(s => s.Title == "Normal Mani-Padi").First();

                var customerId1 = AddCustomer(context, "", "+44", "", "cust1", "cust1", "cust1@cust.com");
                var customerId2 = AddCustomer(context, "", "+44", "", "cust2", "cust2", "cust2@cust.com");

                var serviceProviderId1 = AddServiceProvider(context, "SE10DH", "+44", "11111111111111", "111", "111", "111@111.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 1, 1, 1, new List<Guid>());
                var serviceProviderId2 = AddServiceProvider(context, "SE10DH", "+44", "22222222222222", "222", "222", "222@222.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 1, 1, 1, new List<Guid>());
                var serviceProviderId3 = AddServiceProvider(context, "SE10DH", "+44", "22222222222222", "222", "222", "222@222.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.ManiPedi, 1, 1, 1, new List<Guid>());
                var serviceProviderId4 = AddServiceProvider(context, "SE10DH", "+44", "22222222222222", "222", "222", "222@222.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.ManiPedi, 1, 1, 1, new List<Guid>());

                var dtOrder1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId1 = AddOrder(context, 1, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder1.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId1);

                var dtOrder3 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId3 = AddOrder(context, 3, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder3.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId2);
                var dtOrder4 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId4 = AddOrder(context, 4, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder4.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId2);

                var dtOrder5 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId5 = AddOrder(context, 5, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder5.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId3);
                var dtOrder6 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId6 = AddOrder(context, 6, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder6.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId3);

                var dtOrder7 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId7 = AddOrder(context, 7, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder7.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId4);
                var dtOrder8 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId8 = AddOrder(context, 8, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder8.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId4);

                var addressService = new AddressService("jNoFfAuChUSW46rMEDspKQ9178");

                var orderService = new OrderAssignmentService(context, 50, 120, addressService);
                //Act
                var newOrderDt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 14, 0, 0, DateTimeKind.Local).AddDays(1);
                var isAvailibleTime = orderService.IsAssignmentPossible(new List<db.OrderService>(new db.OrderService[] { new db.OrderService() { Quantity = 1, Service = service1 } }), newOrderDt.ToUniversalTime());

                var newOrder = AddOrder(context, 5, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, newOrderDt.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), Guid.Empty);

                var spResult = orderService.AssignOrder(newOrder);

                //Assert
                Assert.AreEqual(true, isAvailibleTime);
                Assert.AreEqual(false, spResult.IsBroadcasted);
                Assert.AreEqual(true, spResult.IsAssigned);
                Assert.AreEqual(serviceProviderId1, spResult.ServiceProvider.Id);
            }
        }

        //order type: not mixed
        //order amount: expensive
        //availible timeslots: yes
        //first sp: contracted, already has two orders
        //second sp: contracted, already has two orders
        //third sp: noncontracted, has one orders
        //fourth sp: noncontracted, already has two orders
        [TestMethod]
        public void TestMethod22313()
        {
            using (var context = new DataContext(_options))
            {
                //Arrange
                var service1 = context.Services.Where(s => s.Title == "Luxury Gel Pedicure").First();
                var service2 = context.Services.Where(s => s.Title == "Normal Mani-Padi").First();

                var customerId1 = AddCustomer(context, "", "+44", "", "cust1", "cust1", "cust1@cust.com");
                var customerId2 = AddCustomer(context, "", "+44", "", "cust2", "cust2", "cust2@cust.com");

                var serviceProviderId1 = AddServiceProvider(context, "SE10DH", "+44", "11111111111111", "111", "111", "111@111.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 1, 1, 1, new List<Guid>());
                var serviceProviderId2 = AddServiceProvider(context, "SE10DH", "+44", "22222222222222", "222", "222", "222@222.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 1, 1, 1, new List<Guid>());
                var serviceProviderId3 = AddServiceProvider(context, "SE10DH", "+44", "22222222222222", "222", "222", "222@222.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.ManiPedi, 1, 1, 1, new List<Guid>());
                var serviceProviderId4 = AddServiceProvider(context, "SE10DH", "+44", "22222222222222", "222", "222", "222@222.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.ManiPedi, 1, 1, 1, new List<Guid>());

                var dtOrder1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId1 = AddOrder(context, 1, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder1.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId1);
                var dtOrder2 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId2 = AddOrder(context, 2, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder2.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId1);

                var dtOrder3 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId3 = AddOrder(context, 3, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder3.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId2);
                var dtOrder4 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId4 = AddOrder(context, 4, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder4.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId2);

                var dtOrder5 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId5 = AddOrder(context, 5, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder5.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId3);
                //var dtOrder6 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                //var orderId6 = AddOrder(context, 6, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder6.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId3);

                var dtOrder7 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId7 = AddOrder(context, 7, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder7.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId4);
                var dtOrder8 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId8 = AddOrder(context, 8, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder8.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId4);

                var addressService = new AddressService("jNoFfAuChUSW46rMEDspKQ9178");

                var orderService = new OrderAssignmentService(context, 50, 120, addressService);
                //Act
                var newOrderDt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 14, 0, 0, DateTimeKind.Local).AddDays(1);
                var isAvailibleTime = orderService.IsAssignmentPossible(new List<db.OrderService>(new db.OrderService[] { new db.OrderService() { Quantity = 1, Service = service1 } }), newOrderDt.ToUniversalTime());

                var newOrder = AddOrder(context, 5, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, newOrderDt.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), Guid.Empty);

                var spResult = orderService.AssignOrder(newOrder);

                //Assert
                Assert.AreEqual(true, isAvailibleTime);
                Assert.AreEqual(false, spResult.IsBroadcasted);
                Assert.AreEqual(true, spResult.IsAssigned);
                Assert.AreEqual(serviceProviderId3, spResult.ServiceProvider.Id);
            }
        }

        //order type: not mixed
        //order amount: expensive
        //availible timeslots: yes
        //first sp: contracted, has one orders
        //second sp: contracted, already has two orders
        //third sp: noncontracted, has one orders
        //fourth sp: noncontracted, already has two orders
        [TestMethod]
        public void TestMethod22314()
        {
            using (var context = new DataContext(_options))
            {
                //Arrange
                var service1 = context.Services.Where(s => s.Title == "Luxury Gel Pedicure").First();
                var service2 = context.Services.Where(s => s.Title == "Normal Mani-Padi").First();

                var customerId1 = AddCustomer(context, "", "+44", "", "cust1", "cust1", "cust1@cust.com");
                var customerId2 = AddCustomer(context, "", "+44", "", "cust2", "cust2", "cust2@cust.com");

                var serviceProviderId1 = AddServiceProvider(context, "SE10DH", "+44", "11111111111111", "111", "111", "111@111.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 1, 1, 1, new List<Guid>());
                var serviceProviderId2 = AddServiceProvider(context, "SE10DH", "+44", "22222222222222", "222", "222", "222@222.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 1, 1, 1, new List<Guid>());
                var serviceProviderId3 = AddServiceProvider(context, "SE10DH", "+44", "22222222222222", "222", "222", "222@222.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.ManiPedi, 1, 1, 1, new List<Guid>());
                var serviceProviderId4 = AddServiceProvider(context, "SE10DH", "+44", "22222222222222", "222", "222", "222@222.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.ManiPedi, 1, 1, 1, new List<Guid>());

                var dtOrder1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId1 = AddOrder(context, 1, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder1.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId1);
                //var dtOrder2 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                //var orderId2 = AddOrder(context, 2, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder2.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId1);

                var dtOrder3 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId3 = AddOrder(context, 3, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder3.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId2);
                var dtOrder4 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId4 = AddOrder(context, 4, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder4.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId2);

                var dtOrder5 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId5 = AddOrder(context, 5, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder5.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId3);
                //var dtOrder6 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                //var orderId6 = AddOrder(context, 6, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder6.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId3);

                var dtOrder7 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId7 = AddOrder(context, 7, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder7.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId4);
                var dtOrder8 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId8 = AddOrder(context, 8, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder8.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId4);

                var addressService = new AddressService("jNoFfAuChUSW46rMEDspKQ9178");

                var orderService = new OrderAssignmentService(context, 50, 120, addressService);
                //Act
                var newOrderDt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 14, 0, 0, DateTimeKind.Local).AddDays(1);
                var isAvailibleTime = orderService.IsAssignmentPossible(new List<db.OrderService>(new db.OrderService[] { new db.OrderService() { Quantity = 1, Service = service1 } }), newOrderDt.ToUniversalTime());

                var newOrder = AddOrder(context, 5, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, newOrderDt.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), Guid.Empty);

                var spResult = orderService.AssignOrder(newOrder);

                //Assert
                Assert.AreEqual(true, isAvailibleTime);
                Assert.AreEqual(false, spResult.IsBroadcasted);
                Assert.AreEqual(true, spResult.IsAssigned);
                Assert.AreEqual(serviceProviderId3, spResult.ServiceProvider.Id);
            }
        }

        //order type: not mixed
        //order amount: expensive
        //availible timeslots: yes
        //first sp: contracted, one order
        //second sp: contracted, one order
        //third sp: noncontracted, already has two orders
        //fourth sp: noncontracted, already has two orders
        [TestMethod]
        public void TestMethod22321()
        {
            using (var context = new DataContext(_options))
            {
                //Arrange
                var service1 = context.Services.Where(s => s.Title == "Luxury Gel Pedicure").First();
                var service2 = context.Services.Where(s => s.Title == "Normal Mani-Padi").First();

                var customerId1 = AddCustomer(context, "", "+44", "", "cust1", "cust1", "cust1@cust.com");
                var customerId2 = AddCustomer(context, "", "+44", "", "cust2", "cust2", "cust2@cust.com");

                var serviceProviderId1 = AddServiceProvider(context, "SE10DH", "+44", "11111111111111", "111", "111", "111@111.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 2, 1, 1, new List<Guid>());
                var serviceProviderId2 = AddServiceProvider(context, "SE10DH", "+44", "22222222222222", "222", "222", "222@222.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 3, 1, 1, new List<Guid>());
                var serviceProviderId3 = AddServiceProvider(context, "SE10DH", "+44", "22222222222222", "222", "222", "222@222.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.ManiPedi, 4, 1, 1, new List<Guid>());
                var serviceProviderId4 = AddServiceProvider(context, "SE10DH", "+44", "22222222222222", "222", "222", "222@222.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.ManiPedi, 4, 1, 1, new List<Guid>());

                var dtOrder1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId1 = AddOrder(context, 1, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder1.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId1);
                //var dtOrder2 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                //var orderId2 = AddOrder(context, 2, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder2.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId1);

                var dtOrder3 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId3 = AddOrder(context, 3, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder3.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId2);
                //var dtOrder4 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                //var orderId4 = AddOrder(context, 4, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder4.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId2);

                var dtOrder5 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId5 = AddOrder(context, 5, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder5.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId3);
                var dtOrder6 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId6 = AddOrder(context, 6, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder6.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId3);

                var dtOrder7 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId7 = AddOrder(context, 7, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder7.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId4);
                var dtOrder8 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId8 = AddOrder(context, 8, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder8.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId4);

                var addressService = new AddressService("jNoFfAuChUSW46rMEDspKQ9178");

                var orderService = new OrderAssignmentService(context, 50, 120, addressService);
                //Act
                var newOrderDt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 14, 0, 0, DateTimeKind.Local).AddDays(1);
                var isAvailibleTime = orderService.IsAssignmentPossible(new List<db.OrderService>(new db.OrderService[] { new db.OrderService() { Quantity = 1, Service = service1 } }), newOrderDt.ToUniversalTime());

                var newOrder = AddOrder(context, 9, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, newOrderDt.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), Guid.Empty);

                var spResult = orderService.AssignOrder(newOrder);

                //Assert
                Assert.AreEqual(true, isAvailibleTime);
                Assert.AreEqual(false, spResult.IsBroadcasted);
                Assert.AreEqual(true, spResult.IsAssigned);
                Assert.AreEqual(serviceProviderId2, spResult.ServiceProvider.Id);
            }
        }

        //order type: not mixed
        //order amount: expensive
        //availible timeslots: yes
        //first sp: contracted, already has two orders
        //second sp: contracted, already has two orders
        //third sp: noncontracted, one order
        //fourth sp: noncontracted, one order
        [TestMethod]
        public void TestMethod22322()
        {
            using (var context = new DataContext(_options))
            {
                //Arrange
                var service1 = context.Services.Where(s => s.Title == "Luxury Gel Pedicure").First();
                var service2 = context.Services.Where(s => s.Title == "Normal Mani-Padi").First();

                var customerId1 = AddCustomer(context, "", "+44", "", "cust1", "cust1", "cust1@cust.com");
                var customerId2 = AddCustomer(context, "", "+44", "", "cust2", "cust2", "cust2@cust.com");

                var serviceProviderId1 = AddServiceProvider(context, "SE10DH", "+44", "11111111111111", "111", "111", "111@111.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 4, 1, 1, new List<Guid>());
                var serviceProviderId2 = AddServiceProvider(context, "SE10DH", "+44", "22222222222222", "222", "222", "222@222.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 5, 1, 1, new List<Guid>());
                var serviceProviderId3 = AddServiceProvider(context, "SE10DH", "+44", "22222222222222", "222", "222", "222@222.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.ManiPedi, 2, 1, 1, new List<Guid>());
                var serviceProviderId4 = AddServiceProvider(context, "SE10DH", "+44", "22222222222222", "222", "222", "222@222.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.ManiPedi, 3, 1, 1, new List<Guid>());

                var dtOrder1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId1 = AddOrder(context, 1, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder1.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId1);
                var dtOrder2 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId2 = AddOrder(context, 2, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder2.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId1);

                var dtOrder3 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId3 = AddOrder(context, 3, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder3.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId2);
                var dtOrder4 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId4 = AddOrder(context, 4, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder4.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId2);

                var dtOrder5 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId5 = AddOrder(context, 5, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder5.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId3);
                //var dtOrder6 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                //var orderId6 = AddOrder(context, 6, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder6.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId3);

                var dtOrder7 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId7 = AddOrder(context, 7, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder7.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId4);
                //var dtOrder8 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                //var orderId8 = AddOrder(context, 8, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder8.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId4);

                var addressService = new AddressService("jNoFfAuChUSW46rMEDspKQ9178");

                var orderService = new OrderAssignmentService(context, 50, 120, addressService);
                //Act
                var newOrderDt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 14, 0, 0, DateTimeKind.Local).AddDays(1);
                var isAvailibleTime = orderService.IsAssignmentPossible(new List<db.OrderService>(new db.OrderService[] { new db.OrderService() { Quantity = 1, Service = service1 } }), newOrderDt.ToUniversalTime());

                var newOrder = AddOrder(context, 9, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, newOrderDt.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), Guid.Empty);

                var spResult = orderService.AssignOrder(newOrder);

                //Assert
                Assert.AreEqual(true, isAvailibleTime);
                Assert.AreEqual(false, spResult.IsBroadcasted);
                Assert.AreEqual(true, spResult.IsAssigned);
                Assert.AreEqual(serviceProviderId4, spResult.ServiceProvider.Id);
            }
        }

        //order type: not mixed
        //order amount: expensive
        //availible timeslots: yes
        //first sp: contracted, one order
        //second sp: contracted, one order
        //third sp: noncontracted, one order
        //fourth sp: noncontracted, one order
        [TestMethod]
        public void TestMethod22323()
        {
            using (var context = new DataContext(_options))
            {
                //Arrange
                var service1 = context.Services.Where(s => s.Title == "Luxury Gel Pedicure").First();
                var service2 = context.Services.Where(s => s.Title == "Normal Mani-Padi").First();

                var customerId1 = AddCustomer(context, "", "+44", "", "cust1", "cust1", "cust1@cust.com");
                var customerId2 = AddCustomer(context, "", "+44", "", "cust2", "cust2", "cust2@cust.com");

                var serviceProviderId1 = AddServiceProvider(context, "SE10DH", "+44", "11111111111111", "111", "111", "111@111.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 1, 1, 1, new List<Guid>());
                var serviceProviderId2 = AddServiceProvider(context, "SE10DH", "+44", "22222222222222", "222", "222", "222@222.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 3, 1, 1, new List<Guid>());
                var serviceProviderId3 = AddServiceProvider(context, "SE10DH", "+44", "22222222222222", "222", "222", "222@222.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.ManiPedi, 2, 1, 1, new List<Guid>());
                var serviceProviderId4 = AddServiceProvider(context, "SE10DH", "+44", "22222222222222", "222", "222", "222@222.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.ManiPedi, 1, 1, 1, new List<Guid>());

                var dtOrder1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId1 = AddOrder(context, 1, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder1.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId1);
                //var dtOrder2 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                //var orderId2 = AddOrder(context, 2, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder2.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId1);

                var dtOrder3 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId3 = AddOrder(context, 3, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder3.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId2);
                //var dtOrder4 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                //var orderId4 = AddOrder(context, 4, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder4.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId2);

                var dtOrder5 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId5 = AddOrder(context, 5, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder5.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId3);
                //var dtOrder6 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                //var orderId6 = AddOrder(context, 6, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder6.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId3);

                var dtOrder7 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId7 = AddOrder(context, 7, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder7.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId4);
                //var dtOrder8 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                //var orderId8 = AddOrder(context, 8, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder8.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId4);

                var addressService = new AddressService("jNoFfAuChUSW46rMEDspKQ9178");

                var orderService = new OrderAssignmentService(context, 50, 120, addressService);
                //Act
                var newOrderDt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 14, 0, 0, DateTimeKind.Local).AddDays(1);
                var isAvailibleTime = orderService.IsAssignmentPossible(new List<db.OrderService>(new db.OrderService[] { new db.OrderService() { Quantity = 1, Service = service1 } }), newOrderDt.ToUniversalTime());

                var newOrder = AddOrder(context, 9, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, newOrderDt.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), Guid.Empty);

                var spResult = orderService.AssignOrder(newOrder);

                //Assert
                Assert.AreEqual(true, isAvailibleTime);
                Assert.AreEqual(false, spResult.IsBroadcasted);
                Assert.AreEqual(true, spResult.IsAssigned);
                Assert.AreEqual(serviceProviderId3, spResult.ServiceProvider.Id);
            }
        }

        //order type: not mixed
        //order amount: expensive
        //availible timeslots: yes
        //first sp: contracted, one order
        //second sp: contracted, one order
        //third sp: noncontracted, already has two orders
        //fourth sp: noncontracted, already has two orders
        [TestMethod]
        public void TestMethod22331()
        {
            using (var context = new DataContext(_options))
            {
                //Arrange
                var service1 = context.Services.Where(s => s.Title == "Luxury Gel Pedicure").First();
                var service2 = context.Services.Where(s => s.Title == "Normal Mani-Padi").First();

                var customerId1 = AddCustomer(context, "", "+44", "", "cust1", "cust1", "cust1@cust.com");
                var customerId2 = AddCustomer(context, "", "+44", "", "cust2", "cust2", "cust2@cust.com");

                var serviceProviderId1 = AddServiceProvider(context, "SE10DH", "+44", "11111111111111", "111", "111", "111@111.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 1, 1, 1, new List<Guid>());
                var serviceProviderId2 = AddServiceProvider(context, "SE10DH", "+44", "22222222222222", "222", "222", "222@222.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 1, 1, 2, new List<Guid>());
                var serviceProviderId3 = AddServiceProvider(context, "SE10DH", "+44", "22222222222222", "222", "222", "222@222.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.ManiPedi, 1, 1, 1, new List<Guid>());
                var serviceProviderId4 = AddServiceProvider(context, "SE10DH", "+44", "22222222222222", "222", "222", "222@222.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.ManiPedi, 1, 1, 1, new List<Guid>());

                var dtOrder1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId1 = AddOrder(context, 1, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder1.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId1);
                //var dtOrder2 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                //var orderId2 = AddOrder(context, 2, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder2.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId1);

                var dtOrder3 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId3 = AddOrder(context, 3, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder3.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId2);
                //var dtOrder4 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                //var orderId4 = AddOrder(context, 4, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder4.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId2);

                var dtOrder5 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId5 = AddOrder(context, 5, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder5.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId3);
                var dtOrder6 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId6 = AddOrder(context, 6, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder6.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId3);

                var dtOrder7 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId7 = AddOrder(context, 7, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder7.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId4);
                var dtOrder8 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId8 = AddOrder(context, 8, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder8.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId4);

                var addressService = new AddressService("jNoFfAuChUSW46rMEDspKQ9178");

                var orderService = new OrderAssignmentService(context, 50, 120, addressService);
                //Act
                var newOrderDt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 14, 0, 0, DateTimeKind.Local).AddDays(1);
                var isAvailibleTime = orderService.IsAssignmentPossible(new List<db.OrderService>(new db.OrderService[] { new db.OrderService() { Quantity = 1, Service = service1 } }), newOrderDt.ToUniversalTime());

                var newOrder = AddOrder(context, 9, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, newOrderDt.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), Guid.Empty);

                var spResult = orderService.AssignOrder(newOrder);

                //Assert
                Assert.AreEqual(true, isAvailibleTime);
                Assert.AreEqual(false, spResult.IsBroadcasted);
                Assert.AreEqual(true, spResult.IsAssigned);
                Assert.AreEqual(serviceProviderId2, spResult.ServiceProvider.Id);
            }
        }

        //order type: not mixed
        //order amount: expensive
        //availible timeslots: yes
        //first sp: contracted, already has two orders
        //second sp: contracted, already has two orders
        //third sp: noncontracted, one order
        //fourth sp: noncontracted, one order
        [TestMethod]
        public void TestMethod22332()
        {
            using (var context = new DataContext(_options))
            {
                //Arrange
                var service1 = context.Services.Where(s => s.Title == "Luxury Gel Pedicure").First();
                var service2 = context.Services.Where(s => s.Title == "Normal Mani-Padi").First();

                var customerId1 = AddCustomer(context, "", "+44", "", "cust1", "cust1", "cust1@cust.com");
                var customerId2 = AddCustomer(context, "", "+44", "", "cust2", "cust2", "cust2@cust.com");

                var serviceProviderId1 = AddServiceProvider(context, "SE10DH", "+44", "11111111111111", "111", "111", "111@111.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 1, 1, 1, new List<Guid>());
                var serviceProviderId2 = AddServiceProvider(context, "SE10DH", "+44", "22222222222222", "222", "222", "222@222.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 1, 1, 1, new List<Guid>());
                var serviceProviderId3 = AddServiceProvider(context, "SE10DH", "+44", "22222222222222", "222", "222", "222@222.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.ManiPedi, 1, 1, 1, new List<Guid>());
                var serviceProviderId4 = AddServiceProvider(context, "SE10DH", "+44", "22222222222222", "222", "222", "222@222.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.ManiPedi, 1, 1, 2, new List<Guid>());

                var dtOrder1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId1 = AddOrder(context, 1, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder1.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId1);
                var dtOrder2 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId2 = AddOrder(context, 2, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder2.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId1);

                var dtOrder3 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId3 = AddOrder(context, 3, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder3.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId2);
                var dtOrder4 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId4 = AddOrder(context, 4, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder4.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId2);

                var dtOrder5 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId5 = AddOrder(context, 5, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder5.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId3);
                //var dtOrder6 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                //var orderId6 = AddOrder(context, 6, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder6.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId3);

                var dtOrder7 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId7 = AddOrder(context, 7, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder7.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId4);
                //var dtOrder8 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                //var orderId8 = AddOrder(context, 8, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder8.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId4);

                var addressService = new AddressService("jNoFfAuChUSW46rMEDspKQ9178");

                var orderService = new OrderAssignmentService(context, 50, 120, addressService);
                //Act
                var newOrderDt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 14, 0, 0, DateTimeKind.Local).AddDays(1);
                var isAvailibleTime = orderService.IsAssignmentPossible(new List<db.OrderService>(new db.OrderService[] { new db.OrderService() { Quantity = 1, Service = service1 } }), newOrderDt.ToUniversalTime());

                var newOrder = AddOrder(context, 9, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, newOrderDt.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), Guid.Empty);

                var spResult = orderService.AssignOrder(newOrder);

                //Assert
                Assert.AreEqual(true, isAvailibleTime);
                Assert.AreEqual(false, spResult.IsBroadcasted);
                Assert.AreEqual(true, spResult.IsAssigned);
                Assert.AreEqual(serviceProviderId4, spResult.ServiceProvider.Id);
            }
        }

        //order type: not mixed
        //order amount: expensive
        //availible timeslots: yes
        //first sp: contracted, one order
        //second sp: contracted, one order
        //third sp: noncontracted, one order
        //fourth sp: noncontracted, one order
        [TestMethod]
        public void TestMethod22333()
        {
            using (var context = new DataContext(_options))
            {
                //Arrange
                var service1 = context.Services.Where(s => s.Title == "Luxury Gel Pedicure").First();
                var service2 = context.Services.Where(s => s.Title == "Normal Mani-Padi").First();

                var customerId1 = AddCustomer(context, "", "+44", "", "cust1", "cust1", "cust1@cust.com");
                var customerId2 = AddCustomer(context, "", "+44", "", "cust2", "cust2", "cust2@cust.com");

                var serviceProviderId1 = AddServiceProvider(context, "SE10DH", "+44", "11111111111111", "111", "111", "111@111.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 1, 1, 4, new List<Guid>());
                var serviceProviderId2 = AddServiceProvider(context, "SE10DH", "+44", "22222222222222", "222", "222", "222@222.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 1, 1, 1, new List<Guid>());
                var serviceProviderId3 = AddServiceProvider(context, "SE10DH", "+44", "22222222222222", "222", "222", "222@222.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.ManiPedi, 1, 1, 3, new List<Guid>());
                var serviceProviderId4 = AddServiceProvider(context, "SE10DH", "+44", "22222222222222", "222", "222", "222@222.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.ManiPedi, 1, 1, 1, new List<Guid>());

                var dtOrder1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId1 = AddOrder(context, 1, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder1.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId1);
                //var dtOrder2 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                //var orderId2 = AddOrder(context, 2, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder2.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId1);

                var dtOrder3 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId3 = AddOrder(context, 3, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder3.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId2);
                //var dtOrder4 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                //var orderId4 = AddOrder(context, 4, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder4.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId2);

                var dtOrder5 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId5 = AddOrder(context, 5, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder5.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId3);
                //var dtOrder6 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                //var orderId6 = AddOrder(context, 6, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder6.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId3);

                var dtOrder7 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId7 = AddOrder(context, 7, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder7.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId4);
                //var dtOrder8 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                //var orderId8 = AddOrder(context, 8, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder8.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId4);

                var addressService = new AddressService("jNoFfAuChUSW46rMEDspKQ9178");

                var orderService = new OrderAssignmentService(context, 50, 120, addressService);
                //Act
                var newOrderDt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 14, 0, 0, DateTimeKind.Local).AddDays(1);
                var isAvailibleTime = orderService.IsAssignmentPossible(new List<db.OrderService>(new db.OrderService[] { new db.OrderService() { Quantity = 1, Service = service1 } }), newOrderDt.ToUniversalTime());

                var newOrder = AddOrder(context, 9, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, newOrderDt.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), Guid.Empty);

                var spResult = orderService.AssignOrder(newOrder);

                //Assert
                Assert.AreEqual(true, isAvailibleTime);
                Assert.AreEqual(false, spResult.IsBroadcasted);
                Assert.AreEqual(true, spResult.IsAssigned);
                Assert.AreEqual(serviceProviderId3, spResult.ServiceProvider.Id);
            }
        }

        //order type: not mixed
        //order amount: expensive
        //availible timeslots: yes
        //first sp: contracted, one order, located closer
        //second sp: contracted, one order
        //third sp: noncontracted, already has two orders
        //fourth sp: noncontracted, already has two orders
        [TestMethod]
        public void TestMethod22341()
        {
            using (var context = new DataContext(_options))
            {
                //Arrange
                var service1 = context.Services.Where(s => s.Title == "Luxury Gel Pedicure").First();
                var service2 = context.Services.Where(s => s.Title == "Normal Mani-Padi").First();

                var customerId1 = AddCustomer(context, "", "+44", "", "cust1", "cust1", "cust1@cust.com");
                var customerId2 = AddCustomer(context, "", "+44", "", "cust2", "cust2", "cust2@cust.com");

                var serviceProviderId1 = AddServiceProvider(context, "SE10DH", "+44", "11111111111111", "111", "111", "111@111.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 1, 1, 1, new List<Guid>());
                var serviceProviderId2 = AddServiceProvider(context, "W25BE", "+44", "22222222222222", "222", "222", "222@222.com", "36 Chepstow Rd, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 1, 1, 1, new List<Guid>());
                var serviceProviderId3 = AddServiceProvider(context, "SE10DH", "+44", "22222222222222", "222", "222", "222@222.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.ManiPedi, 1, 1, 1, new List<Guid>());
                var serviceProviderId4 = AddServiceProvider(context, "SE10DH", "+44", "22222222222222", "222", "222", "222@222.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.ManiPedi, 1, 1, 1, new List<Guid>());

                var dtOrder1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId1 = AddOrder(context, 1, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder1.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId1);
                //var dtOrder2 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                //var orderId2 = AddOrder(context, 2, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder2.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId1);

                var dtOrder3 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId3 = AddOrder(context, 3, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder3.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId2);
                //var dtOrder4 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                //var orderId4 = AddOrder(context, 4, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder4.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId2);

                var dtOrder5 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId5 = AddOrder(context, 5, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder5.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId3);
                var dtOrder6 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId6 = AddOrder(context, 6, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder6.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId3);

                var dtOrder7 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId7 = AddOrder(context, 7, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder7.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId4);
                var dtOrder8 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId8 = AddOrder(context, 8, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder8.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId4);

                var addressService = new AddressService("jNoFfAuChUSW46rMEDspKQ9178");

                var orderService = new OrderAssignmentService(context, 50, 120, addressService);
                //Act
                var newOrderDt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 14, 0, 0, DateTimeKind.Local).AddDays(1);
                var isAvailibleTime = orderService.IsAssignmentPossible(new List<db.OrderService>(new db.OrderService[] { new db.OrderService() { Quantity = 1, Service = service1 } }), newOrderDt.ToUniversalTime());

                var newOrder = AddOrder(context, 9, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, newOrderDt.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), Guid.Empty);

                var spResult = orderService.AssignOrder(newOrder);

                //Assert
                Assert.AreEqual(true, isAvailibleTime);
                Assert.AreEqual(false, spResult.IsBroadcasted);
                Assert.AreEqual(true, spResult.IsAssigned);
                Assert.AreEqual(serviceProviderId1, spResult.ServiceProvider.Id);
            }
        }

        //order type: not mixed
        //order amount: expensive
        //availible timeslots: yes
        //first sp: contracted, already has two orders
        //second sp: contracted, already has two orders
        //third sp: noncontracted, one order, located closer
        //fourth sp: noncontracted, one order
        [TestMethod]
        public void TestMethod22342()
        {
            using (var context = new DataContext(_options))
            {
                //Arrange
                var service1 = context.Services.Where(s => s.Title == "Luxury Gel Pedicure").First();
                var service2 = context.Services.Where(s => s.Title == "Normal Mani-Padi").First();

                var customerId1 = AddCustomer(context, "", "+44", "", "cust1", "cust1", "cust1@cust.com");
                var customerId2 = AddCustomer(context, "", "+44", "", "cust2", "cust2", "cust2@cust.com");

                var serviceProviderId1 = AddServiceProvider(context, "SE10DH", "+44", "11111111111111", "111", "111", "111@111.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 1, 1, 1, new List<Guid>());
                var serviceProviderId2 = AddServiceProvider(context, "SE10DH", "+44", "22222222222222", "222", "222", "222@222.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 1, 1, 1, new List<Guid>());
                var serviceProviderId3 = AddServiceProvider(context, "SE10DH", "+44", "22222222222222", "222", "222", "222@222.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.ManiPedi, 1, 1, 1, new List<Guid>());
                var serviceProviderId4 = AddServiceProvider(context, "W25BE", "+44", "22222222222222", "222", "222", "222@222.com", "36 Chepstow Rd, London", "London", "United Kingdom", db.ServiceProviderTypes.ManiPedi, 1, 1, 1, new List<Guid>());

                var dtOrder1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId1 = AddOrder(context, 1, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder1.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId1);
                var dtOrder2 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId2 = AddOrder(context, 2, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder2.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId1);

                var dtOrder3 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId3 = AddOrder(context, 3, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder3.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId2);
                var dtOrder4 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId4 = AddOrder(context, 4, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder4.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId2);

                var dtOrder5 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId5 = AddOrder(context, 5, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder5.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId3);
                //var dtOrder6 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                //var orderId6 = AddOrder(context, 6, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder6.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId3);

                var dtOrder7 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId7 = AddOrder(context, 7, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder7.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId4);
                //var dtOrder8 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                //var orderId8 = AddOrder(context, 8, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder8.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId4);

                var addressService = new AddressService("jNoFfAuChUSW46rMEDspKQ9178");

                var orderService = new OrderAssignmentService(context, 50, 120, addressService);
                //Act
                var newOrderDt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 14, 0, 0, DateTimeKind.Local).AddDays(1);
                var isAvailibleTime = orderService.IsAssignmentPossible(new List<db.OrderService>(new db.OrderService[] { new db.OrderService() { Quantity = 1, Service = service1 } }), newOrderDt.ToUniversalTime());

                var newOrder = AddOrder(context, 9, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, newOrderDt.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), Guid.Empty);

                var spResult = orderService.AssignOrder(newOrder);

                //Assert
                Assert.AreEqual(true, isAvailibleTime);
                Assert.AreEqual(false, spResult.IsBroadcasted);
                Assert.AreEqual(true, spResult.IsAssigned);
                Assert.AreEqual(serviceProviderId3, spResult.ServiceProvider.Id);
            }
        }

        //order type: not mixed
        //order amount: expensive
        //availible timeslots: yes
        //first sp: contracted, one order
        //second sp: contracted, one order, located closer
        //third sp: noncontracted, one order, located closer
        //fourth sp: noncontracted, one order
        [TestMethod]
        public void TestMethod22343()
        {
            using (var context = new DataContext(_options))
            {
                //Arrange
                var service1 = context.Services.Where(s => s.Title == "Luxury Gel Pedicure").First();
                var service2 = context.Services.Where(s => s.Title == "Normal Mani-Padi").First();

                var customerId1 = AddCustomer(context, "", "+44", "", "cust1", "cust1", "cust1@cust.com");
                var customerId2 = AddCustomer(context, "", "+44", "", "cust2", "cust2", "cust2@cust.com");

                var serviceProviderId1 = AddServiceProvider(context, "W25BE", "+44", "11111111111111", "111", "111", "111@111.com", "36 Chepstow Rd, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 1, 1, 1, new List<Guid>());
                var serviceProviderId2 = AddServiceProvider(context, "SE10DH", "+44", "22222222222222", "222", "222", "222@222.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.Contract, 1, 1, 1, new List<Guid>());
                var serviceProviderId3 = AddServiceProvider(context, "SE10DH", "+44", "22222222222222", "222", "222", "222@222.com", "Capital People, Unit 4, King James Court, London", "London", "United Kingdom", db.ServiceProviderTypes.ManiPedi, 1, 1, 1, new List<Guid>());
                var serviceProviderId4 = AddServiceProvider(context, "W25BE", "+44", "22222222222222", "222", "222", "222@222.com", "36 Chepstow Rd, London", "London", "United Kingdom", db.ServiceProviderTypes.ManiPedi, 1, 1, 1, new List<Guid>());

                var dtOrder1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId1 = AddOrder(context, 1, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder1.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId1);
                //var dtOrder2 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                //var orderId2 = AddOrder(context, 2, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder2.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId1);

                var dtOrder3 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId3 = AddOrder(context, 3, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder3.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId2);
                //var dtOrder4 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                //var orderId4 = AddOrder(context, 4, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder4.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId2);

                var dtOrder5 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId5 = AddOrder(context, 5, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder5.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId3);
                //var dtOrder6 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                //var orderId6 = AddOrder(context, 6, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder6.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId3);

                var dtOrder7 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0, DateTimeKind.Local).AddDays(1);
                var orderId7 = AddOrder(context, 7, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder7.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), serviceProviderId4);
                //var dtOrder8 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0, DateTimeKind.Local).AddDays(1);
                //var orderId8 = AddOrder(context, 8, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, dtOrder8.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service2 }), serviceProviderId4);

                var addressService = new AddressService("jNoFfAuChUSW46rMEDspKQ9178");

                var orderService = new OrderAssignmentService(context, 50, 120, addressService);
                //Act
                var newOrderDt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 14, 0, 0, DateTimeKind.Local).AddDays(1);
                var isAvailibleTime = orderService.IsAssignmentPossible(new List<db.OrderService>(new db.OrderService[] { new db.OrderService() { Quantity = 1, Service = service1 } }), newOrderDt.ToUniversalTime());

                var newOrder = AddOrder(context, 9, "SE10DH", "Capital People, Unit 4, King James Court, London", "London", db.OrderStates.New, newOrderDt.ToUniversalTime(), customerId1, new List<db.Service>(new db.Service[] { service1 }), Guid.Empty);

                var spResult = orderService.AssignOrder(newOrder);

                //Assert
                Assert.AreEqual(true, isAvailibleTime);
                Assert.AreEqual(false, spResult.IsBroadcasted);
                Assert.AreEqual(true, spResult.IsAssigned);
                Assert.AreEqual(serviceProviderId3, spResult.ServiceProvider.Id);
            }
        }
        #endregion

        [TestCleanup]
        public void TestCleanup()
        {
            using (var context = new DataContext(_options))
            {
                context.Orders.RemoveRange(context.Orders);
                context.ServiceProviders.RemoveRange(context.ServiceProviders);
                context.Customers.RemoveRange(context.Customers);

                context.SaveChanges();
            }
        }
    }
}
