﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Models
{
    public class SendCodeResponseModel
    {
        public String RequestId { get; set; }

        public Boolean IsAlreadyRegistred { get; set; }
    }
}
