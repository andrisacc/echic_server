﻿using System;
using ExternalServices.Contracts;
using Microsoft.Extensions.Logging;

namespace ExternalServices
{
    public class NexmoTestService : INexmoService
    {
        private NLog.Logger _logger = NLog.Web.NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();

        public NexmoTestService()
        {
        }

        private string _requestId = "d3564954-9d1d-40cd-936a-3d45e1faa845";
        public string SendCode(string code, string number)
        {
            _logger.Debug($"SendCode for code='{code}' and number='{number}'");

            return _requestId;
        }

        public bool VerifyCode(string requestId, string code)
        {
            return code == "1111" && requestId == _requestId;
        }
    }
}
