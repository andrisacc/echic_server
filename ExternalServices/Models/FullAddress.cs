﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExternalServices.Models
{
    public class FullAddress
    {
        public string Address { get; set; }
        public string DetailedAddress { get; set; }
    }
}
