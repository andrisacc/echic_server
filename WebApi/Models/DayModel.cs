﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Models
{
    public class DayModel
    {
        public Int32 Day { get; set; }

        public Int32 Month { get; set; }

        public Int32 Year { get; set; }

        public Boolean IsAllowed { get; set; }

        public Int32 MinTimeH { get; set; }

        public Int32 MinTimeM { get; set; }

        public Int32 MaxTimeH { get; set; }

        public Int32 MaxTimeM { get; set; }
    }
}
