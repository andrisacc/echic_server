﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Models
{
    public class OrderModel
    {
        public String PostalCode { get; set; }

        public String PhoneCode { get; set; }

        public String PhoneNumber { get; set; }

        public String FirstName { get; set; }

        public String LastName { get; set; }

        public String Email { get; set; }

        public String Address { get; set; }

        public String City { get; set; }

        public Guid LocationId { get; set; }

        public DateTime RequestedDateTimeLocal { get; set; }

        public PurchaseModel[] Purchases { get; set; }
    }
}
