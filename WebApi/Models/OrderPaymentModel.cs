﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Models
{
    public class OrderPaymentModel
    {
        public Guid OrderId { get; set; }

        public String StripeToken { get; set; }
    }
}
