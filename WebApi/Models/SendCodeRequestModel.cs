﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Models
{
    public class SendCodeRequestModel
    {
        public String PhoneCode { get; set; }

        public String PhoneNumber { get; set; }

        public Boolean IsCustomer { get; set; }
    }
}
