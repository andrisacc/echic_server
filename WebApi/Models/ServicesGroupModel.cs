﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Models
{
    public class ServicesGroupModel
    {
        public Guid Id { get; set; }

        public String Title { get; set; }

        public String SubTitle { get; set; }

        public String PictureUrl { get; set; }

        public Int32 Order { get; set; }

        public Boolean IsActive { get; set; }

        public Boolean IsSelected { get; set; }

        public Int32 UiId { get; set; }
    }
}
