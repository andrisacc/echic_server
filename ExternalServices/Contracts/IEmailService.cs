﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;


namespace ExternalServices.Contracts
{
    public interface IEmailService
    {
        void SendEmailToAdmins(string subject, string body, Stream attachment = null, string attachmentName = null);
    }
}
