﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Database.Entities
{
    public class Order : BaseEntity
    {
        public Int32 ExternalId { get; set; }

        public String PostalCode { get; set; }

        public String Address { get; set; }

        public String City { get; set; }

        public OrderStates State { get; set; }

        public DateTime RequestedDateTimeUtc { get; set; }

        public DateTime RequestedDateTimeLocal { get; set; }

        public virtual List<OrderService> Services { get; set; }

        public Guid? CustomerCardId { get; set; }

        [ForeignKey("CustomerCardId")]
        public CustomerCard CustomerCard { get; set; }

        [Required]
        public Guid CustomerId { get; set; }

        [ForeignKey("CustomerId")]
        public Customer Customer { get; set; }

        [Required]
        public Guid LocationId { get; set; }

        [ForeignKey("LocationId")]
        public AllowedLocation Location { get; set; }

        public Guid? ServiceProviderId { get; set; }

        [ForeignKey("ServiceProviderId")]
        public ServiceProvider ServiceProvider { get; set; }

        [Required]
        public OrderAssignmentStatus AssignmentStatus {get;set;}

        public DateTime StartBroadcastTime { get; set; }
    }
}
