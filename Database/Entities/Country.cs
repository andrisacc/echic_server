﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Entities
{
    public class Country
    {
        public Guid Id { get; set; }

        public String Name { get; set; }
    }
}
