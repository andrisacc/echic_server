﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Database.Migrations
{
    public partial class AddOrderAssignment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "EyebrowsExpirience",
                table: "ServiceProviders",
                nullable: false,
                defaultValue: 1);

            migrationBuilder.AddColumn<int>(
                name: "ManicureExpirience",
                table: "ServiceProviders",
                nullable: false,
                defaultValue: 1);

            migrationBuilder.AddColumn<int>(
                name: "ServiceProviderType",
                table: "ServiceProviders",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<Guid>(
                name: "ServiceProviderId",
                table: "Orders",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Orders_ServiceProviderId",
                table: "Orders",
                column: "ServiceProviderId");

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_ServiceProviders_ServiceProviderId",
                table: "Orders",
                column: "ServiceProviderId",
                principalTable: "ServiceProviders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Orders_ServiceProviders_ServiceProviderId",
                table: "Orders");

            migrationBuilder.DropIndex(
                name: "IX_Orders_ServiceProviderId",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "EyebrowsExpirience",
                table: "ServiceProviders");

            migrationBuilder.DropColumn(
                name: "ManicureExpirience",
                table: "ServiceProviders");

            migrationBuilder.DropColumn(
                name: "ServiceProviderType",
                table: "ServiceProviders");

            migrationBuilder.DropColumn(
                name: "ServiceProviderId",
                table: "Orders");
        }
    }
}
