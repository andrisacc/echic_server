﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Database.Entities
{
    public class ServiceProvider : BaseEntity
    {
        public String PostalCode { get; set; }

        public String PhoneCode { get; set; }

        public String PhoneNumber { get; set; } 

        public String FirstName { get; set; }

        public String LastName { get; set; }

        public String Email { get; set; }

        public String Address { get; set; }

        public String City { get; set; }

        public String Nationality { get; set; }

        public Boolean IsAgree { get; set; }

        public ServiceProviderTypes ServiceProviderType { get; set; }

        public Int32 ManicureExpirience { get; set; }

        public Int32 EyebrowsExpirience { get; set; }

        public Int32 Rating { get; set; }

        public virtual List<ServiceProviderDoc> Docs { get; set; }

        public virtual List<Order> Orders { get; set; }

        public Double? Latitude { get; set; }

        public Double? Longitude { get; set; }
    }
}
