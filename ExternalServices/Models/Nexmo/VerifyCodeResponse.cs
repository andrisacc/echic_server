﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExternalServices.Models.Nexmo
{
    public class VerifyCodeResponse : BaseResponse
    {
        public string ErrorText { get; set; }
    }
}
