﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExternalServices.Models.GetAddressIO
{
    public class GetAddressListResult
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string [] Addresses { get; set; }
    }
}
