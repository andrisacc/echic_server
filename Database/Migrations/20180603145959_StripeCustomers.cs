﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Database.Migrations
{
    public partial class StripeCustomers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Orders_Customers_CustomerCardId",
                table: "Orders");

            migrationBuilder.AddColumn<string>(
                name: "StripeId",
                table: "CustomerCards",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "StripeToken",
                table: "CustomerCards",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_CustomerCards_CustomerCardId",
                table: "Orders",
                column: "CustomerCardId",
                principalTable: "CustomerCards",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Orders_CustomerCards_CustomerCardId",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "StripeId",
                table: "CustomerCards");

            migrationBuilder.DropColumn(
                name: "StripeToken",
                table: "CustomerCards");

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_Customers_CustomerCardId",
                table: "Orders",
                column: "CustomerCardId",
                principalTable: "Customers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
