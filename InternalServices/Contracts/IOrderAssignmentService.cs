﻿using System;
using Database.Entities;
using System.Collections.Generic;
using System.Text;
using InternalServices.Models;

namespace InternalServices.Contracts
{
    public interface IOrderAssignmentService
    {
        Boolean IsAssignmentPossible(List<OrderService> services, DateTime orderTimeUtc);

        OrderAssignmentResult AssignOrder(Guid orderId);
    }
}
