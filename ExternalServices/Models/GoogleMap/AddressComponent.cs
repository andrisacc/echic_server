﻿namespace ExternalServices.Models.GoogleMap
{
    public class AddressComponent
    {
        public string LongName { get; set; }
        public string[] Types { get; set; }
    }
}
