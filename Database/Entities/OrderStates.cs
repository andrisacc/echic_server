﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Entities
{
    public enum OrderStates
    {
        Initial,
        New,
        Canceled,
        Done,
        Paid
    }
}
