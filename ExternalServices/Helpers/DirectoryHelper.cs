﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

namespace ExternalServices.Helpers
{
    public static class DirectoryHelper
    {
        private static string CreateIfNotExists(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            return path;
        }

        public static string FilesDirectory => CreateIfNotExists(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files"));
    }
}
