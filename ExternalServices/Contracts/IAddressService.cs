﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExternalServices.Models;

namespace ExternalServices.Contracts
{
    public interface IAddressService
    {
        bool CheckAddress(string address);
        GeocodeAddressResult GeocodeAddress(string address);
        AddressList GetAddressList(string postcode);
        FullAddressList GetFullAddressList(string postcode);
    }
}
