﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ExternalServices.Contracts;
using ExternalServices.Models;
using Database;
using Database.Entities;
using Mapster;
using System.Device.Location;
using WebApi.Models;
using Microsoft.Extensions.Logging;


namespace WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Utilities")]
    public class UtilitiesController : BaseController
    {
        private IAddressService _addressService;
        private INexmoService _nexmoService;

        public UtilitiesController(DataContext dataContext, IAddressService addressService, INexmoService nexmoService, ILogger<BaseController> logger) : base(dataContext, logger)
        {
            _addressService = addressService;
            _nexmoService = nexmoService;
        }

        [HttpGet("GetCountries")]
        public IActionResult GetCountries()
        {
            _logger.LogInformation("GetCountries called.");
            var countries = _dataContext.Countries.Select(n => n.Name).ToArray();
            return new JsonResult(countries, jsonSerializerSettings);
        }

        [HttpGet("GetServicesGroups")]
        public IActionResult GetServicesGroups()
        {
            var countries = _dataContext.ServicesGroups.Where(sg=>sg.IsActive).Select(sg => sg.Adapt<ServicesGroupModel>()).ToArray();
            return new JsonResult(countries, jsonSerializerSettings);
        }

        [HttpGet("GetServices")]
        public IActionResult GetServices(String groupId)
        {
            if (!String.IsNullOrEmpty(groupId) && _dataContext.ServicesGroups.Any(sg => sg.UiId.ToString() == groupId))
            {
                var group = _dataContext.ServicesGroups.FirstOrDefault(sg => sg.UiId.ToString() == groupId);

                var services = _dataContext.Services.Where(s => s.IsActive && s.ServicesGroupId == group.Id).Select(s => s.Adapt<ServiceModel>()).ToArray();
                return new JsonResult(services, jsonSerializerSettings);
            }
            else
            {
                var services = _dataContext.Services.Where(s => s.IsActive).Select(s => s.Adapt<ServiceModel>()).ToArray();
                return new JsonResult(services, jsonSerializerSettings);
            }
        }

        // GET: api/Utilities/CheckPostalCode/SE10DH
        //W1A1AB
        [HttpGet("CheckPostalCode/{postalCode}")]
        public IActionResult CheckPostalCode(String postalCode)
        {
            var addresses = _addressService.GetFullAddressList(postalCode);

            if (!addresses.IsExist)
            {
                addresses.IsAllowed = false;
                return new JsonResult(addresses, jsonSerializerSettings);
            }

            var sCoord = new GeoCoordinate(addresses.Latitude, addresses.Longitude);

            var selectedLocation = _dataContext.AllowedLocations
                .Where(location => sCoord.GetDistanceTo(new GeoCoordinate(location.RadiusLatitude, location.RadiusLongitude)) < location.Radius)
                .FirstOrDefault();

            if (selectedLocation != null)
            {
                addresses.IsAllowed = true;
                addresses.PhoneCode = selectedLocation.PhoneCode;
                addresses.LocationId = selectedLocation.Id;
                //addresses.TimeZone = selectedLocation.TimeZone;
                return new JsonResult(addresses, jsonSerializerSettings);
            }

            addresses.IsAllowed = false;
            return new JsonResult(addresses, jsonSerializerSettings);

            //var result = new FullAddressList();
            //result.IsAllowed = true;
            //result.IsExist = true;
            //result.PhoneCode = "44";
            //return new JsonResult(result, jsonSerializerSettings);
        }
        
        // POST: api/Utilities
        [HttpPost("SendPhoneVerificationCode")]
        //"375298083900"
        public IActionResult SendPhoneVerificationCode([FromBody]SendCodeRequestModel model)
        {
            if (!model.IsCustomer && _dataContext.ServiceProviders.Any(sp => sp.PhoneNumber == model.PhoneNumber && sp.PhoneCode == model.PhoneCode && sp.IsAgree))
                return new JsonResult(new SendCodeResponseModel() { IsAlreadyRegistred = true }, jsonSerializerSettings);

            _logger.LogInformation("Sending verification code to " + model.PhoneCode + model.PhoneNumber);

            String requestId = _nexmoService.SendCode(model.PhoneCode, model.PhoneNumber);

            _logger.LogInformation("Sent verification code to " + model.PhoneCode + model.PhoneNumber + " with requestId=" + requestId);

            return new JsonResult(new SendCodeResponseModel() { RequestId = requestId, IsAlreadyRegistred = false }, jsonSerializerSettings);
        }

        // GET: api/Utilities/VerifyPhoneCode/?requestId=qwe&code=sad
        [HttpGet("VerifyPhoneCode")]
        public IActionResult VerifyPhoneCode(String requestId, String code)
        {
            var result = _nexmoService.VerifyCode(requestId, code);
            return new JsonResult(result, jsonSerializerSettings);
        }

        // GET: api/Utilities/VerifyPhoneCode/?requestId=qwe&code=sad
        //1. Корзина меняется как на прототипе, на шаге Календаря, но при нажатии Back на экран сервисов она опять меняется на корзину с возможностью апдейтов
        //2. Если пользователь прошел верификацию по мобиле а потом вернулся беками назад на календарь, телефон надо перепроверять заново.
        //3. Рестрикшены на время заказа:
        //- Пн-Пт 8:30-21:30
        //- Сб 9:00-20:00
        //- Вс 10:00-19:00
        //На заказ сегодняшнего дня ближайшее время для заказа +4 часа от текущего, округленное вперед до 30 мин.соответственно если текущее время +4 часа больше чем время работы, заказ на текущую дату невозможен.
        [HttpPost("GetAllowedTime")]
        public IActionResult GetAllowedTime([FromBody]GetAllowedTimeModel request)
        {
            List<DayModel> days = new List<DayModel>();

            var timeZone = 1;

            DateTime curr_date = new DateTime(request.Year, request.Month, 1);
            DateTime last_date = DateTime.Now.AddDays(29);
            while (curr_date.Date <= last_date.Date)
            {
                if (curr_date.Date < DateTime.Now.Date)
                {
                    var d = new DayModel();
                    d.Day = curr_date.Day;
                    d.Month = curr_date.Month;
                    d.Year = curr_date.Year;
                    
                    d.IsAllowed = false;

                    days.Add(d);
                }
                else if (curr_date.Date == DateTime.Now.Date)
                {
                    DateTime starttime = DateTime.Now.ToUniversalTime().AddHours(timeZone), endtime = DateTime.Now.ToUniversalTime().AddHours(timeZone);

                    if (curr_date.DayOfWeek != DayOfWeek.Saturday && curr_date.DayOfWeek != DayOfWeek.Sunday) //8:30-21:30
                    {
                        starttime = new DateTime(curr_date.Year, curr_date.Month, curr_date.Day, 8, 30, 0);
                    }
                    else if (curr_date.DayOfWeek == DayOfWeek.Saturday)//9:00-20:00
                    {
                        endtime = new DateTime(curr_date.Year, curr_date.Month, curr_date.Day, 9, 0, 0);
                    }
                    else if (curr_date.DayOfWeek == DayOfWeek.Sunday)//10:00-19:00
                    {
                        endtime = new DateTime(curr_date.Year, curr_date.Month, curr_date.Day, 10, 0, 0);
                    }

                    if (starttime - DateTime.Now.ToUniversalTime().AddHours(timeZone) < TimeSpan.FromHours(4))
                    {
                        starttime = DateTime.Now.ToUniversalTime().AddHours(timeZone).AddHours(4);
                    }

                    if (starttime.Minute < 30)
                    {
                        starttime = starttime.AddMinutes(30 - starttime.Minute);
                    }
                    else
                    {
                        starttime = starttime.AddMinutes(60 - starttime.Minute);
                    }

                    if (curr_date.DayOfWeek != DayOfWeek.Saturday && curr_date.DayOfWeek != DayOfWeek.Sunday) //8:30-21:30
                    {
                        endtime = new DateTime(curr_date.Year, curr_date.Month, curr_date.Day, 21, 30, 0);
                    }
                    else if (curr_date.DayOfWeek == DayOfWeek.Saturday)//9:00-20:00
                    {
                        endtime = new DateTime(curr_date.Year, curr_date.Month, curr_date.Day, 20, 0, 0);
                    }
                    else if (curr_date.DayOfWeek == DayOfWeek.Sunday)//10:00-19:00
                    {
                        endtime = new DateTime(curr_date.Year, curr_date.Month, curr_date.Day, 19, 0, 0);
                    }

                    var d = new DayModel();
                    d.Day = curr_date.Day;
                    d.Month = curr_date.Month;
                    d.Year = curr_date.Year;
                    
                    if (starttime >= endtime)
                    {
                        d.IsAllowed = false;
                    }
                    else
                    {
                        d.MinTimeH = starttime.Hour;
                        d.MinTimeM = starttime.Minute;

                        d.MaxTimeH = endtime.Hour;
                        d.MaxTimeM = endtime.Minute;

                        d.IsAllowed = true;
                    }

                    days.Add(d);
                }
                else
                {
                    DateTime starttime = DateTime.Now.ToUniversalTime().AddHours(timeZone), endtime = DateTime.Now.ToUniversalTime().AddHours(timeZone);

                    if (curr_date.DayOfWeek != DayOfWeek.Saturday && curr_date.DayOfWeek != DayOfWeek.Sunday) //8:30-21:30
                    {
                        starttime = new DateTime(curr_date.Year, curr_date.Month, curr_date.Day, 8, 30, 0);
                        endtime = new DateTime(curr_date.Year, curr_date.Month, curr_date.Day, 21, 30, 0);
                    }
                    else if (curr_date.DayOfWeek == DayOfWeek.Saturday)//9:00-20:00
                    {
                        starttime = new DateTime(curr_date.Year, curr_date.Month, curr_date.Day, 9, 0, 0);
                        endtime = new DateTime(curr_date.Year, curr_date.Month, curr_date.Day, 20, 0, 0);
                    }
                    else if (curr_date.DayOfWeek == DayOfWeek.Sunday)//10:00-19:00
                    {
                        starttime = new DateTime(curr_date.Year, curr_date.Month, curr_date.Day, 10, 0, 0);
                        endtime = new DateTime(curr_date.Year, curr_date.Month, curr_date.Day, 19, 0, 0);
                    }

                    var d = new DayModel();
                    d.Day = curr_date.Day;
                    d.Month = curr_date.Month;
                    d.Year = curr_date.Year;

                    d.MinTimeH = starttime.Hour;
                    d.MinTimeM = starttime.Minute;

                    d.MaxTimeH = endtime.Hour;
                    d.MaxTimeM = endtime.Minute;

                    d.IsAllowed = true;

                    days.Add(d);
                }
                curr_date = curr_date.AddDays(1);
            }
            return new JsonResult(days.ToArray(), jsonSerializerSettings);
        }
    }
}
