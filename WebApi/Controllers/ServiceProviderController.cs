﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Database;
using Database.Entities;
using WebApi.Models;
using ExternalServices.Contracts;
using Microsoft.Extensions.Logging;

namespace WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/ServiceProvider")]
    public class ServiceProviderController : BaseController
    {
        IEmailService _emailService;

        public ServiceProviderController(DataContext dataContext, IEmailService emailService, ILogger<BaseController> logger) : base(dataContext, logger)
        {
            _emailService = emailService;
        }

        // PUT: api/Utilities/Create
        [HttpPut("Create")]
        public IActionResult Create()
        {
            var serviceProviderId = Guid.NewGuid();
            var serviceProvider = new ServiceProvider()
            {
                Id = serviceProviderId
            };
            _dataContext.ServiceProviders.Add(serviceProvider);
            _dataContext.SaveChanges();

            return new JsonResult(serviceProviderId, jsonSerializerSettings);
        }

        // POST: api/Utilities
        [HttpPost("UpdatePostalCode")]
        public IActionResult UpdatePostalCode([FromBody]ServiceProviderModel requestedServiceProvider)
        {
            Guid tmpServiceProviderId;
            if(!Guid.TryParse(requestedServiceProvider.Id, out tmpServiceProviderId))
            {
                return StatusCode(500);
            }

            var serviceProvider = _dataContext.ServiceProviders.FirstOrDefault(sp => sp.Id == tmpServiceProviderId);
            if(serviceProvider == null)
            {
                return StatusCode(500);
            }

            serviceProvider.PostalCode = requestedServiceProvider.PostalCode;
            _dataContext.SaveChanges();
            
            return new JsonResult(new ServiceProviderModelResponse() { IsSuccess = true }, jsonSerializerSettings);
        }

        // POST: api/Utilities
        [HttpPost("UpdatePhoneNumber")]
        //"375298083900"
        public IActionResult UpdatePhoneNumber([FromBody]ServiceProviderModel requestedServiceProvider)
        {
            Guid tmpServiceProviderId;
            if (!Guid.TryParse(requestedServiceProvider.Id, out tmpServiceProviderId))
            {
                return StatusCode(500);
            }

            var serviceProvider = _dataContext.ServiceProviders.FirstOrDefault(sp => sp.Id == tmpServiceProviderId);
            if (serviceProvider == null)
            {
                return StatusCode(500);
            }

            String phoneNumber;
            if (requestedServiceProvider.PhoneNumber.StartsWith("00"))
                phoneNumber = requestedServiceProvider.PhoneNumber.Substring(2);
            else if (requestedServiceProvider.PhoneNumber.StartsWith("0"))
                phoneNumber = requestedServiceProvider.PhoneNumber.Substring(1);
            else
                phoneNumber = requestedServiceProvider.PhoneNumber;

            if(_dataContext.ServiceProviders.Any(sp => sp.PhoneNumber == phoneNumber && sp.PhoneCode == requestedServiceProvider.PhoneCode && sp.IsAgree))
            {
                return new JsonResult(new ServiceProviderModelResponse() { IsSuccess = false, IsPhoneNumberAlreadyRegistred = true }, jsonSerializerSettings);
            }

            serviceProvider.PhoneNumber = phoneNumber;
            serviceProvider.PhoneCode = requestedServiceProvider.PhoneCode;
            _dataContext.SaveChanges();

            return StatusCode(200);
        }

        // POST: api/Utilities
        [HttpPost("UpdateContacts")]
        public IActionResult UpdateContacts([FromBody]ServiceProviderModel requestedServiceProvider)
        {
            Guid tmpServiceProviderId;
            if (!Guid.TryParse(requestedServiceProvider.Id, out tmpServiceProviderId))
            {
                return StatusCode(500);
            }

            var serviceProvider = _dataContext.ServiceProviders.FirstOrDefault(sp => sp.Id == tmpServiceProviderId);
            if (serviceProvider == null)
            {
                return StatusCode(500);
            }

            if (_dataContext.ServiceProviders.Any(sp => sp.Email == requestedServiceProvider.Email && sp.IsAgree))
            {
                return new JsonResult(new ServiceProviderModelResponse() { IsSuccess = false, IsEmailAlreadyRegistred = true }, jsonSerializerSettings);
            }

            serviceProvider.FirstName = requestedServiceProvider.FirstName;
            serviceProvider.LastName = requestedServiceProvider.LastName;
            serviceProvider.Email = requestedServiceProvider.Email;
            serviceProvider.Address = requestedServiceProvider.Address;
            serviceProvider.City = requestedServiceProvider.City;
            serviceProvider.Nationality = requestedServiceProvider.Nationality;

            _dataContext.SaveChanges();

            return new JsonResult(new ServiceProviderModelResponse() { IsSuccess = true }, jsonSerializerSettings);
        }

        // POST: api/Utilities
        [HttpPost("UpdateAgree")]
        public IActionResult UpdateAgree([FromBody]ServiceProviderModel requestedServiceProvider)
        {
            Guid tmpServiceProviderId;
            if (!Guid.TryParse(requestedServiceProvider.Id, out tmpServiceProviderId))
            {
                return StatusCode(500);
            }

            var serviceProvider = _dataContext.ServiceProviders.FirstOrDefault(sp => sp.Id == tmpServiceProviderId);
            if (serviceProvider == null)
            {
                return StatusCode(500);
            }

            if (!requestedServiceProvider.IsAgree)
            {
                return StatusCode(500);
            }

            serviceProvider.IsAgree = requestedServiceProvider.IsAgree;

            _dataContext.SaveChanges();

            var passportDoc = _dataContext.ServiceProviderDocs.Where(d => d.ServiceProviderId == tmpServiceProviderId && d.DocType == "passport").FirstOrDefault();
            var niDoc = _dataContext.ServiceProviderDocs.Where(d => d.ServiceProviderId == tmpServiceProviderId && d.DocType == "ninumber").FirstOrDefault();
            var addressDoc = _dataContext.ServiceProviderDocs.Where(d => d.ServiceProviderId == tmpServiceProviderId && d.DocType == "addressproof").FirstOrDefault();
            var cvDoc = _dataContext.ServiceProviderDocs.Where(d => d.ServiceProviderId == tmpServiceProviderId && d.DocType == "cv").FirstOrDefault();
            var permitDoc = _dataContext.ServiceProviderDocs.Where(d => d.ServiceProviderId == tmpServiceProviderId && d.DocType == "workpermit").FirstOrDefault();

            var baseUrl = "";

#if DEBUG
            baseUrl = "http://54.93.33.188/";
#else
            baseUrl = "https://www.echic.world/";
#endif
            try
            {
                string body = "<p>Full Name: " + serviceProvider.FirstName + " " + serviceProvider.LastName + "</p>" +
                    "<p>Phone number: " + serviceProvider.PhoneCode + serviceProvider.PhoneNumber + "</ p>" +
                    "<p>Email: " + serviceProvider.Email + "</p>" +
                    "<p>Address: " + serviceProvider.PostalCode + ", " + serviceProvider.Address + "</p>" +
                    "<p>Nationality: " + serviceProvider.Nationality + "</p>" +
                    "<p>Passport: <a href='" + baseUrl + "api/Files/?serviceProviderId=" + passportDoc.ServiceProviderId + "&filename=" + passportDoc.Id + "'>download passport</a></p>" +
                    "<p>Ni Number: <a href='" + baseUrl + "api/Files/?serviceProviderId=" + niDoc.ServiceProviderId + "&filename=" + niDoc.Id + "'>download ni number</a></p>" +
                    "<p>Proof of address: <a href='" + baseUrl + "api/Files/?serviceProviderId=" + addressDoc.ServiceProviderId + "&filename=" + addressDoc.Id + "'>download proof of address</a></p>" +
                    "<p>CV: <a href='" + baseUrl + "api/Files/?serviceProviderId=" + cvDoc.ServiceProviderId + "&filename=" + cvDoc.Id + "'>download cv</a></p>" +
                    (permitDoc != null ? "<p>Proof of right to work: <a href='" + baseUrl + "api/Files/?serviceProviderId=" + permitDoc.ServiceProviderId + "&filename=" + permitDoc.Id + "'>download proof of right to work</a></p>" : "");

                _emailService.SendEmailToAdmins("new beautician", body);
            }
            catch(Exception exc)
            {

            }

            return new JsonResult(new ServiceProviderModelResponse() { IsSuccess = true }, jsonSerializerSettings);
        }
    }
}