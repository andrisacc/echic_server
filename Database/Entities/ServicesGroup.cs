﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Entities
{
    public class ServicesGroup : BaseEntity
    {
        public String Title { get; set; }

        public String SubTitle { get; set; }

        public String PictureUrl { get; set; }

        public Int32 Order { get; set; }

        public Boolean IsActive { get; set; }

        public Boolean IsSelectedByDefault { get; set; }

        public Int32 UiId { get; set; }

        public virtual List<Service> Services { get; set; }
    }
}
