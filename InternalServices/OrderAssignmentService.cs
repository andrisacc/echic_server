﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Database;
using Database.Entities;
using InternalServices.Models;
using InternalServices.Contracts;
using Microsoft.Extensions.Logging;
using ExternalServices.Contracts;
using System.Device.Location;

namespace InternalServices
{
    public class OrderAssignmentService : IOrderAssignmentService
    {
        private NLog.Logger _logger = NLog.Web.NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();

        DataContext _dataContext;
        IAddressService _addressService;
        Int32 _maxCheapOrderAmount;
        Int32 _orderSpan;

        public OrderAssignmentService(DataContext dataContext, Int32 maxCheapOrderAmount, Int32 orderSpanMin, IAddressService addressService)
        {
            _dataContext = dataContext;
            _maxCheapOrderAmount = maxCheapOrderAmount;
            _orderSpan = orderSpanMin;
            _addressService = addressService;
        }

        private void RefreshServiceProvidersLocation(List<ServiceProvider> serviceProviders)
        {
            serviceProviders.ForEach(sp =>
            {
                if(!sp.Latitude.HasValue || !sp.Longitude.HasValue)
                {
                    var res = _addressService.GeocodeAddress(sp.Address);
                    if(res.Success)
                    {
                        var s = _dataContext.ServiceProviders.Where(spp => spp.Id == sp.Id).First();
                        s.Longitude = res.Longitude;
                        s.Latitude = res.Latitude;
                        _dataContext.SaveChanges();
                    }
                }
            });
        }

        private List<ServiceProvider> GetAvailibleServiceProviders(List<OrderService> services, DateTime orderTimeUtc)
        {
            var totalDuration = TimeSpan.FromMinutes(services.Sum(s => s.Service.Duration * s.Quantity));

            var totalAmount = services.Sum(s => s.Service.Cost * s.Quantity);

            var maxOrderTimeUtc = orderTimeUtc + totalDuration + TimeSpan.FromMinutes(_orderSpan);

            var assignedOrders = _dataContext.OrdersServices
                .Select(os => new { os.Order, TotalDuration = os.Quantity * os.Service.Duration })
                .Where(g => g.Order.State == OrderStates.New && g.Order.ServiceProvider != null)
                .GroupBy(g => g.Order)
                .Select(g => new { Order = g.Key, TotalDuration = g.Sum(o => o.TotalDuration) })
                .ToList();

            var timeConflictedServiceProviders = assignedOrders
                .Where(g => (orderTimeUtc >= g.Order.RequestedDateTimeUtc && orderTimeUtc <= (g.Order.RequestedDateTimeUtc + TimeSpan.FromMinutes(g.TotalDuration) + TimeSpan.FromMinutes(_orderSpan)))
                    || (maxOrderTimeUtc >= g.Order.RequestedDateTimeUtc && maxOrderTimeUtc <= (g.Order.RequestedDateTimeUtc + TimeSpan.FromMinutes(g.TotalDuration) + TimeSpan.FromMinutes(_orderSpan))))
                .Select(g => g.Order.ServiceProviderId)
                .ToList();

            var availibleSP = _dataContext.ServiceProviders.Where(sp => !timeConflictedServiceProviders.Contains(sp.Id)).ToList();

            return availibleSP;
        }

        public Boolean IsAssignmentPossible(List<OrderService> services, DateTime orderTimeUtc)
        {
            var availibleProviders = GetAvailibleServiceProviders(services, orderTimeUtc);

            if (services.Any(s => s.Service.IsManiPedi) && services.Any(s => s.Service.IsWaxing)) //mixed
            {
                return availibleProviders.Any(sp => sp.ServiceProviderType == ServiceProviderTypes.Contract); //looking for contracted beautitians only
            }
            else if (services.Any(s => s.Service.IsManiPedi)) //manipedi
            {
                return availibleProviders.Any(sp => sp.ServiceProviderType == ServiceProviderTypes.ManiPedi || sp.ServiceProviderType == ServiceProviderTypes.Contract);
            }
            else //waxing
            {
                return availibleProviders.Any(sp => sp.ServiceProviderType == ServiceProviderTypes.WaxingEyebrows || sp.ServiceProviderType == ServiceProviderTypes.Contract);
            }
        }

        public OrderAssignmentResult AssignOrder(Guid orderId)
        {
            var order = _dataContext.Orders.Where(o => o.Id == orderId).Include(o => o.Services).ThenInclude(s => s.Service).FirstOrDefault();

            if (order == null)
                return new OrderAssignmentResult() { IsAssigned = false, IsBroadcasted = false, ServiceProvider = null };

            //first check. two order sp limitation on requested order date
            var availibleServiceProviders = GetAvailibleServiceProviders(order.Services.ToList(), order.RequestedDateTimeUtc);

            var availibleSPWithLessThenTwoOrder = _dataContext.ServiceProviders
                .SelectMany(sp => sp.Orders, (sp, o) => new { ServiceProvider = sp, OrderId = o.Id, OrderDate = o.RequestedDateTimeUtc })
                .Where(g => g.OrderDate.Date == order.RequestedDateTimeUtc.Date)
                .GroupBy(g => g.ServiceProvider)
                .Select(g => new { ServiceProvider = g.Key, OrdersCnt = g.Count() })
                .Where(g => g.OrdersCnt < 2)
                .ToList();

            List<ServiceProvider> availibleSPWithLessThenTwoOrderAccordingToOrderType;
            if ((order.Services.Any(s => s.Service.IsManiPedi) && order.Services.Any(s => s.Service.IsWaxing)) || order.Services.Sum(s => s.Quantity * s.Service.Cost) <= _maxCheapOrderAmount) //mixed or cheap
            {
                availibleSPWithLessThenTwoOrderAccordingToOrderType = availibleSPWithLessThenTwoOrder
                    .Where(sp => sp.ServiceProvider.ServiceProviderType == ServiceProviderTypes.Contract)
                    .Select(sp => sp.ServiceProvider)
                    .ToList(); //looking for contracted beautitians only
            }
            else if (order.Services.Any(s => s.Service.IsManiPedi)) //manipedi
            {
                availibleSPWithLessThenTwoOrderAccordingToOrderType = availibleSPWithLessThenTwoOrder
                    .Where(sp => sp.ServiceProvider.ServiceProviderType == ServiceProviderTypes.ManiPedi || sp.ServiceProvider.ServiceProviderType == ServiceProviderTypes.Contract)
                    .Select(sp => sp.ServiceProvider)
                    .ToList();
            }
            else //waxing
            {
                availibleSPWithLessThenTwoOrderAccordingToOrderType = availibleSPWithLessThenTwoOrder
                    .Where(sp => sp.ServiceProvider.ServiceProviderType == ServiceProviderTypes.WaxingEyebrows || sp.ServiceProvider.ServiceProviderType == ServiceProviderTypes.Contract)
                    .Select(sp => sp.ServiceProvider)
                    .ToList();
            }

            if (!availibleSPWithLessThenTwoOrderAccordingToOrderType.Any()) //no providers with less then two date orders found, but time nonconflicted providers availible 
            {
                order.AssignmentStatus = OrderAssignmentStatus.Broadcasted;
                order.StartBroadcastTime = DateTime.Now.ToUniversalTime();
                _dataContext.SaveChanges();

                return new OrderAssignmentResult()
                {
                    IsAssigned = false,
                    IsBroadcasted = true,
                    ServiceProvider = null
                };
            }

            if (availibleSPWithLessThenTwoOrderAccordingToOrderType.Count() == 1) //if found only one providers with less then two date orders
            {
                order.AssignmentStatus = OrderAssignmentStatus.Assigned;
                order.ServiceProviderId = availibleSPWithLessThenTwoOrderAccordingToOrderType.First().Id;
                _dataContext.SaveChanges();

                return new OrderAssignmentResult()
                {
                    IsAssigned = true,
                    IsBroadcasted = false,
                    ServiceProvider = availibleSPWithLessThenTwoOrderAccordingToOrderType.First()
                };
            };


            //second check. max expirience
            List<ServiceProvider> selectedServiceProviders = new List<ServiceProvider>();
            if ((order.Services.Any(s => s.Service.IsManiPedi) && order.Services.Any(s => s.Service.IsWaxing)) 
                || order.Services.Sum(s => s.Quantity * s.Service.Cost) <= _maxCheapOrderAmount 
                || availibleSPWithLessThenTwoOrderAccordingToOrderType.Where(sp => sp.ServiceProviderType != ServiceProviderTypes.Contract).Count() == 0) //mixed
            {
                var oveallExperienceServiceProviders = availibleSPWithLessThenTwoOrderAccordingToOrderType
                    .Where(sp => sp.ServiceProviderType == ServiceProviderTypes.Contract)
                    .Select(sp => new { ServiceProvider = sp, Experience = Math.Sqrt(sp.EyebrowsExpirience * sp.EyebrowsExpirience + sp.ManicureExpirience * sp.ManicureExpirience) });

                var maxExperience = oveallExperienceServiceProviders.Max(sp => sp.Experience);

                selectedServiceProviders = oveallExperienceServiceProviders
                    .Where(sp => Math.Sqrt(sp.ServiceProvider.EyebrowsExpirience * sp.ServiceProvider.EyebrowsExpirience + sp.ServiceProvider.ManicureExpirience * sp.ServiceProvider.ManicureExpirience) == maxExperience)
                    .Select(sp=>sp.ServiceProvider)
                    .ToList();
            }
            else if (order.Services.Any(s => s.Service.IsManiPedi) && !order.Services.Any(s => s.Service.IsWaxing)) //manipedi
            {
                var max_manipedi_experience = availibleSPWithLessThenTwoOrderAccordingToOrderType
                    .Where(sp => sp.ServiceProviderType == ServiceProviderTypes.ManiPedi)
                    .Max(sp => sp.ManicureExpirience);

                selectedServiceProviders = availibleSPWithLessThenTwoOrderAccordingToOrderType
                    .Where(sp => sp.ServiceProviderType == ServiceProviderTypes.ManiPedi)
                    .Where(sp => sp.ManicureExpirience == max_manipedi_experience)
                    .ToList();
            }
            else //waxing
            {
                var max_waxing_experience = availibleSPWithLessThenTwoOrderAccordingToOrderType
                    .Where(sp => sp.ServiceProviderType == ServiceProviderTypes.WaxingEyebrows)
                    .Max(sp => sp.EyebrowsExpirience);

                selectedServiceProviders = availibleSPWithLessThenTwoOrderAccordingToOrderType
                    .Where(sp => sp.ServiceProviderType == ServiceProviderTypes.WaxingEyebrows)
                    .Where(sp => sp.EyebrowsExpirience == max_waxing_experience)
                    .ToList();
            }

            if (selectedServiceProviders.Count() == 1)
            {
                return new OrderAssignmentResult()
                {
                    IsAssigned = true,
                    IsBroadcasted = false,
                    ServiceProvider = selectedServiceProviders.First()
                };
            }

            //thrid check. rating
            var max_rating = selectedServiceProviders.Max(sp => sp.Rating);
            selectedServiceProviders = selectedServiceProviders.Where(sp => sp.Rating == max_rating).ToList();

            if (selectedServiceProviders.Count() == 1)
            {
                return new OrderAssignmentResult()
                {
                    IsAssigned = true,
                    IsBroadcasted = false,
                    ServiceProvider = selectedServiceProviders.First()
                };
            }

            //fourth check. nearest one
            RefreshServiceProvidersLocation(selectedServiceProviders);

            var orderLocation = _addressService.GeocodeAddress(order.Address);
            
            if(orderLocation.Success)
            {
                var orderCoords = new GeoCoordinate(orderLocation.Latitude, orderLocation.Longitude);
                var locatedSelectedServiceProviders = selectedServiceProviders
                    .Select(sp => new { ServiceProvider = sp, Dist = orderCoords.GetDistanceTo(new GeoCoordinate(sp.Latitude.Value, sp.Longitude.Value)) });

                var minDistance = locatedSelectedServiceProviders.Min(sp => sp.Dist);

                var selectedServiceProvider = locatedSelectedServiceProviders
                    .Where(sp => sp.Dist == minDistance)
                    .Select(sp => sp.ServiceProvider)
                    .First();

                return new OrderAssignmentResult()
                {
                    IsAssigned = true,
                    IsBroadcasted = false,
                    ServiceProvider = selectedServiceProvider
                };
            }

            return null;
        }
    }
}
