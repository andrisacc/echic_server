﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Database.Entities
{
    public class Service : BaseEntity
    {
        [Required]
        public Guid ServicesGroupId { get; set; }

        public String Title { get; set; }

        public String SubTitle { get; set; }

        public Int32 Order { get; set; }

        public Decimal Cost { get; set; }

        public Int32 Duration { get; set; }

        public Boolean IsActive { get; set; }

        public Boolean IsManiPedi { get; set; }

        public Boolean IsWaxing { get; set; }

        [ForeignKey("ServicesGroupId")]
        public virtual ServicesGroup ServicesGroup { get; set; }
    }
}
