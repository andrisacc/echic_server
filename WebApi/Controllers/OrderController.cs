﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Database;
using Database.Entities;
using WebApi.Models;
using ExternalServices.Contracts;
using Microsoft.Extensions.Logging;
using Mapster;
using System.IO;
using Newtonsoft.Json;
using InternalServices;
using InternalServices.Contracts;
using InternalServices.Models;

namespace WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Order")]
    public class OrderController : BaseController
    {
        IEmailService _emailService;
        IStripeService _stripeService;
        IOrderAssignmentService _orderService;

        public OrderController(DataContext dataContext, IEmailService emailService, ILogger<BaseController> logger, IStripeService stripeService, IOrderAssignmentService orderService) : base(dataContext, logger)
        {
            _emailService = emailService;
            _stripeService = stripeService;
            _orderService = orderService;
        }

        // PUT: api/Order/Create
        [HttpPut("Create")]
        public IActionResult Create([FromBody]OrderModel model)
        {
            String phoneNumber = "";
            if (model.PhoneNumber.StartsWith("00"))
                phoneNumber = model.PhoneNumber.Substring(2);
            else if (model.PhoneNumber.StartsWith("0"))
                phoneNumber = model.PhoneNumber.Substring(1);
            else
                phoneNumber = model.PhoneNumber;

            //1. Check Customer, if not exists create new one
            //2. Create new order with reference to customer and purchases and location

            Customer customer = _dataContext.Customers.Where(c => c.PhoneCode == model.PhoneCode && c.PhoneNumber == phoneNumber).FirstOrDefault();
            if (customer == null)
            {
                customer = new Customer();
                customer = model.Adapt<Customer>();
                _dataContext.Customers.Add(customer);
            }
            else
            {
                customer.Email = model.Email;

                if (!String.IsNullOrEmpty(customer.CustomerStripeId))
                {
                    _stripeService.UpdateCustomer(customer.CustomerStripeId, model.Email);
                }

                customer.FirstName = model.FirstName;
                customer.LastName = model.LastName;
            }
            _dataContext.SaveChanges();

            Int32 orderNum = 1;
            if (_dataContext.Orders.Count() > 0)
                orderNum = _dataContext.Orders.Max(o => o.ExternalId) + 1;

            var order = model.Adapt<Order>();
            order.State = OrderStates.Initial;
            order.ExternalId = orderNum;
            order.RequestedDateTimeUtc = order.RequestedDateTimeLocal.ToUniversalTime();
            order.CustomerId = customer.Id;
            _dataContext.Orders.Add(order);
            _dataContext.SaveChanges();

            var orderServices = model.Purchases.Select(p => new OrderService()
            {
                OrderId = order.Id,
                Quantity = p.Quantity,
                ServiceId = p.Id
            });
            _dataContext.AddRange(orderServices);
            _dataContext.SaveChanges();

            return new JsonResult(new { orderNumber = orderNum, orderId = order.Id }, jsonSerializerSettings);
        }

        [HttpGet("GetPulisableKey")]
        public IActionResult GetPulisableKey()
        {
            var result = _stripeService.GetPublishableKey();
            return new JsonResult(result, jsonSerializerSettings);
        }

        // POST: api/Utilities
        [HttpPost("UpdateOrder")]
        public IActionResult UpdateOrder([FromBody]OrderPaymentModel orderToken)
        {
            Order order = null;
            Customer customer = null;
            CustomerCard card = null;
            try
            {
                order = _dataContext.Orders.FirstOrDefault(o => o.Id == orderToken.OrderId);
                if (order == null)
                {
                    return StatusCode(500);
                }

                customer = _dataContext.Customers.Where(c => c.Id == order.CustomerId).FirstOrDefault();
                if (customer == null)
                {
                    return StatusCode(500);
                }

                //_logger.LogInformation("UpdateOrder for order: " + JsonConvert.SerializeObject(order));
                //_logger.LogInformation("UpdateOrder for customer: " + JsonConvert.SerializeObject(customer));

                
                if (String.IsNullOrEmpty(customer.CustomerStripeId))
                {
                    var stripeCustomerResult = _stripeService.CreateCustomer(orderToken.StripeToken, customer.Email);
                    customer.CustomerStripeId = stripeCustomerResult.CustomerId;

                    card = new CustomerCard() { CustomerId = customer.Id, StripeId = stripeCustomerResult.CardId, StripeToken = orderToken.StripeToken };
                    _dataContext.CustomerCards.Add(card);
                }
                else
                {
                    card = _dataContext.CustomerCards.FirstOrDefault(c => c.StripeToken == orderToken.StripeToken && c.CustomerId == customer.Id);

                    if (card == null)
                    {
                        var stripeCardResult = _stripeService.AddCustomerCard(customer.CustomerStripeId, orderToken.StripeToken);
                        card = new CustomerCard() { StripeId = stripeCardResult.Id, StripeToken = orderToken.StripeToken, CustomerId = customer.Id };
                        _dataContext.CustomerCards.Add(card);
                        _dataContext.SaveChanges();
                    }
                }

            }
            catch (Exception exc)
            {
                _logger.LogError("UpdateOrder 1: " + exc.Message);
                new JsonResult(new ServiceProviderModelResponse() { IsSuccess = false }, jsonSerializerSettings);
            }

            if (order == null || customer == null || card == null)
            {
                new JsonResult(new ServiceProviderModelResponse() { IsSuccess = false }, jsonSerializerSettings);
            }

            try
            {
                order.CustomerCardId = card.Id;
                order.State = OrderStates.New;
                _dataContext.SaveChanges();

                var orderServicesForEmail = _dataContext.OrdersServices.Where(os => os.OrderId == orderToken.OrderId).Select(op => new
                {
                    op.Service.Title,
                    op.Quantity,
                    op.Service.Cost,
                    op.Service.Duration
                });

                var orderForEmail = _dataContext.Orders.Where(o => o.Id == orderToken.OrderId).Select(o => new
                {
                    o.Id,
                    o.ExternalId,
                    o.Customer.FirstName,
                    o.Customer.LastName,
                    o.Customer.PhoneNumber,
                    o.PostalCode,
                    o.Address,
                    o.City,
                    o.Customer.Email,
                    o.RequestedDateTimeLocal,
                    o.RequestedDateTimeUtc
                }).FirstOrDefault();

            
                string body = "<p>New order:<p>"
                    + "<p>OrderId: " + orderForEmail.Id + "</p>"
                    + "<p>ExternalId: " + orderForEmail.ExternalId + "</p>"
                    + "<p>FirstName: " + orderForEmail.FirstName + "</p>"
                    + "<p>LastName: " + orderForEmail.LastName + "</p>"
                    + "<p>PhoneNumber: " + orderForEmail.PhoneNumber + "</p>"
                    + "<p>PostalCode: " + orderForEmail.PostalCode + "</p>"
                    + "<p>Address: " + orderForEmail.Address + "</p>"
                    + "<p>City: " + orderForEmail.City + "</p>"
                    + "<p>Email: " + orderForEmail.Email + "</p>"
                    + "<p>Date local: " + orderForEmail.RequestedDateTimeLocal.ToShortDateString() + " " + orderForEmail.RequestedDateTimeLocal.ToShortTimeString() + "</p>"
                    + "<p>Date utc: " + orderForEmail.RequestedDateTimeUtc.ToShortDateString() + " " + orderForEmail.RequestedDateTimeUtc.ToShortTimeString() + "</p>"
                    + "<p>Services:</p>";

                int serviceNum = 1;
                foreach (var os in orderServicesForEmail)
                {
                    body += "<p>" + serviceNum.ToString() + "</p>"
                        + "<p>&#9;Title: " + os.Title + "</p>"
                        + "<p>&#9;Cost of one: " + os.Cost + "</p>"
                        + "<p>&#9;Duration of one: " + os.Duration + "</p>";
                    serviceNum++;
                }

                _logger.LogInformation("email body: " + body);

                _emailService.SendEmailToAdmins("new order", body);
            }
            catch (Exception exc)
            {
                _logger.LogError("UpdateOrder 2: " + exc.Message);
                new JsonResult(new ServiceProviderModelResponse() { IsSuccess = false }, jsonSerializerSettings);
            }

            return new JsonResult(new ServiceProviderModelResponse() { IsSuccess = true }, jsonSerializerSettings);
        }

        [HttpGet("GetOrders")]
        public IActionResult GetOrders(String secretKey)
        {
            if (secretKey == _stripeService.GetInternalKey())
            {
                var result = _dataContext.Orders.Select(o => new {
                    o.Id,
                    o.ExternalId,
                    customerId = o.Customer.Id,
                    o.Customer.PhoneCode,
                    o.Customer.PhoneNumber,
                    o.Customer.FirstName,
                    o.Customer.LastName,
                    o.City,
                    o.Address,
                    o.PostalCode,
                    o.LocationId,
                    o.RequestedDateTimeLocal,
                    o.RequestedDateTimeUtc,
                    o.State,
                    Services = o.Services.Select(s => new
                    {
                        s.Id,
                        s.Quantity,
                        s.ServiceId,
                        s.Service.Title
                    })
                }).OrderBy(o => o.ExternalId);

                return new JsonResult(result, jsonSerializerSettings);
            }

            return StatusCode(500);
        }

        [HttpGet("SendEmail")]
        public IActionResult SendEmail(String secretKey)
        {
            if (secretKey == _stripeService.GetInternalKey())
            {
                string body = "<p>test email<p>";
                _emailService.SendEmailToAdmins("test", body);
                return StatusCode(200);
            }

            return StatusCode(500);
        }

        [HttpGet("Charge")]
        public IActionResult Charge(String orderNumber, String secretKey)
        {
            Int32 orderNumberInt;
            if (secretKey == _stripeService.GetInternalKey() && Int32.TryParse(orderNumber, out orderNumberInt))
            {
                var order = _dataContext.Orders.FirstOrDefault(o => o.ExternalId == orderNumberInt);
                var customer = _dataContext.Customers.FirstOrDefault(c => c.Id == order.CustomerId);
                var customerCard = _dataContext.CustomerCards.FirstOrDefault(cc => cc.Id == order.CustomerCardId);

                if (order != null)
                {
                    var amount = _dataContext.OrdersServices.Where(os => os.OrderId == order.Id).Select(os => new
                    {
                        os.Quantity,
                        os.Service.Cost
                    }).Sum(ns => ns.Cost * ns.Quantity);

                    var location = _dataContext.AllowedLocations.FirstOrDefault(l => l.Id == order.LocationId);

                    var result = _stripeService.Charge(customer.CustomerStripeId, customerCard.StripeId, order.ExternalId.ToString(), (Int32)amount, order.Id, location.Currency);

                    order.State = OrderStates.Done;

                    _dataContext.SaveChanges();

                    var orderResult = _dataContext.Orders.Where(o=>o.Id == order.Id).Select(o => new
                    {
                        o.Id,
                        o.ExternalId,
                        customerId = o.Customer.Id,
                        o.Customer.PhoneCode,
                        o.Customer.PhoneNumber,
                        o.Customer.FirstName,
                        o.Customer.LastName,
                        o.City,
                        o.Address,
                        o.PostalCode,
                        o.LocationId,
                        o.RequestedDateTimeLocal,
                        o.RequestedDateTimeUtc,
                        o.State,
                        Services = o.Services.Select(s => new
                        {
                            s.Id,
                            s.Quantity,
                            s.ServiceId,
                            s.Service.Title
                        })
                    });

                    return new JsonResult(orderResult, jsonSerializerSettings);
                }

                return StatusCode(500);
            }

            return StatusCode(500);
        }

        [HttpGet("Cancel")]
        public IActionResult Cancel(String orderNumber, String secretKey)
        {
            Int32 orderNumberInt;
            if (secretKey == _stripeService.GetInternalKey() && Int32.TryParse(orderNumber, out orderNumberInt))
            {
                var order = _dataContext.Orders.FirstOrDefault(o => o.ExternalId == orderNumberInt);

                if (order != null)
                {
                    order.State = OrderStates.Canceled;

                    _dataContext.SaveChanges();

                    var orderResult = _dataContext.Orders.Where(o => o.Id == order.Id).Select(o => new
                    {
                        o.Id,
                        o.ExternalId,
                        customerId = o.Customer.Id,
                        o.Customer.PhoneCode,
                        o.Customer.PhoneNumber,
                        o.Customer.FirstName,
                        o.Customer.LastName,
                        o.City,
                        o.Address,
                        o.PostalCode,
                        o.LocationId,
                        o.RequestedDateTimeLocal,
                        o.RequestedDateTimeUtc,
                        o.State,
                        Services = o.Services.Select(s => new
                        {
                            s.Id,
                            s.Quantity,
                            s.ServiceId,
                            s.Service.Title
                        })
                    });

                    return new JsonResult(orderResult, jsonSerializerSettings);
                }

                return StatusCode(500);
            }

            return StatusCode(500);
        }

        // POST: api/Order/Stripe
        [HttpPost("StripeWebhook")]
        public IActionResult StripeWebhook()
        {
            var json = new StreamReader(Request.Body).ReadToEnd();

            _logger.LogInformation(json);

            var signature = Request.Headers["Stripe-Signature"];

            var res = _stripeService.ParseWebhook(json, signature);

            if (res != null)
            {

                var order = _dataContext.Orders.FirstOrDefault(o => o.Id == res.OrderId);
                if (order != null)
                {
                    var amount = _dataContext.OrdersServices.Where(os => os.OrderId == order.Id).Select(os => new
                    {
                        os.Quantity,
                        os.Service.Cost
                    }).Sum(ns => ns.Cost * ns.Quantity);

                    if (res.Amount == amount && res.IsPaymentSuccesfull)
                    {
                        order.State = OrderStates.Paid;
                        _dataContext.SaveChanges();
                    }
                }
            }

            return StatusCode(200);
        }
    }
}